%GOLDEN  Changes current figure size to match the golden ratio keeping width.
%  GOLDEN  Sets current figure rectance size to the golden ratio in landscape.
%  GOLDEN(FIGURE,PORTRAIT) sets the specified figure handled rectangle to the
%  golden ratio, in landscape if portrait is FALSE (default), portrait
%  otherwise.
%

function golden(handle,portrait)

% Handle inputs
narginchk(0,2);
if nargin < 1
    handle = gcf;
end
if nargin < 2
    portrait = false;
end

% Handle 'all'
if ischar(handle)
    if strcmpi(handle,'all')
        handle = get(0,'Children');
    else
        error(['Unsupported command: ' handle]);
    end
end

% Handle multiples
if length(handle) > 1
    for i = 1:length(handle)
        golden(handle(i),portrait);
    end
    return;
end

% Golden ratio
goldenratio = (1+sqrt(5))/2;

% Save it
pos    = get(handle,'Position');
width  = pos(3);
height = pos(4);
if portrait
    width = height/goldenratio;
else
    height = width/goldenratio;
end
set(handle,'Position',[pos(1) pos(2) width height]);
onscreen;
