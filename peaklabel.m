%PEAKLABEL  Label peaks on current plot.
%  Labels peaks in current line object or first line object in current axes.
%  Uses PEAKFINDER for peak detection.
%
%  See also: PEAKFINDER
%

% Doug Harriman (doug.harriman@gmail.com)

function [h_str,h_pt] = peaklabel(thresh)

% Get handle of line object.
han = gco;
if ~strcmp(get(han,'Type'),'line')
    % Look for line in current axes.
    ch = get(gca,'Children');
    han = findobj(ch,'Type','line');
    
    if isempty(han)
        error('No line objects found.');
    end
    han = han(end);
end

% Get data.
x = get(han,'XData');
y = get(han,'YData');

% Find peaks
if nargin > 0
    idx = peakfinder(y,thresh);
else
    idx = peakfinder(y);
end

% Labels.
hold('on');
x = x(idx);
y = y(idx);
h_pt = plot(x,y,'ro');

xlim = get(get(han,'Parent'),'XLim');
dx = range(xlim) * 0.025;

if isrow(x)
    x = x';
end
str = cellstr(num2str(x,'%0.1f'));

h_str = text(x+dx,y,str);