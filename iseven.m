%ISEVEN Returns TRUE if argument is even.
%       Note that any non-integer values are considered odd.
%

% DLH, 01/18/00

function result = isodd(arg)

result = ~isodd(arg);
