%FLEXTOOLPOLL  Polls directory for Flextool file, opening a file when it's available.
%
%  Path     - Path to check for file.  Default = 'c:\'.
%  Ext      - File extension.  Default = '*.trc'.
%  Period   - Polling period [sec].  Default = 1.
%  PollTime - Time duration for polling [sec].  Default = 120.
%  Fcn      - Function to run after loading Flextool data.
%
%  See also: flextool

% DLH
% 14-DEC-06 - Created

function [] = flextoolpoll(varargin)

% Default options
opt.path      = 'c:\';
opt.ext       = '*.trc';
opt.period    = 1;    % [sec]
opt.polltime  = 60*2; % [sec]
opt.fcn       = [];

% Apply changes
opt = options(opt,varargin{:});

% Search path
data.path = [opt.path filesep opt.ext];
data.fcn  = opt.fcn;

% Number of times to run
num_polls = fix(opt.polltime/opt.period);

% If not, set up a timer to do the polling.
T = timer('Name','FlextoolPollTimer',...
    'BusyMode','Drop',...
    'ExecutionMode','FixedDelay',...
    'Period',opt.period,...        % [sec]
    'TasksToExecute',num_polls,... % [sec]
    'TimerFcn',@CheckForFile,...
    'UserData',data); 
       
start(T);

return;

% CheckForFile
% Checks to see if file exists & load if exists.
function [] = CheckForFile(varargin)

% Timer that called us.
T = varargin{1};
data = get(T,'UserData');

% Look for data, bail if none.
d  = dir(data.path);
if length(d) < 1
    return;
end

% If got here, have data
% Delete the timer
delete(T);

% Poll for file readability
try_again = 1;
while(try_again)
    try
        % Load the data
        flextool;
        
        % Mark success
        try_again = 0;
    catch
        % Sleep a bit to let file writing continue.
        pause(2);
    end
end

% Do some more work
if ~isempty(data.fcn)
    evalin('base',data.fcn);
end