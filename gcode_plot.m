%GCODE_PLOT
% Plots G1 command values.

function xy = gcode_plot(filename)

% Input error handling
if nargin < 1
   filename = '';
end

% Read the file
str = file2str(filename);

% Dropp comments
str = regexprep(str,';.*$','','lineanchors');

% Drop the speeds
str = regexprep(str,'F\d+.?\d*','');

% Drop the G0 & G1
str = regexprep(str,'G0','');
str = regexprep(str,'G1','');

% Drop the X
str = regexprep(str,'X','');

% Drop the Y
str = regexprep(str,'Y','');

% Drop the Z
str = regexprep(str,'Z','');

% Drop the E
str = regexprep(str,'E\d+.?\d*','');

xy = str2num(str);

% Plot
figure;
plot(xy(:,1),xy(:,2));
set(gca,'DataAspectRatio',[1 1 1]);
pt;
xlabel('X [mm]');
ylabel('Y [mm]');

disp([int2str(length(xy)) ' points']);

if nargout == 0
   clear xy;
end
