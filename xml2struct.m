%XML2STRUCT  Reads XML file into Matlab structure.
%   S=XML2STRUCT(FILENAME) reads XML data from FILENAME and returns a
%   structure S that is defined by the XML data.
%

% DLH (doug.harriman@comcast.net)
% 11-DEC-06 - Created

function [S]=xml2struct(filename)

% Error check inputs.
if nargin ~= 1
    error('XML file name not specified.');
end
if ~ischar(filename)
    error('File name must be specified with a string input.');
end
if exist(filename,'file') ~= 2
    error(['Unable to find file: ' filename]);
end

% Read in XML data
fid = fopen(filename,'r');
if fid < 0
    error(['Unable to open file: ' filename]);
end
xml = char(fread(fid,inf)');
fclose(fid);

% Build a token type dictionary to contain its fields.
dict = struct();

% Define inputs for data converters.
expr = '<(\w+).*?>.*?</\1>';  % Regular expression
fcn  = @(x) x{1};             % Array function

% Parse structure & root node
g   = graph;
[xml, tok] = regexp(xml,expr,'match','tokens');
tok = cellfun(fcn,tok,'UniformOutput',false);
tok = tok{1};
xml = xml{1};
len = length(tok);
xml = xml(len+3:length(xml)-len-3);
data.type  = 'xml';
data.value = strtrim(xml);
[parent,g] = addnode(g,'Name',tok,'UserData',data);

% Track nodes we've handled & those we haven't
nodes_handled   = [];
nodes_unhandled = parent;

while ~isempty(nodes_unhandled)
    % Pull first node off of the unhandled list and handle its XML.  Mark
    % it as handled.
    node_cur        = nodes_unhandled(1);
    nodes_handled   = [nodes_handled node_cur];
    
    % Exract tokens or record data
    data = getnode(g,node_cur,'UserData');
    xml  = data.value;
    [new_xml, tok] = regexp(xml,expr,'match','tokens');

    % Check for data in field instead of more XML
    if isempty(new_xml)
        % Update the record for this node to reflect that it stores data
        % and is a terminal node.
        data.type  = 'value';
        g = setnode(g,node_cur,'UserData',data);
        
    else
        % Convert from cells-of-cells to cells-of-strings
        tok = cellfun(fcn,tok,'UniformOutput',false);

        % Strip the XML specifiers for this token
        for i = 1:length(new_xml)
            len    = length(tok{i});
            new_xml{i} = new_xml{i}(len+3:length(new_xml{i})-len-3);

            data.type  = 'xml';
            data.value = strtrim(new_xml{i});

            % Link back to parent
            [node_new,g] = addnode(g,'Name',tok{i},'UserData',data);
            [new_edge,g] = addedge(g,node_cur,node_new);
        end
    end

    % Update unhandled nodes
    nodes_unhandled = setdiff(nodes(g),nodes_handled);
    
end % while


% Convert the graph/tree to a heirarchical structure

% First build a list of all node types
node_names     = nodenames(g);
node_handles   = g.nodehandles;
node_type_list = unique(node_names);
node_type_dict = struct();

% Loop on each node type, looking for nodes of that type.
for i_node_type = 1:length(node_type_list)
    % Current node type
    node_type = node_type_list{i_node_type};
    
    % Add an entry to the dict for that type.
    node_type_dict.(node_type) = {};
    
    % Find node handles for nodes of that type.
    idx = strmatch(node_type,node_names,'exact');
    han = node_handles(idx);
    
    % For each instance of this node type, find the types of children that
    % it has.  Build a list of all child types.
    for i_han = 1:length(han)
        % Current node
        cur_han = han(i_han);
        
        % Child node types
        child_types = unique(ChildNames(g,cur_han));
        node_type_dict.(node_type) = union(node_type_dict.(node_type),child_types);
    end
    
end % i_node_type

S = node_type_dict;


function names = ChildNames(g,h)
han   = children(g,h);
idx   = handle2index(g,'node',han);
names = nodenames(g);
names = names(idx);

