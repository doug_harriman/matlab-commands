%SIGFIG  Rounds a number to the specified number of significant figures.
%   Y=SIGFIG(X,DIGITS) 
%

function y = sigfig(x,digits)

% Error checks.
error(nargchk(1,2,nargin));

% Defaults
if nargin < 2
    digits = 3;
end

if digits < 0
    error('Digits must be positive.');
end
if ~isint(digits)
    error('Digits must be integer valued.');
end
if ~isscalar(digits)
    error('Digits must be scalar.');
end
if ~isa(x,'double')
    error('X must be double.');
end

% Capture sign of elements.
sgn = sign(x);
x   = abs(x);

% Handle matrices by calling recursively.
if numel(x) > 1
    y = arrayfun(@(z)sigfig(z,digits),x);
    return;
end

% Bail if zero
if x == 0
    y = x;
    return;
end

% Get magnitude of number.
mag = log10(x);

% Truncate magnitude.
mag = floor(mag);

% Get divisor
div = mag - digits + 1;
div = 10^div;

% Do the rounding
y = round(x/div)*div;

% Re-apply sign.
y = sgn.*y;
