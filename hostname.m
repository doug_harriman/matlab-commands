%HOSTNAME  Returns system name.
%

function name = hostname

% Get it
[s,r] = system('hostname');
if s ~= 0
    error(r);
end

% Return 
name = strtrim(r);