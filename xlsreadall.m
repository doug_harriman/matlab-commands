function fullData = xlsreadall(fileNm)
%
% TMW (Gouda) The Netherlands,
% Robert Schultz (Application Engineer)
% Created: 3 aug 2004
%
% function fullData = xlsreadall(fileNm)
%
% Read all data from Ms Excel spreadsheet file
% input:   fileNm   -  file name
% output:  fullData -  full data of spreadsheet in data structure

[type, xlSheets] = xlsfinfo(fileNm);

if ~strcmpi(type, 'Microsoft Excel Spreadsheet')
    warning('This is not an Excel file')
    fullData = NaN;
else
    % init
    fullData = [];

    for ii = 1:length(xlSheets)
	% read only one sheet at a time
	% see >> doc xlsread
	[N, T] = xlsread(fileNm, xlSheets{ii});

	if ~isempty(N)
	    % add numeric data
	    expression = [xlSheets{ii} '_N'];
	    fullData.(expression) = N;
	end

	if ~isempty(T)
	    % add text data
	    expression = [xlSheets{ii} '_T'];
	    fullData.(expression) = T;
	end

    end % for ii

end % if
