%WS2STRUCT  Copies contents of current workspace into fields of a structure.
%  S=WS2STRUCT  Returns a structure S with a field named for each variable
%  in the current work space.
%

function S = ws2struct

% Error checks
error(nargchk(0,0,nargin));

% Get vars
vars = evalin('caller','who');

% Assign each var to a field
S = struct;
for i = 1:length(vars)
  % Current variable
  var = vars{i};
  
  % Capture it.
  S.(var) = evalin('caller',var);
end
