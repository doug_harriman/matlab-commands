%CONMAN  Contour manager.
%

% Doug Harriman

%TODO - Support 3D PDF generation with:
%http://www.mathworks.com/matlabcentral/fileexchange/25383-matlab-mesh-to-pdf-with-3d-interactive-object
% See: >> doc trimesh  for steps to convert surf to triangulated surf.
%
%TODO - Method to get a plot3 on graph in one step by providing (X,Y) data.

classdef conman < hgsetget
    properties
        % X axis data
        x = [];
        
        % Y axis data
        y = [];
        
        % Z data
        z = [];
        
        % X axis units
        xunits = '';
        
        % Y axis units
        yunits = '';
        
        % Z axis units
        zunits = '';
        
        % X axis label
        xlabel = '';
        
        % Y axis label
        ylabel = '';
        
        % Z axis label
        zlabel = '';
        
        % Isolabels on/off
        isolabels = true;
        
    end % properties - public
    
    properties (SetAccess=private)
        % Contour group handle
        h_contour = [];
        
        % Contour3 group handle
        h_contour3 = [];
        
        % ContourF group handle
        h_contourf = [];
        
        % Surf handle
        h_surf = [];
        
        % Surfl handle
        h_surfl = [];
        
        % SurfC handle
        h_surfc = [];
        
        % Mesh handle
        h_mesh = [];
        
        % Contour data
        c_contour = [];
        
        % Contour3 data
        c_contour3 = [];
        
        % ContourF data
        c_contourf = [];
        
        % Contour label handles
        l_contour = [];
        
        % Contour3 label handles
        l_contour3 = [];
        
        % Contourf label handles
        l_contourf = [];
        
    end % properties - read only
    
    properties (Access=private)
        % Have units toolbox
        haveunits = false;
        
    end % properties - private
    
    methods
        function obj = conman(x,y,z)
            % CONMAN
            % CONMAN(Z)
            % CONMAN(X,Y,Z)
            
            % Error checks.
            if (nargin ~= 0) && (nargin ~=1) && (nargin ~= 3)
                error('CONMAN requires one or three inputs.');
            end
            
            % See if we have a units toolbox.
            if ~isempty(which('unit'))
                obj.haveunits = true;
            end
            
            % Handle empty object.
            if nargin == 0
                return;
            end
            
            % Handle the two input cases.
            if nargin == 1
                obj.z = x;
                return;
            end
            
            % Must have had three inputs.
            obj.x = x;
            obj.y = y;
            obj.z = z;
            
        end % constructor
        
        function [c,h] = contour(obj,varargin)
            %CONTOUR  Draw contour plot
            %
            %  See also: contour
            %
            
            % Must have data.
            if isempty(obj.z)
                error('Z data requried.');
            end
            
            % Two calling syntaxes.
            if isempty(obj.x) || isempty(obj.y)
                [c,h] = contour(obj.z,varargin{:});
            else
                [c,h] = contour(obj.x,obj.y,obj.z,varargin{:});
            end
            
            % Store data
            obj.h_contour = h;
            obj.c_contour = c;
            
            % Call for updates
            obj.UpdateLabels;
            
            % Draw contour labels if requested.
            if obj.isolabels
                obj.l_contour = clabel(c,h);
            end
            
        end % contour
        
        function [c,h] = contour3(obj,varargin)
            %CONTOUR3  Draw contour3 plot
            %
            %  See also: contour3
            
            % Must have data.
            if isempty(obj.z)
                error('Z data requried.');
            end
            
            % Two calling syntaxes.
            if isempty(obj.x) || isempty(obj.y)
                [c,h] = contour3(obj.z,varargin{:});
            else
                [c,h] = contour3(obj.x,obj.y,obj.z,varargin{:});
            end
            
            % Store data
            obj.h_contour3 = h;
            obj.c_contour3 = c;
            
            % Call for updates
            obj.UpdateLabels;
            
            % Draw contour labels if requested.
            if obj.isolabels
                obj.l_contour3 = clabel(c,h);
            end
            
        end % contour3
        
        function [c,h] = contourf(obj,varargin)
            %CONTOURF  Draw filled contour plot
            %
            %  See also: contourf
            %
            
            % Must have data.
            if isempty(obj.z)
                error('Z data requried.');
            end
            
            % Two calling syntaxes.
            if isempty(obj.x) || isempty(obj.y)
                [c,h] = contourf(obj.z,varargin{:});
            else
                [c,h] = contourf(obj.x,obj.y,obj.z,varargin{:});
            end
            
            % Store data
            obj.h_contourf = h;
            obj.c_contourf = c;
            
            % Call for updates
            obj.UpdateLabels;
            
            % Draw contour labels if requested.
            if obj.isolabels
                obj.l_contourf = clabel(c,h);
            end
            
        end % contourf
        
        function h = surf(obj,varargin)
            %SURF  Draw surf plot
            %
            %  See also: surf
            %
            
            % Must have data.
            if isempty(obj.z)
                error('Z data requried.');
            end
            
            % Two calling syntaxes.
            if isempty(obj.x) || isempty(obj.y)
                h = surf(obj.z,varargin{:});
            else
                h = surf(obj.x,obj.y,obj.z,varargin{:});
            end
            
            % Store data
            obj.h_surf = h;
            
            % Surf niceties
            set(h,'FaceLighting','gouraud',...
                'EdgeColor','None',...
                'FaceColor','interp',...
                'FaceAlpha',0.8);
            
            % Call for updates
            obj.UpdateLabels;
            
        end % surf
        
        function h = surfl(obj,varargin)
            %SURFL  Draw surfl plot
            %
            %  See also: surfl
            %
            
            % Must have data.
            if isempty(obj.z)
                error('Z data requried.');
            end
            
            % Two calling syntaxes.
            if isempty(obj.x) || isempty(obj.y)
                h = surfl(obj.z,varargin{:});
            else
                h = surfl(obj.x,obj.y,obj.z,varargin{:});
            end
            
            % Store data
            obj.h_surfl = h;
            
            % Surf niceties
            set(h,'FaceLighting','gouraud',...
                'EdgeColor','None',...
                'FaceColor','interp',...
                'FaceAlpha',0.9);
            
            % Call for updates
            obj.UpdateLabels;
            
        end % surfl
        
        function h = surfc(obj,varargin)
            %SURFC  Draw surfc plot
            %
            %  See also: surfc
            %
            
            % Must have data.
            if isempty(obj.z)
                error('Z data requried.');
            end
            
            % Two calling syntaxes.
            if isempty(obj.x) || isempty(obj.y)
                h = surfc(obj.z,varargin{:});
            else
                h = surfc(obj.x,obj.y,obj.z,varargin{:});
            end
            
            % Store data
            obj.h_surfc = h;
            
            % Surf niceties
            set(h,'FaceLighting','gouraud',...
                'EdgeColor','None',...
                'FaceColor','interp',...
                'FaceAlpha',0.8);
            
            % Call for updates
            obj.UpdateLabels;
            
        end % surfc
        
        function h = mesh(obj,varargin)
            %MESH  Draw mesh plot
            %
            %  See also: mesh
            %
            
            % Must have data.
            if isempty(obj.z)
                error('Z data requried.');
            end
            
            % Two calling syntaxes.
            if isempty(obj.x) || isempty(obj.y)
                h = mesh(obj.z,varargin{:});
            else
                h = mesh(obj.x,obj.y,obj.z,varargin{:});
            end
            
            % Store data
            obj.h_mesh = h;
            
            % Call for updates
            obj.UpdateLabels;
            
        end % mesh
        
        function [x,y] = extractxy(obj,val)
            %EXTRACTXY  Extracts X & Y values for given Z isocline value.
            %  [X,Y]=CONMAN.EXTRACTXY(CONMAN_OBJ,Z_VALUE) returns the X and
            %  Y coordinate vectors for the isocline with requested
            %  Z_VALUE.  If multiple isoclines exist for the given Z_VALUE,
            %  then X and Y will be cell arrays.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            
            % Handle units.
            if isa(val,'unit')
                val = convert(val,obj.zunits);
                val = double(val);
            end
            
            if ~isnumeric(val)
                error('Numeric value required.');
            end
            if ~isscalar(val)
                error('Scalar value required.');
            end
            if isempty(obj.z)
                error('No Z data.');
            end
            
            % Make user value in range.
            z_min = min(min(obj.z));
            if val < z_min
                error('Z value out of data range.');
            end
            z_max = max(max(obj.z));
            if val > z_max
                error('Z value out of data range.');
            end
            
            % Find contour at the requested level.
            % Two calling syntaxes.
            if isempty(obj.x) || isempty(obj.y)
                c = contourc(obj.z,[1 1]*val);
            else
                c = contourc(obj.x,obj.y,obj.z,[1 1]*val);
            end
            
            i = 1;
            while ~isempty(c)
                % Get number of poitns in this contour.
                n = c(2,1);
                
                % Subset with which to deal.
                cc = c(:,2:n+1)';
                
                % Remaining data.
                c  = c(:,n+2:end);
                
                % Store out what we have.
                x{i,1} = cc(:,1); %#ok<AGROW>
                y{i,1} = cc(:,2); %#ok<AGROW>
                
                % Next round
                i = i + 1;
            end
            
            % Clean up data if only have one vector.
            if length(x) == 1
                x = x{1};
                y = y{1};
                
                % Apply units if have them.
                if obj.haveunits && ~isempty(obj.xunits)
                    x = unit(x,obj.xunits);
                end
                if obj.haveunits && ~isempty(obj.yunits)
                    y = unit(y,obj.yunits);
                end
            end
            
        end % extractxy
        
        function z = extractz(obj,x,y)
            %EXTRACTZ  Return Z data values for each (X,Y) point.
            %  Data found by linear interpolation using INTERP2.
            %
            
            % Error checks
            error(nargchk(3,3,nargin));
            
            % Handle units
            if isa(x,'unit')
                if isconsistent(x,unit(1,obj.xunits))
                    x = convert(x,obj.xunits);
                    x = double(x);
                else
                    error('Inconsistent units for X data');
                end
            end
            
            if isa(y,'unit')
                if isconsistent(y,unit(1,obj.yunits))
                    y = convert(y,obj.yunits);
                    y = double(y);
                else
                    error('Inconsistent units for Y data');
                end
            end
            
            if ~isnumeric(x) || ~isnumeric(y)
                error('Numeric values required.');
            end
            if ~isvector(x) || ~isvector(y)
                error('Vector values required.');
            end
            if length(x) ~= length(y)
                error('Vectors must be same length.');
            end
            if isempty(obj.z)
                error('No Z data.');
            end
            
            % Force x & y to have the same orientation.
            if size(x,1) ~= size(y,1)
                x = x';
            end
            
            % Must have X data
            if isempty(obj.x)
                x_data = 1:1:size(obj.z,1);
            else
                x_data = obj.x;
            end
            
            % Must have Y data
            if isempty(obj.y)
                y_data = 1:1:size(obj.z,2);
            else
                y_data = obj.y;
            end
            
            % Only interpolate in range.
            idx1 = (x<=max(x_data)) & (x>=min(x_data));
            idx2 = (y<=max(y_data)) & (y>=min(y_data));
            idx  = idx1 & idx2;
            
            % Interpolate for all Z values.
            z = zeros(size(x));
            for i = 1:length(x)
                if idx(i)
                    z(i) = interp2(x_data,y_data,obj.z,x(i),y(i));
                else
                    z(i) = 0;
                end
            end
            
            % Apply units if have them.
            if obj.haveunits && ~isempty(obj.zunits)
                z = unit(z,obj.zunits);
            end
            
        end % extractz
        
        function set.isolabels(obj,val)
            %ISOLABEL  Set ISOLABEL property.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            if ~isscalar(val)
                error('Scalar value expected.');
            end
            if ~islogical(val)
                try
                    val = logical(val);
                catch %#ok<CTCH>
                    error('Logical value expected.');
                end
            end
            
            % Set the val.
            obj.isolabels = val;
            
            if obj.isolabels
                % Turn on label handles for any open plots.
                try
                    % Make axes active.
                    p = get(obj.h_contour,'parent');
                    axes(p); %#ok<*MAXES>
                    
                    % Create contours.
                    obj.l_contour = clabel(obj.c_contour,obj.h_contour);
                end
                
                try
                    % Make axes active.
                    p = get(obj.h_contour3,'parent');
                    axes(p);
                    
                    % Create contours.
                    obj.l_contour3 = clabel(obj.c_contour3,obj.h_contour3);
                end
                
                try
                    % Make axes active.
                    p = get(obj.h_contourf,'parent');
                    axes(p);
                    
                    % Create contours.
                    obj.l_contourf = clabel(obj.c_contourf,obj.h_contourf);
                end
                
                
            else
                % Delete contour labels if we have them.
                try %#ok<*TRYNC>
                    delete(obj.l_contour);
                end
                
                try
                    delete(obj.l_contour3);
                end
                
                try
                    delete(obj.l_contourf);
                end
                
            end % isolabels t/f
            
        end
        
        function set.x(obj,val)
            %SET.X  Set X values.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            
            % Allow data to be cleared.
            if isempty(val)
                obj.x = [];
                return;
            end
            [val,ustr] = obj.CheckVector(val);
            
            % If we have Z data, our size must match.
            if ~isempty(obj.z)
                if length(val) ~= size(obj.z,2)
                    error('Length of X vector must equal size(Z,2)');
                end
            end
            
            % Set units
            obj.xunits = ustr; %#ok<*MCSUP>
            
            % X must be monotonically increasing
            if ~issorted(val)
                % Sort X.
                val = sort(val);
                
                % If have Z data, flip it.
                obj.z = fliplr(obj.z);
            end
            
            % Store the data.
            obj.x = val;
            
        end % set.x
        
        function set.y(obj,val)
            %SET.Y  Set Y values.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            
            % Allow data to be cleared.
            if isempty(val)
                obj.y = [];
                return;
            end
            [val,ustr] = obj.CheckVector(val);
            
            % If we have Z data, our size must match.
            if ~isempty(obj.z)
                if length(val) ~= size(obj.z,1)
                    error('Length of Y vector must equal size(Z,1)');
                end
            end
            
            % Set units
            obj.yunits = ustr; %#ok<*MCSUP>
            
            % Y must be monotonically increasing
            if ~issorted(val)
                % Sort Y.
                val = sort(val);
                
                % If have Z data, flip it.
                obj.z = flipud(obj.z);
            end
            
            % Store the data.
            obj.y = val;
            
        end % set.y
        
        function set.z(obj,val)
            %SET.Z  Set Z values.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            
            % Allow data to be cleared.
            if isempty(val)
                obj.z = [];
                return;
            end
            
            % Try to force matrix to 2D
            val = squeeze(val);
            
            if ndims(val) ~= 2
                error('2D matrix expected.');
            end
            
            % Handle units
            if obj.haveunits
                if isa(val,'unit')
                    obj.zunits = char(val);
                    val = double(val);
                end
            end
            
            if ~isnumeric(val)
                error('Numeric data expected.');
            end
            
            % Flip Z matrix if have X & Y and clearly have rotation wrong.
            if isempty(obj.x) || isempty(obj.y)
                % Nothing we can do to help.
                obj.z = val;
                return;
            end
            
            % Verify that matrix & vector sizes match.
            nx = length(obj.x);
            ny = length(obj.y);
            
            % First try.
            if ~all(size(val) == [ny nx])
                % That didn't work.  Try transpose and check again.
                val = val'; %#ok<*PROP>
                
                if ~all(size(val) == [ny nx])
                    error('Z matrix size does not match X & Y vector lengths.');
                end
            end
            
            % Store it.
            obj.z = val;
            
        end % set.z
        
        function set.xunits(obj,val)
            %SET.Xunits  Set X axis units.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            obj.ValidateString(val);
            
            % Set it.
            obj.xunits = val;
            
            % Call for updates
            obj.UpdateLabels;
            
        end % set.xunits
        
        function set.yunits(obj,val)
            %SET.Yunits  Set Y axis units.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            obj.ValidateString(val);
            
            % Set it.
            obj.yunits = val;
            
            % Call for updates
            obj.UpdateLabels;
            
        end % set.yunits
        
        function set.zunits(obj,val)
            %SET.Zunits  Set Z axis units.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            obj.ValidateString(val);
            
            % Set it.
            obj.zunits = val;
            
            % Call for updates
            obj.UpdateLabels;
            
        end % set.zunits
        
        function set.xlabel(obj,val)
            %SET.X_LABEL  Set X axis label.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            obj.ValidateString(val);
            
            % If label has units embedded, use those.
            [val,ustr] = obj.GetUnitString(val);
            if ~isempty(ustr)
                obj.xunits = ustr;
            end
            
            % Set it.
            obj.xlabel = val;
            
            % Call for updates
            obj.UpdateLabels;
            
        end % set.xlabel
        
        function set.ylabel(obj,val)
            %SET.Y_LABEL  Set Y axis label.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            obj.ValidateString(val);
            
            % If label has units embedded, use those.
            [val,ustr] = obj.GetUnitString(val);
            if ~isempty(ustr)
                obj.yunits = ustr;
            end
            
            % Set it.
            obj.ylabel = val;
            
            % Call for updates
            obj.UpdateLabels;
            
        end % set.ylabel
        
        function set.zlabel(obj,val)
            %SET.Z_LABEL  Set Z axis label.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            obj.ValidateString(val);
            
            % If label has units embedded, use those.
            [val,ustr] = obj.GetUnitString(val);
            if ~isempty(ustr)
                obj.zunits = ustr;
            end
            
            % Set it.
            obj.zlabel = val;
            
            % Call for updates
            obj.UpdateLabels;
            
        end % set.zlabel
        
        function val = get.x(obj)
            %GET.X  X property accessor.
            %
            
            % Retrieve value.
            val = obj.x;
            
            % Handle no explicit x vector.
            if isempty(val)
                val = 1:size(obj.z,2);
            end
            
            % Always return non-unit data to this object.
            s = dbstack;
            if length(s) > 1
                if strcmp(s(2).file,[mfilename '.m'])
                    return;
                end
            end
            
            % Apply units if have them.
            if obj.haveunits && ~isempty(obj.xunits)
                val = unit(val,obj.xunits);
            end
            
        end % get.x
        
        function val = get.y(obj)
            %GET.Y  Y property accessor.
            %
            
            % Retrieve value.
            val = obj.y;
            
            % Handle no explicit x vector.
            if isempty(val)
                val = 1:size(obj.z,1);
            end
            
            % Always return non-unit data to this object.
            s = dbstack;
            if length(s) > 1
                if strcmp(s(2).file,[mfilename '.m'])
                    return;
                end
            end
            
            % Apply units if have them.
            if obj.haveunits && ~isempty(obj.yunits)
                val = unit(val,obj.yunits);
            end
            
        end % get.y
        
        function val = get.z(obj)
            %GET.Z   property accessor.
            %
            
            % Retrieve value.
            val = obj.z;
            
            % Always return non-unit data to this object.
            s = dbstack;
            if length(s) > 1
                if strcmp(s(2).file,[mfilename '.m'])
                    return;
                end
            end
            
            % Apply units if have them.
            if obj.haveunits && ~isempty(obj.zunits)
                val = unit(val,obj.zunits);
            end
            
        end % get.z
        
        function objnew = extractpoly(obj,px,py)
            %EXTRACTPOLY  Extracts a region of the Z data based on the given polygon.
            %  CONMAN2=CONMAN1.EXTRACTPOLY(X,Y) where X & Y are Nx1 or 1xN.
            %  CONMAN2=CONMAN1.EXTRACTPOLY(XY) where XY is Nx2 or 2xN.
            %
            %  A rectangular window of data will be extracted that is the
            %  bounding box for the polygon. Data in the window that is
            %  outside the window will be set to NaN so that it is not
            %  plotted.
            %
            %  The polygon bounds will be quantized by the grid resolution
            %  of the Z data.
            %
            
            % Error checks.
            error(nargchk(2,3,nargin));
            
            if nargin == 2
                % Make sure numeric
                if ~isnumeric(px)
                    error('Numeric value expected.');
                end
                
                % Error check
                if min(size(px)) ~= 2
                    error('2xN or Nx2 polygon vertex matrix expected.');
                end
                
                % Make a column matrix
                if size(px,2) > size(px,1)
                    x = x';
                end
                
                % Extract X & Y
                py = px(:,2);
                px = px(:,1);
            end
            
            % Error check X & Y against each other.
            if ~all(size(px) == size(py))
                error('X & Y must be same size.');
            end
            
            %% Find bounding box indecies.
            xmax = obj.x > max(px) ;
            xmax = find(xmax,1,'first');
            
            xmin = obj.x < min(px);
            xmin = find(xmin,1,'last');
            
            ymax = obj.y > max(py) ;
            ymax = find(ymax,1,'first');
            
            ymin = obj.y < min(py);
            ymin = find(ymin,1,'last');
            
            xind = xmin:xmax;
            yind = ymin:ymax;
            
            % Extract region of interest.
            x = obj.x(xind);
            y = obj.y(yind);
            z = obj.z(yind,xind);
            
            % Build matrices where each points corrospond to the X or Y
            % coords.
            X = (x * ones(1,length(y)))';
            Y = (y * ones(1,length(x))) ;
            
            % Determine which points are fully inside the polygon.
            [in,on] = inpolygon(X,Y,px,py);
            idx1     = in | on;
            
            warning('EXTRACTPOLY still missing points');
            
            % Now determine which pixels are crossed by polygon perimeter.
            % Outermost loop iterates through polygon points.
if (0)            
            px = px'; % Want column vectors.
            py = py';
            idx2 = false(size(idx1));
            for si = 1:length(px)-1
                % Polynomial segment we're checking.
                s_poly = [px(si:si+1), py(si:si+1)];
                %h_poly = plot(s_poly(:,1),s_poly(:,2),'r');
                %set(h_poly,'linewidth',2);
                
                % Check each grid line segment for crossing.  If they
                % cross, mark that element of idx2 true.
                % Optimization: bounding box of polynomial segment can
                %               drastically reduce the number of grid
                %               segments to check.
                for xi = 1:length(x)-1
                    for yi = 1:length(y)-1
                        % Optimization: bail if cell already marked.
                        
                        % Select grid segment to check.
                        % Vertical segment
                        s_grid = [x(xi) y(yi); x(xi) y(yi+1)];
                        
                        % Plot
                        %h_grid = plot(s_grid(:,1),s_grid(:,2),'r');
                        %set(h_grid,'linewidth',2);
                        
                        % Check for intersection
                        xy = obj.segintersect(s_poly,s_grid);
                        if ~isempty(xy)
                            % Have an intersection.
                            % Mark and skip ahead to next pixel, as this one
                            % is already marked, no use marking twice.
                            idx2(yi,xi) = true;
                            
                            % If we cross a line, make sure we mark the
                            % pixel to the left too.
                            if xi > 1
                                idx2(yi,xi-1) = true;
                            end
                            
                            continue;
                        end
                        %delete(h_grid)
                        
                        % Horizontal segment
                        s_grid = [x(xi) y(yi); x(xi+1) y(yi)];
                        
                        %h_grid = plot(s_grid(:,1),s_grid(:,2),'r');
                        %set(h_grid,'linewidth',2);
                        
                        % Check for intersection
                        xy = obj.segintersect(s_poly,s_grid);
                        if ~isempty(xy)
                            % Have an intersection.
                            % Mark and skip ahead to next pixel, as this one
                            % is already marked, no use marking twice.
                            idx2(yi,xi) = true;
                            
                            % If we cross a line, make sure we mark the
                            % pixel below too.
                            if yi > 1
                                idx2(yi-1,xi) = true;
                            end
                        end
                        %delete(h_grid)
                    end % yi
                end % xi
                %delete(h_poly);
            end % i1
            
            % Combine the index vectors & clear out Z data not used.
            idx = idx1 | idx2;
else
    idx = idx1;
end
            z(~idx) = NaN;
            
            % Create new object and fill in extracted data.
            objnew   = obj.copy;  % Get all public props copied over.
            
            % Clear out old data
            objnew.x = [];
            objnew.y = [];
            objnew.z = [];
            
            % Put in new data.
            objnew.z = z;
            objnew.x = x;
            objnew.y = y;
        end
        
        function objnew = copy(obj)
            %COPY  Create a copy of a CONMAN object.
            %  CONMAN2=COPY(CONMAN1)
            %
            
            % Error checks
            error(nargchk(1,1,nargin));
            
            % Create a new, empty object.
            objnew = eval(mfilename);
            
            % Get meta data.
            data = metaclass(objnew);
            
            % Loop on properties looking for public settable props.
            for i = 1:length(data.Properties)
                if strcmp(data.Properties{i}.SetAccess,'public')
                    % Have a public setter.
                    prop = data.Properties{i}.Name;
                    objnew.(prop) = obj.(prop);
                end
            end
            
        end % copy constructor.
        
        function [x,y,z] = xyz(obj)
            %XYZ  Generates full X,Y,Z data vectors with an entry for each point.
            %  This data is compatible with PLOT3.
            
            % Error checks
            error(nargchk(1,1,nargin));
            
            % Check for any empty data
            if isempty(obj.x)
                error('X data has not been set.');
            end
            if isempty(obj.y)
                error('Y data has not been set.');
            end
            if isempty(obj.z)
                error('Z data has not been set.');
            end
            
            % Construct exhaustive data point list.
            [ny, nx] = size(obj.z);
            
            % Stair step x
            x = ones(ny,1)*obj.x';
            x = reshape(x,numel(x),1);
            
            % Sawtooth y
            y = ones(nx,1)*obj.y';
            y = reshape(y',numel(y),1);
            
            % Just reshape z
            z = reshape(obj.z,numel(obj.z),1);
            
        end % xyz
        
        function setxyz(obj,x,y,z)
            %SETXYZ  Setx data by allowing input of X, Y and Z vectors.
            %  SETXYZ(X,Y,Z) sets object x,y and z properties where X, Y
            %  and Z are Nx1 or 1xN vectors with one (x,y,z) set per data
            %  point.  This is the data format used with PLOT3.
            %
            %  X is expected to be formatted [x1 x1 x1 ... xn xn xn].
            %  Y is expected to be formatted [y1 y2 y3 ... y1 y2 y3].
            %
            
            % Error checks
            error(nargchk(4,4,nargin));
            
            if ~isnumeric(x) || ~isnumeric(y) || ~isnumeric(z)
                error('Numeric data expected.');
            end
            
            if ~isvector(x) || ~isvector(y) || ~isvector(z)
                error('Vector data expected.');
            end
            
            if length(x) ~= length(y)
                error('X and Y must be of same length.');
            end
            if length(x) ~= length(z)
                error('X and Z must be of same length.');
            end
            
            % Force column vectors
            if ~iscol(x)
                x = x';
            end
            if ~iscol(y)
                y = y';
            end
            if ~iscol(z)
                z = z';
            end
            
            % X is expected to be stored in a stair step array that
            % increases.
            x   = unique(x);  % The sort is OK in this case.
            n_x = length(x);
            
            % The number of unique elements of Y is now a function of the
            % size of X and the number of Y elements.  Just want to pick off
            % the first N unique elements.
            n_y = length(y)/length(x);
            assert(isint(n_y));
            y = y(1:n_y);
            
            % Now reshape z into a matrix of n_x by n_y
            Z = reshape(z,n_y,n_x);
            
            % Clear data
            obj.z = [];
            obj.x = [];
            obj.y = [];
            
            % Store data
            obj.z = Z;
            obj.x = x;
            obj.y = y;
            
        end % setxyz
        
        function [objnew,k] = fitplane(obj)
            %FITPLANE  Least squares fit of a CONMAN surface.
            %  OBJNEW=FITPLANE(OBJ) returns a new CONMAN object holding a
            %  plane that is least squares fit to the data in the original
            %  CONMAN object.
            %
            %  [OBJNEW,K]=FITPLANE(OBJ) also returns the fit coefficents
            %  that define the plane according to the equation:
            %
            %    z(x,y) = k(1)*x + k(2)*y + k(3)
            %
            
            % Error checks
            error(nargchk(1,1,nargin));
            
            % Get data as vectors.
            [x,y,z] = obj.xyz;
            
            % Biff any NaN data, as it destroys fit.
            idx = ~isnan(z);
            x = x(idx);
            y = y(idx);
            z = z(idx);
            
            % Do the fit.
            const = ones(size(z));
            k = [x y const]\z;      % Least squares fit.
            
            % Generate new CONMAN object with plane data.
            [xx,yy] = meshgrid(obj.x,obj.y);
            zz = k(1)*xx + k(2)*yy + k(3);
            
            objnew   = copy(obj);
            objnew.x = [];
            objnew.y = [];
            objnew.z = [];
            
            objnew.z = zz;
            objnew.x = obj.x;
            objnew.y = obj.y;
            
        end
        
    end % methods - public
    
    methods (Access=private)
        function [data,ustr] = CheckVector(obj,data)
            %CHECKVECTOR  Validates vector data.
            %
            
            if ~isvector(data)
                error('Nx1 or 1xN data expected.');
            end
            
            % Handle units
            ustr = '';
            if obj.haveunits
                if isa(data,'unit')
                    ustr = char(data);
                    data = double(data);
                end
            end
            
            if ~isnumeric(data)
                error('Numeric data expected.');
            end
            
            if isrow(data)
                data = data';
            end
            
        end % CheckVector
        
        function [str,ustr] = GetUnitString(obj,str) %#ok<MANU>
            %GetUnitStr  Extracts unit string from label string.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            
            % Look for '[]'
            reg = '\[([\w/*\w\^]+)\]';
            tok = regexp(str,reg,'tokens');
            if ~isempty(tok)
                ustr = tok{1}{1};
                str  = regexprep(str,reg,'');
                str  = strtrim(str);
                return;
            end
            
            % Look for '()'
            reg = '\(([\w/*\w\^]+)\)';
            tok = regexp(str,reg,'tokens');
            if ~isempty(tok)
                ustr = tok{1}{1};
                str  = regexprep(str,reg,'');
                str  = strtrim(str);
                return;
            end
            
            % Just return the default
            ustr = '';
            
        end % GetUnitString
        
        function UpdateLabels(obj)
            %UPDATELABELS  Update all plots with new label values.
            %
            
            % Get handles of valid contours and surfaces.
            handles = obj.GetHandles;
            
            % Cycle through each, updating xlabels & ylabels.
            for i = 1:length(handles)
                p = get(handles(i),'parent');
                axes(p); %#ok<LAXES>
                if isempty(obj.xunits)
                    xlabel(obj.xlabel); %#ok<*CPROP>
                else
                    xlabel([obj.xlabel ' [' obj.xunits ']']); %#ok<*CPROP>
                end
                
                if isempty(obj.yunits)
                    ylabel(obj.ylabel);
                else
                    ylabel([obj.ylabel ' [' obj.yunits ']']);
                end
                
                if isempty(obj.zunits)
                    zlabel(obj.zlabel);
                    title(obj.zlabel);
                else
                    str = [obj.zlabel ' [' obj.zunits ']'];
                    zlabel(str);
                    title(str);
                end
                
            end % handles
            
        end % UpdateLabels
        
        function ValidateString(obj,val) %#ok<MANU>
            %VALIDATEUNITS  Validates strings for units or labels.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            if ~ischar(val)
                error('Character array expected.');
            end
            if size(val,1) > 1
                error('Multi line text not supported.');
            end
            if any(val == char(10))
                error('Carriage returns not supported.');
            end
            if any(val == char(13))
                error('New lines not supported.');
            end
            
            % If you get here, the string is OK.
            
        end % ValidateString
        
        function h = GetHandles(obj)
            %GETHANDLES  Returns array of all valid contour & surface handles.
            %
            
            % Simple ones.
            h = [obj.h_contour obj.h_contourf obj.h_surf obj.h_surfl ...
                obj.h_mesh obj.h_surfc' obj.h_contour3'];
            
            % Contour3 returns a handle for each line object.
            % Validate them
            idx = ishandle(h);
            h = h(idx);
            
        end % GetHandles
        
        function [xy] = segintersect(s1,s2)
            %SEGINTERSECT  Line segment intersection detection.
            %  XY=SEGINTERSECT(S1,S2) determines the segment intersection point XY of
            %  line segments S1 and S2.  S1 and S2 are defined as:
            %  S = [x1 y1; x2 y2];
            %  XY = [x y];
            %  XY is empty if segments are colinear, parallel or do not intesect within
            %  the segment bounds.
            %
            % Algorithm from:
            % http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
            %
            
            % Assume segments don't cross
            xy = [];
            
            % Break segment definitions into pt, delta pt representation.
            p = [s1(1,:)  0];
            r = [diff(s1) 0];
            
            q = [s2(1,:)  0];
            s = [diff(s2) 0];
            
            % Check for parallel
            m = cross(r,s);
            if m(3) == 0
                return;
            end
            
            % Check for collinear
            m = cross(q-p,r);
            if m(3) == 0
                return;
            end
            
            % Find the ratio along each segment that the intersection would occur.
            t = cross((q - p),s) / cross(r,s);
            u = m / cross(r,s);
            
            % Make sure intersection lies on both segments.
            if (t<0) || (t>1)
                return;
            end
            
            if (u<0) || (u>1)
                return;
            end
            
            % Calc the intersection point.
            xy = p(1:2) + t*r(1:2);
        end % segintersect
        
    end % methods - private
    
end % classdef