%VLINE  Draw vertical line at given level.
%

function [han] = vline(level,varargin)

ylim = get(gca,'ylim');
hold('on');
for i = 1:numel(level)
    han(i) = plot([1 1]*level(i),ylim,varargin{:});
end
