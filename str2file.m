%STR2FILE  Overwrites a text file with the given string.
%  STR2FILE(STR,FILENAME) overwrites FILENAME with STR.  If FILENAME is not
%  provided, a temporary file name will be used.
%

% Doug Harriman

function filename = str2file(str,filename)

% Error checks
narginchk(1,2);
if nargin < 2
    filename = tempname;
end
if ~ischar(filename)
    error('File name must be specified with a string value.');
end

% Acceptable input types
if ~iscellstr(str) && ~isa(str,'char')
   error('Input must be char or cellstr');
end

% Try to open.
fid = fopen(filename,'w');
if fid < 0
    error(['Unable to open file: ' filename]);
end
c = onCleanup(@()fclose(fid));

% Cell string.
if iscellstr(str)
   for i = 1:length(str)
      fprintf(fid,'%s\n',str{i});
   end
   return;
end

% Normal string
fwrite(fid,str);
