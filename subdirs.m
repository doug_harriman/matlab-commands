%SUBDIRS  Returns a cell string list of all directories below a given directory.
%   LIST=SUBDIRS(PARENT) returns a LIST of all subdirectories below the
%   PARENT directory.  Directory names starting with '.' are ignored.
%
%  
%
%   See also: DIR, SUBDIRS.
%

% DLH (doug.harriman@comcast.net)
% 11-DEC-06 - Created

function [dir_name_list] = subdirs(base_dir_name)

% Error check inputs
if nargin ~= 1
    error('One input expected.');
end
if ~ischar(base_dir_name)
    error('Parent directory must be specified with a string.');
end

% Look for the base dir
if exist(base_dir_name,'dir') ~= 7
    error(['Directory does not exist: ' base_dir_name]);
end

% Build list of all directory names
dir_name_list     = {base_dir_name};
dir_count_handled = 0;

while dir_count_handled < length(dir_name_list)
    % Current directory to work.
    dir_current = dir_name_list{dir_count_handled+1};
    dir_data    = dir(dir_current);

    % Add any subdirectories to list.
    for i = 1:length(dir_data)
        if (dir_data(i).isdir) && ~strcmp(dir_data(i).name,'.') && ~strcmp(dir_data(i).name,'..') && ~(dir_data(i).name(1)=='.')
            dir_name_list{length(dir_name_list)+1} = [dir_current filesep dir_data(i).name];
        end
    end
        
    % Note this directory as handled
    dir_count_handled = dir_count_handled + 1;

end % main while

dir_name_list = dir_name_list';