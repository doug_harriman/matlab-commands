%ISINT  Checks that all values in a vector are integers.
%       Returns 1 if true, 0 if false.
%

% DLH
% 08/03/00 - Created
% 10/08/04 - Vectorized results.

function result = isint(data)

% Error check input
if nargin ~= 1,
   error('ISINT requires a single input.');
end

result = (data == round(data));
