%LEASTSQ Least squares line fit.
%
%>>[m,b] = leastsq(x,y) 
%		x: X data series.
%		y: Y data series.
%		m: Line slope.
%		b: Line Y axis intercept.

% DLH 1/27/98
% 

%TODO - Add R2 output to determine fit quality:
%  http://en.wikipedia.org/wiki/Coefficient_of_determination

function [m,b] = leastsq(x,y)

% Error checks
error(nargchk(2,2,nargin));
if numel(x) ~= numel(y)
    error('Inputs must have same number of elements.');
end
if size(x,1) ~= size(y,1)
    y = y';
end

% Calculate sub terms
SumX  = sum(x) ;
SumY  = sum(y) ;
SumXY = sum(x.*y) ;

SumXSq = sum(x.^2) ;

pts = length(x) ;

% Calculate constants
m = (pts*SumXY - SumY*SumX)/(pts*SumXSq - SumX^2) ;
b = (SumY*SumXSq - SumXY*SumX)/(pts*SumXSq - SumX^2) ;