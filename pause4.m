function pause4(timesec)

%PAUSE4  Run code while pausing for specified time.
%   PAUSE4(n) ... PAUSE4 should be used as a clause that wraps code that is
%   inteded to run during a specified interval.  n is in seconds, and it
%   can be fractional.  PAUSE4 without any arguments completes the pause
%   routine.
%
%   Example:
%       PAUSE4(2);
%           <code 1>
%       PAUSE4;
%       <code 2>
%
%   This will run <code 1> during "pause" period, and <code 2> will be
%   executed 2 seconds after PAUSE4(2) command.
%
%   This is useful if you want to maintain a regular interval, but do not
%   want intermediate code to disrupt the interval.  If the execution of
%   <code 1> is longer than the pause period, <code 2> will execute
%   immediately after the clause.
%
%   Try the following code to test the difference between PAUSE4 and PAUSE:
%
%     tic;
%     for iLoop=1:10
%       pause4(1);
%       pause(0.5); % this simulates a lengthy calculation within the loop
%       fprintf('loop = %d\n',iLoop);
%       pause4;
%     end
%     fprintf('Elapse Time using PAUSE4: %f sec\n\n',toc);
% 
%     tic;
%     for iLoop=1:10
%       pause(0.5); % same lengthy calculation
%       fprintf('loop = %d\n',iLoop);
%       pause(1);
%     end
%     fprintf('Elapse Time using PAUSE: %f sec\n\n',toc);
%
%
%   See also PAUSE.
%
%   Jiro Doke
%   August 2004

persistent pause_ending_time;

if nargin==0
  try
    a=pause_ending_time;clear pause_ending_time;
    pause(datenummx(a - clock)*86400);return;
  catch
    error('You must first run pause4(n)');
  end
else
  pause_ending_time=clock+[0,0,0,0,0,timesec];return;
end