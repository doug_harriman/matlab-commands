%ROUNDDOWN  Rounds the value down to the specified value.
%  ROUNDDOWN=ROUNDDOWN(VALUE,PLACE) rounds the given value to the specified
%  PLACE.  Magnitude of the number will alway decrease, regardless of sign.
%  Limitations: If PLACE is not integer, then INV(PLACE) must be integer.
%
%  PLACE may be any value.  
%  Examples:
%  >> rounddown(12.6,0.25) -> 12.5
%  >> rounddown(1.33,1)    -> 1
%  >> rounddown(1.33,5)    -> 0
%

function out = rounddown(val,place)

% Error checks
error(nargchk(2,2,nargin));

% Place must be scalar
if ~isscalar(place)
    error('PLACE must be scalar.');
end

% Make sure place is a valid value.
if ~isint(place)
if ~isint(inv(place))
    error('Inverse of PLACE must be an integer value.');
end
end


% Save out sign.
sgn = sign(val);
val = abs(val);

% Do the rounding
out = place*floor(val./place);
out = sgn.*out;