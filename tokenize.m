%TOKENIZE  Converts a string to a cell array.
%

function [C] = tokenize(str,chr)

% First indecies.
idx = strfind(str,chr);

% Capture first char.
if str(1) ~= chr
    idx = [0 idx];
end

% Get start/stop indecies
idx1 = idx + 1;
idx2 = idx - 1;
idx2 = [idx2(2:end) length(str)];

n = length(idx1);
C = cell(n,1);
for i = 1:n
    S = str(idx1(i):idx2(i));
    if isempty(S)
        S = '';
    end
    C{i,1} = S;
end