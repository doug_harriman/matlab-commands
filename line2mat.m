%LINE2MAT Convert '|' delimited strings to string matrices.
%

function [strmat] = line2mat(in)

% Get indicies of '|'
ind = find('|'==in) ;

% Get number of lines we'll have
lines = length(ind)+1 ;

% Loop through each to make mat
ind = [0 ind (length(in)+1)] ;
for i=2:length(ind),

  % Get substring
  sub = in(ind(i-1)+1:ind(i)-1) ;

  % Tack on
  strmat = str2mat(strmat,sub) ;

end

% remove blank 1st line
strmat = strmat(2:size(strmat,1),:) ;
