%PRINTALL  Prints all open figure windows.
%

% DLH
% 06/08/01 - Created.

function [] = printall()

figs = get(0,'children');

for i=1:length(figs),
    figure(figs(i));
    print -dwinc
end
