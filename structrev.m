%STRUCTREV  Generates a reverse lookup structure.
% S2=STRUCTREV(S1) generates a structure where the values of S1 become the
% field names of S2, and the fieldnames of S1 become the values of S2.  S1
% values must by only strings that are convertable to valid Matlab variable
% names, and the values must be unique.
%

% Doug Harriman

function S2 = structrev(S1)

% Error checks
error(nargchk(1,1,nargin));

% Get fields & values
fields = fieldnames(S1);
vals   = struct2cell(S1);

% Validate value types.
tf = cellfun(@(x)ischar(x),vals);
if ~all(tf)
    error('Input structure contains non-char values.');
end

% Generate new field names
vals = cellfun(@(x)genvarname(x),vals,'UniformOutput',false);

% Make sure new field names are unique.
n_vals = length(vals);
n_uni  = length(unique(vals));
if n_uni < n_vals
    error('Input structure contains redundant values.');
end

% Create the new structure.
S2 = cell2struct(fields,vals);
    