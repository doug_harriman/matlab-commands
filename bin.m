%BIN  Bin data according to specified bin spacing.
%  [CENTERS,COUNT,INDEX]=BIN(X,SPACING) returns the values of the bin
%  CENTERS, the COUNT of items in each bin, and the bin INDEX for each item
%  in X.
%

% Doug Harriman 

function [centers,count,index] = bin(x,spacing)

% Error checks
error(nargchk(2,2,nargin));

if ~isvector(x)
    error('X must be a vector.');
end

if ~isscalar(spacing) || (spacing <= 0)
    error('Spacing must be a positive scalar.');
end

% Generate equally spaced bins based on data range and bin spacing.
centers = [rounddown(min(x),spacing):spacing:roundup(max(x),spacing)];

% Determine bin edges on the inside.
edges = centers(1:end-1) + diff(centers)/2;

% Add outside edges.
edges = [-inf edges inf];

% Get data indecies for each bin.
[count,index] = histc(x,edges);

% Last bin has values that match edges(end).
% Since we set up bin edges carefully, this value is not needed.
count = count(1:end-1);
