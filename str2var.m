%STR2VAR  Converts string to valid variable name.
%
%  VARIABLE_NAME=STR2VAR(STRING)  Converts STRING to a valid Matlab
%  variable name by converting and removing illegal characters.
%

% DLH
% 04/14/05 - Created

function [str] = str2var(str)

warning('STR2VAR is depricated.  Use GENVARNAME instead.');
str = genvarname(str);

return;

% Original implementation

% Error check inputs
if nargin ~= 1, error('1 input expected.'); end
if ~isstr(str), error('String input expected.'); end

% Remove leading an trailing blanks
str = fliplr(deblank(fliplr(deblank(str))));

% Replace illegal name characters with '_'
illegal_chars = ['_\/,- []()'];
for i = 1:length(illegal_chars),
    str = strrep(str,illegal_chars(i),'_');
end

% Make sure we don't start with a numeric character.
if ~isempty(str2num(str(1))),
    str = ['_' str];
end
