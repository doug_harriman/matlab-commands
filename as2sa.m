%AS2SA  DEPRICATED: use SCALARSTRUCT instead.

function [s_out] = as2sa(varargin)

warning('Depricated.  Use SCALARSTRUCT instead.');
s_out = scalarstruct(varargin{1},1);
