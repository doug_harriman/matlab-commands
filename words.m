%WORDS Converts a string to a list of words. 
%   ARRY = WORDS(STRING,DELIMITER) breaks STRING
%   into a cell array of strings ARRY.  Words are 
%   separated by DELIMITER.  If DELIMITER is not
%   specified, whitespace is used.
%

% DLH
% 05/18/04 - Allowed use of other delimiter.
% 06/14/01 - Created

function strAry = words(str,delim)

if ~isstr(str),
    error('Argument must be a string');
end

strAry = {};
index  = 1;
while ~isempty(str),
    if nargin == 2,
        [strAry{index}, str] = strtok(str,delim);
    else
        [strAry{index}, str] = strtok(str);
    end
    
    % Remove whitespace padding.
    strAry{index} = strtrim(strAry{index});
    
    index = index + 1;
end