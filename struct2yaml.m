%STRUCT2YAML  Serializes a structure as YAML markup.
%

% Doug Harriman (doug.harriman@gmail.com)
% 

function str = struct2yaml(obj,toplevel,indent)

% Error checks and defaults
if nargin < 2
   toplevel = class(obj); 
end
if nargin < 3
    indent = '';
end

% Initializations
str = '';
CR  = char(10);
di  = '  ';

% Convert to struct
if ~isstruct(obj)
    obj = struct(obj);
end

% Header
str    = [str indent toplevel ':' CR];
indent = [indent di];

if toplevel(1) == '-'
    indent = [indent di];
end
    

% Process fields.
fields = fieldnames(obj);
for i = 1:length(fields)
    field = fields{i};
    
    % Recurse or not?
    val = obj.(field);
    
    if isempty(val)
        % Blank
        str  = [str indent field ': ']; %#ok<AGROW>
        valstr = '';
    elseif isnumeric(val)
        % Convert to char
        str  = [str indent field ': ']; %#ok<AGROW>
        valstr = num2str(val);
    elseif ischar(val)
        str  = [str indent field ': ']; %#ok<AGROW>
        valstr = val;
    elseif isstruct(val)
        % Handle structure arrays
        len = length(val);
        if len > 1
            % Need another header
            str = [str indent field 's:' CR]; %#ok<AGROW>
            field = ['- ' field]; %#ok<AGROW>
            for j = 1:len
                valstr = struct2yaml(val(j),field,[indent di]);
                
                % Don't add last iteration here.
                if j<len
                    str = [str valstr CR]; %#ok<AGROW>
                end
            end

        else
            % Single value
            valstr = struct2yaml(val,field,indent);
            valstr = valstr(1:end-1);  % Drop last CR
        end
        
    elseif iscell(val)
        % Header
        valstr = [indent field ':' CR];
        
        % Drop into cell array
        % Assume char data for now.
        for j = 1:length(val)
            valstr = [valstr indent di '- ' val{j} CR];
        end % cell elements
        valstr = valstr(1:end-1);  % Drop last CR
    else
       % Have some non-simple data toplevel.
       % See if we can get a simple serialization with CHAR.
       % If not, recurse into.
        valstr = struct2yaml(val,field,[indent di]);
    end
    
    % Handle mult-line val string.
    
    % Write the value
    str = [str valstr CR]; %#ok<AGROW>
end


