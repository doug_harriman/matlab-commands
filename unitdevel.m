%UNITDEVEL  Tools for Units Toolbox development.
%   Supported commands:
%   SWAPPATH - Swaps path to units toolbox between TRUNK and set BRANCH
%   BRANCH   - Sets/gets current branch path.  With no inputs it will
%              returns the currently set branch.  Accepts a new branch name
%              as an input.
%   BRANCHES - Lists available branches
%   ROOT     - Sets/gets the current path to the root of the
%              tags/trunk/branches directory structure.


function [varargout] = unitdevel(cmd,varargin)

if nargin < 1
    help(mfilename);
    return;
end

% Constants
pref_group = 'UnitsToolboxDevelopment';

switch lower(cmd)
    case 'root'
        pref = 'RootPath';
        if nargin > 1
            setpref(pref_group,pref,varargin{1});
        else
            varargout{1} = getpref(pref_group,pref);
        end
    
    case 'branch'
        pref = 'BranchName';
        if nargin > 1
            branch = varargin{1};
            
            % Validate branch name
            branch_list = feval(mfilename,'Branches');
            if ~ismember(branch,branch_list)
                error(['Invalid branch: ' branch]);
            end
            
            % Save branch name
            setpref(pref_group,pref,branch);
            
        else
            varargout{1} = getpref(pref_group,pref);
        end

    case 'branches'
        % Generate list of available branches.
        
        % Get path to branches dir
        p = feval(mfilename,'Root');
        p = fullfile(p,'branches');
        
        % Get directories
        files = dir(p);
        branches = {};
        for i = 1:length(files)
            name = files(i).name;
           
            % Append name if valid
            if files(i).isdir && (name(1)~= '.')
                branches{end+1} = name;
            end
        end
            
        if nargout > 0
            varargout{1} = branches;
        else
            disp(branches);
        end
        
    case 'swappath'
        % Make sure a branch path has been set
        b = feval(mfilename,'Branch');
        if isempty(b)
            error('No branch set');
        end
        
        % Read current path variable and look for the trunk path.
        root       = feval(mfilename,'Root');
        trunk      = fullfile(root,'trunk');
        branchpath = fullfile(root,'branches',b);
        
        % If find it, swap to the branch.  If not, swap to the trunk.
        p = path;  % Current search path
        if ~isempty(strmatch(lower(trunk),lower(p)))
            % Trunk is on path
            rmpath(trunk);
            addpath(branchpath);
        else
            % Trunk is not on path
            try %#ok<TRYNC> 
                % may not be on path
                rmpath(branchpath);
            end
            addpath(trunk);
        end
        
    otherwise
        error(['Unsupported command: ' upper(cmd)]);
end