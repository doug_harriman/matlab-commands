%S1  Sets the current axes to subplot 1 of 2.
function [h] = s1(han)

if nargin < 1
   h=subplot(2,1,1);
   return;
end

% Verify handle type
if ~strcmp(get(han,'Type'),'axes') 
   error('Axes handle expected.');
end

% Create a hidden figure, create the axes to get the size, then use that size in
% the handle of interest.
fig = figure('Visible','off');
h = s1;
pos = get(h,'Position');
close(fig);
u = get(han,'Units');
set(han,'Units','Normalized');
set(han,'Position',pos);
set(han,'Units',u);
h = han;
