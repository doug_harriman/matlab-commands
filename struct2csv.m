%STRUCT2CSV  Writes a structure of arrays to a CSV file.
%  [DATA,HEADER]=STRUCT2CSV(STRUCT,FILENAME) writes the contents of the STRUCT of
%  arrays to the given FILENAME.
%

% Doug Harriman (doug.harriman@hp.com)
% 29-Oct-2008 - Created

function [str,header] = struct2csv(S,filename)

% Header: fields with units
str = '';
fields  = fieldnames(S);
n_field = length(fields);

% We expect a structure of arrays.  Handle an array of structures.
if(length(S) > 1)
    S = scalarstruct(S);
end

for i = 1:n_field
    var = fields{i};
    str = [str var]; %#ok<*AGROW>

    % Add units if have them
    val = S.(var);
    if isa(val,'unit')
        str = [str ' [' char(val) ']'];
    end

    % Comma if not end of line
    if i < n_field
        str = [str ', '];
    end
end
header = str;

% Total records
n_pts     = length(S.(fields{1}));

% Generate comma string
str_comma = char(ones(n_pts,1)*',');

% Data fields
str = [];
for i = 1:n_field
    % Get data type
    field = fields{i};
    val = S.(field)(1);

    % Special handling of cell arrays of unit objects.
    try
    if( isa(val{1},'unit') )
      
      
    end
    end
    
    
    % Convert to string based on type
    switch class(val)
        case 'cell'
            % Handle numeric values
            if isnumeric(val{1})
                % Convert to cellstr
                S.(field) = cellfun(@(x)num2str(x),S.(field),...
                    'UniformOutput',false);
            end
            
            % Have cellStr
            val = char(S.(field){:});
            
        case 'unit'
            val = double(S.(field));
            if ~iscol(val)
                val = val';
            end
            val = num2str(val);
        case 'double'
            num = double(S.(field));
            num = reshape(num,numel(num),1);
            val = num2str(num);
        case 'logical'
            val = S.(field);
            val = reshape(val,numel(val),1);
            val = num2str(val);
        otherwise
            error(['Unhandled data type: ' class(val)]);
    end
        
    % Write the value
    str = [str val];

    % Handle last value in line
    if i < n_field
        str = [str str_comma];
    end

end % i

% EOL
str_cr = char(10*ones(n_pts,1));
str    = [str str_cr];

% Convert from 2D char array to 1D array with CR's
str = reshape(str',1,numel(str));

% Write the file if given file name.
if exist('filename','var')
    % Write the header separately so we don't pad data with spaces to make
    % data lines as long as header line.
    fid = fopen(filename,'w');
    if fid < 0
        error(['Unable to open file for writing: ' filename]);
    end
    fprintf(fid,'%s\n',header);
    fprintf(fid,'%s',str);
    fclose(fid);
end