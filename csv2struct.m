%CSV2STRUCT  Reads CSV file into a structure of arrays.
%

% Doug Harriman (doug.harriman@hp.com)
% 17-JAN-10 - Gutted to use IMPORTDATA & apply units.
% 03-DEC-08 - Gutted to use EZREAD.  Now just applies units.
% 21-JUL-08 - Created

function [S,filename,header] = csv2struct(filename)

% Error checks
narginchk(0,1);
if nargin < 1
    [filename,pathname] = uigetfile('*.csv','Select CSV File');
    if filename == 0
        error('No file name selected.');
    end
    filename = [pathname filesep filename];
end

% Make sure we can find the file.
if ~exist(filename,'file')
    error(['Unable to find file: ' filename]);
end

% Read the data
data = importdata(filename,',');
fileheader = data.textdata;

% Need to convert any fields with unit specifiers '[' or '(' to unit
% objects.
S = struct;
for i = 1:size(data.data,2)
    % Grab header
    if numel(data.colheaders) >= i
        header = data.colheaders{i};
    else
        header = ['col_' int2str(i)];
    end
    
    % Process header
    % Look for units.
    res = regexp(header,'(?<var>[\w\s_])+\s[\[\(](?<ustr>[\w/\^\*]+)[\]\)]','names');
    if ~isempty(res)
        var  = res.var;
        ustr = res.ustr;
    else
        var  = header;
        ustr = '';
    end
    
    % Clean up var
    var = genvarname(var);
    
    % Get data
    val = data.data(:,i);
    
    % Apply units if have them.
    if ~isempty(ustr)
        val = unit(val,ustr);
    end
    
    % Store data
    S.(var) = val;
end 

% Tack on file name.
S.filename = filename;
header = fileheader;