%COMMAFORMAT  Formats a number with comma separators for every 3 digits.
% STR=COMMAFORMAT(NUM)
%
% STR=COMMAFORMAT(STR) converts a previously converted number.
%
% Swiped from:
% http://www.mathworks.com/support/solutions/en/data/1-35XMQ9/index.html?product=ML

function str = commaformat(num)

% Error checks.
error(nargchk(1,1,nargin));

% NO-OP if already have a char.
str = num2str(num);

% Add in commas.
FIN = min(length(str),find(str == '.')-1);
if isempty(FIN)
  % Have an integer
  FIN = length(str);
end

% Add in the commas.
for i = FIN-2:-3:2
  str(i+1:end+1) = str(i:end);
  str(i) = ',';
end

