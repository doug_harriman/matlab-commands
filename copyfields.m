%COPYFIELDS  Copies field values from one structure to another.
%  [STRUCT_B,IDX]=COPYFIELDS(STRUCT_A,STRUCT_B,FIELDNAMES) copies
%  FIELDNAMES from STRUCT_A to STRUCT_B.  IDX is a structure of indecies
%  into STRUCT_A and STRUCT_B which can be used for a speed optimized copy
%  if subsequent copies of the same fields from the same structures are
%  required.  The fieldnames to copy are case insensitive.  If none of the
%  fields listed in FIELDNAMES exist in one or the other structures,
%  STRUCT_B is returned unmodified.
%
%  [STRUCT_B,IDX]=COPYFIELDS(STRUCT_A,STRUCT_B) copies all fields which are
%  common between STRUCT_A and STRUCT_B.
%
%  [STRUCT_B]=COPYFIELDS(STRUCT_A,STRUCT_B,IDX) does the optimized copy
%  after an initial call as listed above.
%
%  This implementation is faster than looping through a list of field names
%  and copying values with dynamic field names.
%
%  See also: struct, commonfields.

% <USER_ID>
% </USER_ID>

function [struct_b,idx] = copyfields(struct_a,struct_b,fields)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'CodeTools';
TOOLBOX_MODULE  = '';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Trust the user if passed in index & don't do any error checking to
% optimize for speed.
if (nargin>2) && isstruct(fields)
    cell_a = struct2cell(struct_a);
    cell_b = struct2cell(struct_b);
    cell_b(fields.to) = cell_a(fields.from);
    struct_b = cell2struct(cell_b,fields.fieldnames);
    return;
end

% Error check inputs.
if ~isa(struct_a,'struct')
    error('Input to copy data from must be a strucure.');
end
if ~isa(struct_b,'struct')
    error('Input to copy data to must be a strucure.');
end
if (nargin>2) && ~isa(fields,'char') && ~isa(fields,'cell')
    error('Field names must be either a string or cell string array.');
end

% Get fields if not provided
if nargin < 3
    fields = commonfields(struct_a,struct_b);
end

% Early exit
if isempty(fields)
    return;
end

% Force lower case
fields = lower(fields);

% Find the from structure index
fieldnames_a  = lower(fieldnames(struct_a));
[tf,temp1]    = ismember(fieldnames_a,fields);
temp2         = find(temp1);
idx.from      = sortrows([temp1(tf) find(temp1)]);
idx.from      = idx.from(:,2);
if ~any(tf)
    return;
end

fieldnames_b   = lower(fieldnames(struct_b));
idx.fieldnames = fieldnames_b;
[tf,temp1]     = ismember(fieldnames_b,fields);
idx.to         = sortrows([temp1(tf) find(temp1)]);
idx.to         = idx.to(:,2);
if ~any(tf)
    return;
end

% Do the optimized copy.
struct_b = copyfields(struct_a,struct_b,idx);