%NUMINT Simple, fast numerical integration of vectors.
%
% Syntax:
%          z = numint(x,y,y0) 
% where:
%          y = f(x)
%
%              / 
%          z = | y dx 
%              /
%
%          y0 - initial value of y
%

function z = numint(x,y,y0)

if nargin == 2,
  y0 = 0 ;
end

q = pairsum(y)     ;  % Pair by pair sums through vector
q = q/2            ;  % Mean of q pairs
z = q.*diff(x)     ;  % Incremental gains
z = cumsum([y0 z]) ;  % Pointwise values




