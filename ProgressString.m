function    ProgressString( msg )

%   Successive calls, ProgressString( msg ), displays the character
%   string, msg, in the same position in the command window.
%
%   The first call, ProgressString( msg ), displays the character
%   string, msg, in the current position. Successive calls,
%   ProgressString( msg ), overwrites the previous string.
%
%   The call, ProgressString, issues a new line and "resets"
%   ProgressString.
%
%   ProgressString( msg ) where the argument, msg, is not a single
%   row character string displays the string, '******'.
%
%   Limitations:
%   1.  ProgressString might not produce a good result when called from
%       the command line
%   2.  A succession of calls, ProgressString( msg ), that is interupted
%       by other output to the command window may produce unwanted results.
%
%   Example:
%   Step = { 'first', 'second', 'last', [ 'x'; 'y' ], '', 'done' };
%   for step = Step,
%       ProgressString( char( step ) ),
%       pause( 1 ),
%   end
%   ProgressString
%
%   author:     per isakson
%   e-mail:     poi@bim.kth.se
%   created:    2003-07-05
%   modified:   2003-07-07

    persistent  nPos,   if isempty( nPos ), nPos = 0;   end,

    if  nargin == 1,
        if  and( ischar( msg ), size( msg, 1 ) == 1 ),
            fprintf( [ repmat( '\b', 1, nPos ), '%s' ], msg ),
            nPos = size( msg, 2 );
        else
            fprintf( [ repmat( '\b', 1, nPos ), '%s' ], '******' ),
            nPos = 6;
        end,
    else
        nPos = 0;   fprintf( '\n' ),
    end

return
