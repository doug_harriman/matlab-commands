%TRENDLINE  Plots trendline on plot for given data set.
%           
%  >> [polynomial_coefficients] = trendline(line_handle,fit_type,order)
%
%    fit_type: Either 'polynomial' or 'exponential'.
%    order   : Polynomial order.  Not used for exponential.
%

% DLH
% Created 10/01

function [coef] = trendline(dataHan,type,order)

% Validate data set
if ~strcmp(get(dataHan,'Type'),'line'),
    error('Data handle must be of type line.');
end

% Get data
x = get(dataHan,'XData');
y = get(dataHan,'YData');

% Hold state
nextPlot = get(gca,'NextPlot');
set(gca,'NextPlot','Add');

% Might error out, still want to restore hold state.
try,
    coef = datafit(x,y,type,order);
catch,
    coef = [];
end

% Restore hold
set(gca,'NextPlot',nextPlot);



% More general fitting function.
% Handles general polynomial fit, and exponential fit.
% Not debugged yet.
function coef = datafit(x,y,type,order)

% Make sure data is aligned correctly.
if (min(size(x)) ~= 1) | (min(size(y)) ~= 1),
    error('Data must be a vector');
end
if length(x) ~= length(y), 
    error('Data vectors must be same length');
end
if size(x,2) ~= 1,
    x=x';
end
if size(y,2) ~= 1,
    y=y';
end
if nargin < 3,
    error('X,Y and fit type required');
end

% Fit type
switch lower(type),
case 'polynomial'
    if nargin < 4,
        error('Polynomial order must be specified');
    end
    
    % Make sure have enough data
    numPts = length(x);
    if order+1 > numPts,
        error(['Cannot fit polynomial of order ' num2str(order),...
                ' with only ' num2str(length(x)) ' data points.']);
    end
    if order+1 == numPts,
        warning('Polynomial order at maximum for this number of data points.  Will yield a perfect fit.');
    end
    
    % Build matrix
    mat = ones(numPts,1);
    for i = 1:order,
        mat = [x.^i mat];
    end
    
    % Least sq soln
    coef = pinv(mat)*y;
    
    % Plot it
    x = sort(x);
    yest = coef(end);
    for i = 1:order,
        yest = yest + coef(end-i)*x.^i;
    end
    plot(x,yest,'r');    
    
    
case 'exponential'
    % Transform data
    y = log(y);
    coef = datafit(x,y,'polynomial',1);
    coef(2) = exp(coef(2));
    
otherwise
    error(['Unknown fit type: ' type]);
end