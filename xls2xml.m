%XLS2XML  Read XLS file and write data to XML.
%

function [] = xls2xml(filename)

if nargin < 1
    [file,path] = uigetfile('.xls','Select XLS file');
    if path == 0
        error('No file selected.');
    end
    filename = [path filesep file];
end

db = exceldb('SourceFile',filename);
[path,name,ext] = fileparts(filename);
writexml(db,[name '.xml']);  % Store sorted
