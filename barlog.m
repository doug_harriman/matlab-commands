%BARLOG   Bar plot with log scaled Y axis.
%         

% DLH
% 03/22/02 - Created

function [] = barlog(y)

% Generate bar plot, and get data back
b = bar(y);
vert = get(b,'vertices');

% Lower bound of data in Y
lowerBound = log(y);
lowerBound(abs(lowerBound) == Inf) = 0;
vert(vert == 0) = 10^(round(min(lowerBound)-1));

% Reset vertices and redraw
set(b,'vertices',vert);
set(gca,'yscale','log');
grid
