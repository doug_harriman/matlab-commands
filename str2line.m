%STR2LINE  Returns file line number(s) for given string.
%  LINE_NUM=STR2LINE(STRING,FILE_NAME)
%

% DLH
% 12/02/05 - Created

function [lines] = str2line(str_val,filename)

% Error check inputs
if nargin ~= 2, error('Two inputs expected.'); end
if ~isstr(str_val), error('Search value must be a string.'); end
if ~isstr(filename), error('File name must be a string.'); end

% Make sure we have a valid M file name.
[t1,t2,ext] = fileparts(filename);
if isempty(ext),
    filename = [filename '.m'];
end

% Read in the entire file.
fid = fopen(filename,'r');
if fid<0,
    error(['Unable to open file: ' filename]);
end
str_file = char(fread(fid)');
fclose(fid);

% Find the carriage returns.
CR = char(10);
ind_cr = findstr(str_file,CR);
n_lines = length(ind_cr);

% Find the occurences of string.
ind_val = findstr(str_file,str_val);
n_vals  = length(ind_val);

% Create matrix with 1 col of CR ind's for each ind_val
ind_cr = ind_cr'*ones(1,length(ind_val));

% Create matrix with 1 row of ind_val for each CR
ind_val = ones(n_lines,1)*ind_val;

% Generate matrix with 1's on the row that have an occurence of the value.
delta = diff(ind_val<ind_cr);
[lines,temp] = find(delta);
lines = lines+1;