%F2C  Centigrade to Fahrenheit conversion.
%

function C = f2c(F)

C = (F-32)*(5/9);
