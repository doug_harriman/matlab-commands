%PAIRSUM  Sums adjacent vector values.
%

function q = pairsum(v)

n = length(v) ;
q = reshape([1 -1]'*ones(1,n/2),1,2*round(n/2)) ;
q = q(1:length(q)-rem(n,2)) ;
q = -diff(q.*v).*q(1:length(q)-1) ;
