%CFG2DICT  Reads a Unix standard configuration file into a dictionary.
%  D=CFG2DICT(FILENAME,KEYVAR) reads the configuration file FILENAME into a
%  dictionary D.
%
%  D=CFG2DICT(STRING,KEYVAR) reads the configuration data in STRING into
%  dictionary D.
%
%  KEYVAR is optional and is used for multi record configuration files.  If
%  KEYVAR is not used, and array of dictionaries will be returned.  If
%  KEYVAR is specified, the value read for KEYVAR will be used to create a
%  dictionary of dictionaries, keyed by KEYVAR.  Note that the values of
%  KEYVAR must be unique or data will be overwritten.
%
%  Files should be of the following format:
%  <variable name> = <value>
%  One assignment per line.
%  New records denoted with '##'.
%  Comments start with '#' and extend to end of line.
%

% Doug Harriman (doug.harriman@hp.com)
% 31-JUL-08 - Created

function [D] = cfg2dict(str,keyvar)

% Error checks
error(nargchk(1,2,nargin));
if ~ischar(str)
    error('Input must be string');
end

% Determine if we have a file name or input string.
if exist(str,'file')
    str = file2str(str);
end

% Strip C comments.
str = regexprep(str,'/\*.*?\*/','');
str = regexprep(str,'//[^\n\r]*','');
str = strtrim(str);

% Strip config comments
str = regexprep(str,'#[^\n\r]*','');
str = strtrim(str);

% Eliminate blank lines
str = regexprep(str,char(13),'');
str = regexprep(str,[char(10) '{2,}'],char(10));

% Figure out what kind if dictionary we're going to build.

% Single record.
if isempty(findstr('##',str))
    D = Record2Dict(str);
    return;
end

% Multiple records
d = {};
while ~isempty(str)
    % Read a record.
    [rec,str] = strtok(str,'##');
    
    % Parse the record.
    d{end+1} = Record2Dict(rec); %#ok<AGROW>
end

% Just array requested?
if nargin < 2
    D = d;
    return;
end

% Want a dict of dicts.
D = dict;
for i = 1:length(d)
    % Find key value
    if iskey(d{i},keyvar)
        key = d{i}(keyvar);
        D(key) = d{i};
    end
end


function D = Record2Dict(str)
% Convert a single record to a dictionary.

% Blank dictionary
D = dict;

% Read line by line, inserting into dict.
CR = char(10);
while ~isempty(str)
    % Read next line
    [line,str] = strtok(str,CR); %#ok<STTOK>
    
    % Skip blank lines
    line = strtrim(line);
    if isempty(line)
        continue;
    end
    
    % Separate out var and value.
    [var,val] = strtok(line,'=');
    var = strtrim(var);
    val = strtrim(val(2:end));
    
    % Warn on mising value, but attempt to continue
    if isempty(val)
        warning(['No value set for variable, skipping: "' var '"']);
        continue;
    end
    
    % Convert value to correct Matlab data type.
    % Units
    idx1 = findstr('[',val);
    if ~isempty(idx1)
        unit_str = val(idx1+1:end-1);
        num = str2num(val(1:idx1-1)); %#ok<ST2NM>
        val = unit(num,unit_str);
    end
    
    % Numeric
    temp = str2double(val);
    if ~isnan(temp) && ~isempty(temp)
        val = temp;
    end
    
    % Logical
    if strcmpi(val,'true')
        val = true;
    end
    if strcmpi(val,'false')
        val = false;
    end
    
    % Cell
    if ischar(val) && (val(1)=='{') && (val(end)=='}')
        val = eval(val);
    end
        
    % Insert into dict
    D(var) = val;
end
    
    