%SYNCSIZE  Synchronize figure sizes.

function syncsize

global sourcefig;

% Clear source figure handle
sourcefig = [];

% Set callbacks.
SetCallback(@ClickHandler);

% Instructions to user
disp('Click figure with desired size');

end

function ClickHandler(varargin)

global sourcefig

% Determine if this is first or second click.
if isempty(sourcefig)
    % First click
    sourcefig = gcf;
    
    % UI
    disp('Select figure to resize');
    return;
end

% Must have been second click.
% Get size info from source figure.
u = get(sourcefig,'units');
p = get(sourcefig,'position');

% Set new figure.
set(gcf,'units',u);
p_old = get(gcf,'position');
dy = p(4) - p_old(4);


p = [p_old(1) p_old(2)-dy p(3:4)];
set(gcf,'position',p);

% Clear callbacks
SetCallback([]);

end


function SetCallback(fcn)

% Get all figures
figs = get(0,'Children');

% Set their callbacks
set(figs,'ButtonDownFcn',fcn);

% Get axes children of figures.
for i = 1:length(figs)
    % Get axes objects.
    ax = findobj(get(figs(i),'Children'),'Type','axes');

    % Set their callbacks too.
    set(ax,'ButtonDownFcn',fcn)
end

end