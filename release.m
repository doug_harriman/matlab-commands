%RELEASE  Prepare a software release.
%   Release updates any required release code tags for all M files in a given
%   directory.
%   RELEASE(PATH,STRUCT) updates code tags defined in STRUCT using CODEGEN
%   for all M files in the directory and all sub directories specified by
%   PATH.
%
%   See also: codegen.

% Author: Doug Harriman (doug.harriman@hp.com)
%

function [] = release(P,S)

% Validate input
if ~(exist(P,'dir') == 7)
    error('First argument is path to directory for release.')
end

if ~isstruct(S)
    error('Second argument must be structure of code tags to update.');
end

% Get full directory list
dir_list = subdirs(P);

% Get full file list
file_list = filelist('*.m',dir_list);

for i = 1:length(file_list)
    codegen(file_list{i},S);
end