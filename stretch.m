%STRETCH  Makes a plot fill the full x axis.
%

% DLH

function [] = stretch(axhan)

% error check
if nargin==0,
   axhan = gca ;
end

% Get max x point
linehan = get(axhan,'children') ;
linehan = linehan(1) ;
data = get(linehan,'xdata') ;

% Set the limit
xlim = get(axhan,'xlim') ;
set(axhan,'xlim',[min(data) max(data)]) ;



