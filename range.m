%RANGE  Range limits for a vector.

function [r] = range(vect)

r = max(vect(:))-min(vect(:));