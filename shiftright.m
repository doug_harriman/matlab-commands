%SHIFTRIGHT  Takes a binary string and right shifts the number of bits specified.
%
%>> output = shiftright(input,bitShift)
%

function [out] = shiftright(in,bits)

% Error check
if ~isstr(in),
   error('SHIFTRIGHT works only on binary strings.');
end

% Do the shift
out = in(1:length(in)-bits);
out = [char(ones(1,bits)*48) out];
