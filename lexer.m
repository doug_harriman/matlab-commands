%LEXER  Infix mathematical expression lexical analyzer.
%

% Doug Harriman (doug.harriman@gmail.com)

classdef lexer < hgsetget
    properties
        % String to lex.
        string;
        
        % User specified data
        userdata;
        
    end % properties - public
    
    properties (Access=private)
        % Identified string stack.
        stack = stack;

    end % properties - private
    
    methods
        function obj = lexer(varargin)
            %LEXER  Infix mathematical expression lexical analyzer constructor.
            %
            % To create the lexer and analyze the string in one operation:
            %   >> l = lexer('<string'>;
            %
            % In two operations:
            %   >> l = lexer;
            %   >> l.string = '<string>';
            %
            
            % Error check inputs
            error(nargchk(0,1,nargin));
            
            % If we have an arg, set the string.
            if nargin == 0
                return;
            end
            
            % Set the string
            obj.string = varargin{1};
            
        end % constructor
        
        function set.string(obj,str)
            %SET.STRING  Set string to be tokenized.
            %
            
            % Error checks.
            if ~ischar(str)
                error('Character string expected.');
            end
            
            % Store it
            obj.string = str;
            
            % Run the analysis
            try
                obj.tokenize; %#ok<MCSUP>, this clears the stack.
            catch err
                % Rethrow the error from here to minimize call stack.
                throw(err);
            end
            
        end % set.string
        
        function val = get.string(obj)
            %GET.STRING  Return string that to be analyzed.
            %
            
            val = obj.string;
            
        end % get.string
        
        function val = next_token(obj)
            %NEXT_TOKEN  Return next token.
            %  Returns [] if no tokens left.
            
            val = obj.stack.pop;
            
        end % next_token
        
        function val = getstack(obj)
            %GETSTACK  Return lexer stack.
            %
            
            val = obj.stack;
            
        end % getstack
        
        function disp(obj)
            %DISP  Display the lexer.
            %
            
            % Header
            disp('Lexer');
            if isempty(obj.string)
                disp( 'String: <EMPTY>');
            else
                disp(['String: ' obj.string]);
            end
            disp(['Tokens: ' int2str(length(obj.stack))]);
            
        end % disp
        
        function display(obj)
            %DISPLAY  Full lexer display control.
            %
            
            disp(obj);
            
        end % display
        
    end % methods - public
    
    methods (Access = protected)
        function tokenize(obj)
            %TOKENIZE  Perform infix mathematical lexical analysis on object string property.
            %
            
            % Local copy of string to lex.
            str = obj.string;
            
            % Clear the stack
            obj.stack = stack;
            
            % Run through each character in the string.
            % Use an index into the string instead of repeatedly truncating
            % the string, as that requires many memory copies.
            ch_idx = 0;
            while ch_idx < length(str);
                % Read next char.
                ch_idx = ch_idx + 1;
                ch  = str(ch_idx);
                
                % Classify the character
                if ch == '('
                    % Classify object.
                    tok.type = 'PAREN_OPEN';
                    tok.val  = ch;
                    
                    % Store it
                    obj.stack.push(tok);

                elseif ch == ')'
                    % Classify object.
                    tok.type = 'PAREN_CLOSE';
                    tok.val  = ch;
                    
                    % Store it
                    obj.stack.push(tok);
                    
                elseif isstrprop(ch,'alpha') || (ch=='_')
                    % Have a variable name.
                    tok.type = 'ID';
                    tok.val  = ch;
                    
                    % Keep reading while we have chars.
                    while(ch_idx < length(str))
                        % Get next char.
                        ch_idx = ch_idx + 1;
                        ch = str(ch_idx);
                        
                        if(isstrprop(ch,'alphanum') || (ch=='_') )
                            % Store char
                            tok.val = [tok.val ch];
                        else
                            % Push last character back.
                            ch_idx = ch_idx - 1;
                            break;
                        end % if
                    end % while
                    
                    % Push the variable onto the stack.
                    obj.stack.push(tok);
                    
                elseif isstrprop(ch,'digit') || (ch=='.')
                    % Have a number.
                    tok.type = 'CONST';
                    tok.val  = ch;
                    
                    % Keep reading while we have valid chars.
                    % Read the next char.
                    while(ch_idx < length(str))
                        % Get next char.
                        ch_idx = ch_idx + 1;
                        ch = str(ch_idx);
                        
                        if (isstrprop(ch,'digit') || (ch=='.') )
                            % Store char.
                            tok.val = [tok.val ch];
                            
                        else
                            % Push last character back.
                            ch_idx = ch_idx - 1;
                            break;
                        end % if
                    end % while
                    
                    % Push the number onto the stack.
                    obj.stack.push(tok);
                    
                elseif ismember(ch,{'+','-'})
                    % Classify operator
                    tok.type = 'OP_ADD';
                    tok.val  = ch;

                    % Store it.
                    obj.stack.push(tok);

                elseif ismember(ch,{'*','/'})
                    % Classify operator
                    tok.type = 'OP_MULT';
                    tok.val  = ch;

                    % Store it.
                    obj.stack.push(tok);

                elseif ismember(ch,{'^'})
                    % Classify operator
                    tok.type = 'OP_EXP';
                    tok.val  = ch;

                    % Store it.
                    obj.stack.push(tok);

                elseif ch == ' '
                    % Ignore spaces, but keep as a character class so that
                    % spaces can be used as delimiters.
                else
                    error(['Unsupported character: ' ch]);
                end
                
            end % string read loop
            
            % Flip the stack so it is a FIFO container.
            obj.stack.flipud;
            
        end % method tokenize
    end % methods - protected
end % classdef