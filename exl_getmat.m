function a=exl_getmat(target,region,host)
% - C=exl_getmat(filespec,region,'Machine')
% - filespec is the file to read from. default directory = pwd
%                                      default extension = 'xls'
% - Region should be of the form:'Sheet2!R2C4:R3C6'
%    o If region is empty or non-exist, the excel worksheet becomes
%      visible to enable manual selection. You will be promped then to 
%      enter any letter on the Matlab command window to continue.
%   o  For the first worksheet of the workbook you can omit
%      the worksheet address.
% - Machine is needed only for network.
% - The output is a cell array.
% To convert a numeric cell array into a matrix use the (private) function
% 'cell2num'.
%examples:
%----------------
% 1) standard operation:
% M=exl_getmat('c:\mydocu~1\tryset.xls','R3C5:R6C8');
% 2) Using interactive selection with default extension (.xls)
% >>  M=exl_getmat('c:\mydocu~1\tryset');
%  {Here you have to select the region at the pop-up EXCEL window}.
%    Enter any letter when finish selecting f
% 3) Using default file on the present working directory for interactive selection
%   � M=exl_getmat;
%  {Here you have to select the region at the pop-up EXCEL window}.
%   Enter any letter when finish selecting f
% Note:In these examples it was assumed that the Excel Workbooks exist.

% This program was tested under versions 5.2 and 5.3 on PC with WIN95 and OFFICE97.
%
% Created at: sept. 1999  by
%                         |
%                         V
%          e-mails: jonathan@ndc.soreq.gov.il
%                   bar-sagi@actcom.co.il

v=version;
v1 = str2num(v(1));
if v1<5
   disp(' Incorrect Matlab version')
   return
end

if nargin==3
   s=actxserver('Excel.Application',host);
else
   s=actxserver('Excel.Application');
end
if nargin>=1,
   [D,fil,ext]=fileparts(target);
   if isempty(D)
      D=pwd;
   end
   if isempty(ext)
      ext = 'xls';
   end
   target = fullfile(D,[fil '.' ext]);
else
   target = fullfile(pwd,'mat_get_set.xls');
end
% set(s,'Visible',1);
sb = invoke(s.workbooks,'Open',target);
if exist('region') & ~isempty(region)
   invoke(s,'goto',region);
else
   set(s,'Visible',1);
   g=input('Enter any letter when finish selecting ','s');
end

a=get(s.selection,'Value');
set(s,'Visible',0);

invoke(sb,'Close');
% Quit Excel
invoke(s,'Quit');

% Close Excel and terminate ActiveX:
delete(s);
return
