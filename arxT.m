%ARXT System identification of SISO system using ARX least squares.  
%     Data (u,y) must be sampled on a regular interval.  
%
%>>sysout = arx(sysin,t,u,y)
%    sysout - Identified System, LTI structure
%    sysin  - A system of the structure to be identified, LTI structure.
%    t      - Data time vector.
%    u      - Input to unknown system.
%    y      - Output of unknown system.
%

function sysout = arx(sys,t,u,y)

% Sample rate
samp_t = mean(diff(t)) ;

% Convert to discrete TF
if isct(sys),
   sys = c2d(sys,samp_t) ;
end

% Convert to TF in z^-1
sys = tf(sys)               ;
set(sys,'variable','z^-1')  ;
[num,den] = tfdata(sys,'v') ;

% Number of poles and zeros
n_ind = -find(den~=0)+1       ;  % Outputs that affect current output.
n_ind = n_ind(find(n_ind~=0)) ;  % Remove 0 index
m_ind = -find(num~=0)+1       ;  % Inputs that affect current output.
n     = max(abs(n_ind))       ;  % denominator order
m     = max(abs(m_ind))       ;  % numerator order

% Loop through data for summations
for i = n+1:length(t),
   
   % Form Phi vector
   phi = [-y(i+n_ind)' u(i+m_ind)']' ;

   % Numerator summation
   if (i==n+1), % Initialization to correct size
      sum_top = phi*y(i) ;
      sum_bot = phi*phi' ;
   else,
      sum_top = sum_top + phi*y(i) ;
      sum_bot = sum_bot + phi*phi' ;
   end
   
end

% ARX Least Squares Estimates
theta = inv(sum_bot)*sum_top ;

% Parameters
n = length(n_ind) ;  % Number of parameters in denominator.
m = length(m_ind) ;  % Number of parameters in numerator.
a(abs(n_ind)+1) = theta(1:n)     ;  % Parameter extraction.
a(1) = 1                         ;  % First param always 1.
b(abs(m_ind)+1) = theta(n+1:n+m) ;  % Parameter extraction.

% Conversion to LTI system
sysout = tf(b,a,'variable','z^-1','Ts',samp_t) ;

