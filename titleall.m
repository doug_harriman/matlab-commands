%TITLEALL Gives all open figures the specfied title.
%

% DLH
% 02/14/01 - Created

function [] = titleall(txt)

h = findobj(get(0,'children'),'Type','Figure');
for i =1:length(h),
    figure(h(i))
    title(txt);
end
