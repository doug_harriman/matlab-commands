%GA  Genetic Algoirthm optimization class.
%  Individuals must be able be to be stored in an array.  Objects,
%  structures and row vectors are all valid individual descriptions.
%
%  Population makeup is controlled by three parameters: ratioElite,
%  ratioMutant and ratioChildren.  Each of these values must be on the
%  range [0,1].  The sum of these ratios should be  <= 1.0.  If <1, the
%  remainder of the population will be filled by new, random individuals.
%
%  All helper functions are required to take a GA object as the first
%  input.  The GA class is derived from the DYNAMICPROPS class, so the user
%  may add additional properties to the class to pass additional
%  configuration information to the helper functions.

% Created by: Doug Harriman (doug.harriman@simplexitypd.com)
%
% Credits:  Inspired by AGA 1.0 by Manel Soria - ETSEIAT
%           Matlab Central contribution 43743
%           http://www.mathworks.com/matlabcentral/fileexchange/43743-aga-1-0
%

classdef ga < handle & dynamicprops
    properties
        
        % Ratio of elite (most fit) individuals to the overall population.
        % The elite portion of the population is retained between generations.  
        % This value must be on range [0,1].  
        % Default = 0.2.
        ratioElite = 0.2;
        
        % Overall population ratio which will be created by mutating individuals of the elite population.  
        % This value must be on range [0,1].
        % Default = 0.3.
        ratioMutant = 0.3;
        
        % Overall population ratio which will be created by the reproduction of two existing individuals.  
        % This value must be on range [0,1].
        % Default = 0.3.
        ratioChildren = 0.3;
        
        % Scalar value representing the target fitness value.  
        % If non-empty, the optimization will stop if this value is reached.
        % Default = [].
        goal
        
        % Sets the number of generations with no change in fitness at which to stop the optimization.  
        % Default = Inf.
        stagnationLimit = Inf;
        
        % Function handle used to determine the fitness of an individual.
        % The function takes two inputs: a GA object and an individual.
        % The function must return a scalar value that represents the
        % fitness of the individual.  This function is required.
        FcnFitness
        
        % Function handle used to create a new, random individual.  
        % The function takes a GA object as an input, and returns a new, 
        % randomly created individual.  This function is required.
        FcnRandom
        
        % Function handle used to create a new individual from an existing individual.  
        % The function takes two inputs: a GA object and individual.  The 
        % function must return a new individual.  This function is required 
        % if RATIOMUTANT is non-zero.
        FcnMutate
        
        % Funcdtion handle used to create a new individual from two existing individuals.  
        % The function takes three inputs: a GA object and two parent 
        % individuals.  The function returns a new individual that was 
        % created from the two parents.  This function is required if 
        % RATIOCHILDREN is non-zero.
        FcnReproduce
        
        % Function handle used to remove repeated individuals from the population.  
        % The function takes a GA object as input and returns a new 
        % population with no repeated individuals.  If empty, this step is 
        % skipped.  
        % Default = [].
        FcnUnique 
        
        % Function handle to log current generation data.
        % User definable handler to display or log status data.  Takes a GA
        % object as input.
        % Default = ga.log
        FcnLog
        
        % Function handle to dislpay an individual.
        % Takes an individual as input and returns a string.
        % Default = []
        FcnDisplayIndividual
        
    end % properties - public
    
    properties (SetAccess=protected)
        % Number of individuals in the popultion.
        szPopulation = 0;
        
        % Overall population of individuals, sorted from most to least fit.
        population
        
        % Sorted array of fitness values for all individuals in the population.
        fitness
        
        % Current generation
        generationCurrent
        
        % Total generations requested.
        generationTotal
        
    end % properties - set protected
        
    methods

        function obj = ga()
            %GA  Genetic algorithm object contstructor.
            %
            
            % Set default logging function, as can't do that at start.
            obj.FcnLog = @obj.log;
            
        end % ga - constructor
        
        function init(obj,nIndividuals)
           %INIT  Initializes the population to size NINDIVIDUALS. 
           %
           
           % Create random individuals
           % We don't know what an individual will be, only that it can be
           % stored in an array, thus the array must grow at each
           % iteration.
           for i = 1:nIndividuals
               obj.population(i,:) = obj.FcnRandom(obj);
           end
           obj.szPopulation = size(obj.population,1);
           obj.fitness = ones(obj.szPopulation,1)*NaN;
           
        end
        
        function exitflag = run(obj,nGeneration)
            %RUN  Runs the genetic algorithm for NGENERATION generations.
            %  [EXITFLAG]=RUN(NGENERATION) runs the defined
            %  optimization problem for NGENERATION generations returning
            %  the stop condition for the algorithm.
            %
            %  EXITFLAG = 0 => Fitness goal achieved.
            %  EXITFLAG = 1 => NGENERATIONS completed.
            %  EXITFLAG = 2 => Population stagnation limit reached.
            %
            
            % Error checks.
            narginchk(2,2);
            if(obj.szPopulation < 1)
                error('Population not initialized.  See help ga.init.');
            end
            if(nGeneration < 1)
                error('Must run at least 1 generation.');
            end
            obj.generationTotal = nGeneration;
            
            if(obj.ratioElite + obj.ratioMutant + obj.ratioChildren > 1)
                error('Sub-population ratios exceed 1.0');
            end

            % Calculation sub-population sizes.
            szElite    = floor(obj.ratioElite    * obj.szPopulation);
            szMutant   = floor(obj.ratioMutant   * obj.szPopulation);
            szChildren = floor(obj.ratioChildren * obj.szPopulation);
            szRandom   = obj.szPopulation - szElite - szMutant - szChildren;
            assert(szRandom >= 0);
            
            % Calculate initial population fitness.  
            fit = ones(obj.szPopulation,1)*NaN;
            for iIndividual = 1:obj.szPopulation
                fit(iIndividual) = obj.FcnFitness(obj,obj.population(iIndividual,:));
            end
            [obj.fitness,idx] = sort(fit);
            obj.population = obj.population(idx,:);
            fitnessBest    = obj.fitness(1);
            
            % Generational loop
            exitflag = 1;  % Default.
            fitnessGenerationCount = 1;

            for iGen = 1:obj.generationTotal
                % Track current generation for UI stuff.
                obj.generationCurrent = iGen;
                
                %% Check termination criteria.
                if(obj.fitness(1) <= obj.goal)
                    exitflag = 0;
                    break;
                end
                
                if(fitnessGenerationCount >= obj.stagnationLimit)
                    exitflag = 3;
                    break;
                end
                
                %% Create new population.
                % Mutants
                if (szMutant > 0)
                    mutants = arrayfun(@(x)obj.FcnMutate(obj,x),...
                        obj.population(randi(szElite,szMutant,1)));
                else
                    mutants = [];
                end
                
                % Children
                if (szChildren > 0)
                    children = arrayfun(@(mom,dad)obj.FcnReproduce(obj,mom,dad),...
                        obj.population(randi(szElite+szMutant,szChildren,1)),...
                        obj.population(randi(szElite+szMutant,szChildren,1)));
                else
                    children = [];
                end
                
                % Remove redundant individuals.
                if ~isempty(obj.FcnUnique)
                    warning('Removal of random individuals not supported yet.');
                end
                
                % New randoms.
                % NOTE: This should also fill in removed redundants.
                for i = 1:szRandom
                    randoms(i,:) = obj.FcnRandom(obj); %#ok<AGROW>
                end
                
                % Consolidate into the new population.
                obj.population = [obj.population(1:szElite,:); ...
                    mutants;...
                    children;...
                    randoms];
                
                %% Assess population fitness.
                fit = arrayfun(@(x)obj.FcnFitness(obj,x),obj.population(szElite+1:end,:));
                [obj.fitness,idx] = sort([obj.fitness(1:szElite);fit]);
                obj.population = obj.population(idx,:);

                % Track stagnation.
                if(obj.fitness(1) == fitnessBest)
                    fitnessGenerationCount = fitnessGenerationCount + 1;
                else
                    fitnessBest = min(obj.fitness(1),fitnessBest);
                    fitnessGenerationCount = 1;
                end
                
            end % generation loop
            
        end % run
        
        function set.ratioElite(obj,value)
            narginchk(2,2);
            if ~isscalar(value)
                error('Scalar value expected.');
            end
            if (value < 0) || (value > 1)
                error('Value must be on range [0,1]');
            end
            
            obj.ratioElite = value;
        end
            
        function set.ratioMutant(obj,value)
            narginchk(2,2);
            if ~isscalar(value)
                error('Scalar value expected.');
            end
            if (value < 0) || (value > 1)
                error('Value must be on range [0,1]');
            end
            
            obj.ratioMutant = value;
        end
        
        function set.ratioChildren(obj,value)
            narginchk(2,2);
            if ~isscalar(value)
                error('Scalar value expected.');
            end
            if (value < 0) || (value > 1)
                error('Value must be on range [0,1]');
            end
            
            obj.ratioChildren = value;
        end
        
        function set.goal(obj,value)
            narginchk(2,2);
            if ~isscalar(value)
                error('Scalar value expected.');
            end
            if (value < 0) || (value > 1)
                error('Value must be on range [0,1]');
            end
            
            obj.goal = value;
        end
        
        function set.stagnationLimit(obj,value)
            narginchk(2,2);
            if ~isscalar(value)
                error('Scalar value expected.');
            end
            
            % Empty allowed.
            if isempty(value)
                obj.stagnationLimit = value;
                return;
            end
            
            if (value < 1)
                error('Value must be positive integer');
            end
            value = round(value);
            
            obj.stagnationLimit = value;
        end
        
        function log(obj)
            %LOG  Default generation data logging.
            %

            if ~isempty(obj.FcnDisplayIndividual)
                fprintf('Gen=%3d/%3d, Fitness=%f\n',...
                    obj.generationCurrent,...
                    obj.generationTotal,...
                    num2str(obj.fitness(1)) );
            else
                fprintf('Gen=%3d/%3d, Fitness=%f, Best=%s\n',...
                    obj.generationCurrent,...
                    obj.generationTotal,...
                    num2str(obj.fitness(1)),...
                    obj.FcnDisplay(obj.population(1)));
            end
            
        end % log
            
    end % methods - public
    
end % classdef