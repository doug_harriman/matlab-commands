%BLOCKPREPEND  Prepends each line of text with given characters.
%

function str = blockprepend(str,prefix)

% Error checks
error(nargchk(2,2,nargin));
if ~ischar(str) || ~ischar(prefix)
    error('Character arrays expected.');
end

% Replace all CR's with chars + CR
CR = char(10);
str = strrep(str,CR,[CR prefix]);  % Most lines
str = [prefix str];                % First line
str = str(1:end-length(prefix));   % Clean last line
