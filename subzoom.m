%SUBZOOM  Interactively adds a zoomed window to a plot.
%
% >>new_axes_handle = subzoom
%

% DLH
% 09/18/01 - Updated from original to work with Matlab 6.1

%TODO: figure out how to use rbbox.
% Drawing lines from corners of selected region to corners of subplot.
% 1) each corner has a mating corner (top-left to top-left, etc)
% 2) find the convex polygon of all 8 points, the four corners of each the
%    selection and the subplot.
% 3) If both mating corners from (1) are on the convex polygon, connect
%    them with a line.

% Original
% File : subzoom.m
% Written by : Doug Harriman
%              Graduate Student Researcher
%              Mechanical Engineering
%              UC Berkeley
%              harriman@euler.berkeley.edu
%
% Written on : 11/13/95
%
% Purpose : Allows an arbitrarily placed
%           zoomed in subplot in main plot.
%           For 2-D plots only.
%
% Sytax   : >>handle = subzoom
%
% Where   :'handle' is the handle of the 
%           new axes object.
% 

function axhan = subzoom()
 
% Subplot title
title = 'Zoomed Region' ;

% Move to correct figure
parent = gcf;
figure(parent);

% Disable any widowbutton down functions
oldFcn = get(gcf,'windowbuttondownfcn') ;
set(gcf,'windowbuttondownfcn',' ') ;

% Get zoom box
disp('Select Region to Zoom')
waitforbuttonpress            ;
pnt = get(gcf,'currentpoint') ;
xy1 = get(gca,'currentpoint') ;
rbbox([pnt 0 0],pnt)          ;
xy2 = get(gca,'currentpoint') ;

% Clean up data
xy1 = xy1(1,1:2) ;
xy2 = xy2(1,1:2) ;
xx  = [xy1(1) xy2(1)] ;
yy  = [xy1(2) xy2(2)] ;

% Get new plot area
disp('Select Area for Subplot')
waitforbuttonpress ;
pnt1 = get(gcf,'currentpoint') ;
rbbox([pnt1 0 0],pnt1) ;
pnt2 = get(gcf,'currentpoint') ;

% Reset fcn
set(gcf,'windowbuttondownfcn',oldFcn) ;

% Clean up data
pntx = sort([pnt1(1) pnt2(1)]) ;
pnty = sort([pnt1(2) pnt2(2)]) ;

% Need to map figure points to main axes points for patch.
% Create patch to go under new plot
%patch([pntx(1) pntx(1) pntx(2) pntx(2)],...
    %[pnty(1) pnty(2) pnty(2) pnty(1)],'w');


% Create axes
mainaxhan = gca ;
pos = get(gcf,'position') ;
corner = [min(pntx)/pos(3) min(pnty)/pos(4)] ;
width  = [abs(diff(pntx))/pos(3) abs(diff(pnty))/pos(4)] ;
axhan = axes('position',[corner width],'box','on') ;

% Set fontsize
factor = 2/3 ;
size = get(mainaxhan,'fontsize') ;
set(axhan,'fontsize',factor*size) ;

% Set Limits
axis([min(xx) max(xx) min(yy) max(yy)]) ;

% Set plot type 
set(axhan,'xscale',get(mainaxhan,'xscale')) ;
set(axhan,'yscale',get(mainaxhan,'yscale')) ;

% Set dataaspectratio
% Comment this out if you want to set you own aspect ratio
%aspect = get(mainaxhan,'dataaspectratio') ;
%set(axhan,'dataaspectratio',aspect) ;

% Get Data to plot
kids = get(mainaxhan,'children') ;
hold on
for i = 1:length(kids),

    if strcmp(get(kids(i),'type'),'line'),

        xdata = get(kids(i),'xdata') ;
        ydata = get(kids(i),'ydata') ;
        color = get(kids(i),'color') ;
        linestyle = get(kids(i),'linestyle') ;
        linewidth = get(kids(i),'linewidth') ;

        plothan = plot(xdata,ydata) ;

        set(plothan,'color',color) ;
        set(plothan,'linestyle',linestyle) ;
        set(plothan,'linewidth',linewidth) ;

    elseif strcmp(get(kids(i),'type'),'patch'),

        CData     = get(kids(i),'Cdata')     ;
        EdgeColor = get(kids(i),'EdgeColor') ;
        FaceColor = get(kids(i),'FaceColor') ;
        LineWidth = get(kids(i),'LineWidth') ;
        XData     = get(kids(i),'XData')     ;
        YData     = get(kids(i),'YData')     ;
  
        patchhan  = patch(XData,YData,FaceColor) ;

        set(patchhan,'CData',CData)          ;
        set(patchhan,'EdgeColor',EdgeColor)  ;
        set(patchhan,'FaceColor',FaceColor)  ;
        set(patchhan,'LineWidth',LineWidth)  ;

    end

end
hold off

% Put box around zoomed region in main plot
% comment out if you don't like this

% Swith to correct axes
axes(mainaxhan) ;

% Vertical lines
lh(1) = line([1 1]*min(xx),[max(yy) min(yy)]) ;
lh(2) = line([1 1]*max(xx),[max(yy) min(yy)]) ;

% Horizontal lines
lh(3) = line([max(xx) min(xx)],[1 1]*min(yy)) ;
lh(4) = line([max(xx) min(xx)],[1 1]*max(yy)) ;

% Set colors
set(lh,'color','k') ;

% Label it
titlehan = get(axhan,'title') ;
set(titlehan,'string',title,...
    'fontsize',size*factor+3) ;

% Bring subplot up
axes(axhan);

% End of file
