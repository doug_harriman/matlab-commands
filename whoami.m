%WHOAMI		display user id and retrieve system properties
%
%		WHOAMI displays the full user id in the form
%		username|domain|hostname|matlabversion|date
%
%		WHOAMI optionally returns various system properties
%		
%SYNTAX
%-------------------------------------------------------------------------------
%		    WHOAMI
%		W = WHOAMI
%		W = WHOAMI(X)
%
%INPUT
%-------------------------------------------------------------------------------
% X	:	any number or character
%
%
%OUTPUT
%-------------------------------------------------------------------------------
% W	:	structure with contents according to calling syntax
%		WHOAMI		= full user id and system properties
%		WHOAMI(1)	= full user id only
%
%NOTE
%-------------------------------------------------------------------------------
%		- the JAVA engine must be loaded
%		- system properties are retrieved from
%		  NTSystem
%		  InetAddress
%		  System	  
%
%EXAMPLE
%-------------------------------------------------------------------------------
%		whoami;
% %		us|USZ|ws-nos-36362|7.6.0.324.R2008a|17-Jun-2008 21:36:45
%
%		u=whoami;
% %	display a few entries
%		u.os
% %			     arch: 'x86'
% %			     name: 'Windows XP'
% %			  version: '5.1'
%		u.file
% %			 encoding: 'Cp1252'
% %			separator: '\'

% created:
%	us	01-Feb-1988 us@neurol.unizh.ch
% modified:
%	us	17-Jun-2008 21:36:45

%-------------------------------------------------------------------------------
function	p=whoami(varargin)

% check JAVA
	if	~usejava('jvm')
		disp('WHOAMI> java engine not loaded');
	if	nargout
		p='';
	end
		return;
	end

		import	java.lang.*;
		import	com.sun.security.auth.module.*;
		import	java.net.*;

		d=datestr(clock);
		v=regexprep(version,{'(',')',' '},{'','','.'});

		e=NTSystem;
		p.id='';
		p.name=char(e.getName);
		p.domain=char(e.getDomain);

		e=InetAddress.getLocalHost();
		p.host=char(e.getHostName);
		p.ver=v;
		p.runtime=d;

		uid=sprintf('%s|%s|%s|%s|%s',p.name,p.domain,p.host,p.ver,p.runtime);

	if	~nargout
		disp(uid);
		clear p;
	else
		p.id=uid;
	end
	if	nargin					||...
		~nargout
		return;
	end

		w=warning('off');			%#ok
		e=System.getProperties.keys;
	while	e.hasNext
		id=e.nextElement;
		s=strread(char(id),'%s','delimiter','.');
		cp=char(System.getProperty(id));
		cp=WHOAMI_mkctrl(cp);
		sf=struct('type','.','subs',s);
		p=subsasgn(p,sf,cp);
	end
		warning(w);

	if	~nargout
		clear p;
	end
end
%-------------------------------------------------------------------------------
function	s=WHOAMI_mkctrl(s)

% replace non-printable characters

		ix=s<' ';
	if	any(ix)
		dp=s;
		dp(ix)='.';
		dp=cellstr(dp.');
		np=strread(sprintf('CTRL(%d)\n',s(ix)),'%s');
		dp(ix)=np;
		s=cat(2,dp{:});
	end
end