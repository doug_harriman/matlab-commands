%STRUCT2DICT  Convert structure to dictionary.
%  D=STRUCT2DICT(S) Converts the fields of structure S to keys of
%  dictionary D if S is a scalar structure.
%
%  D=STRUCT2DICT(S,KEYFIELD) Converts structure array S to a dictionary
%  keyed by the values of field KEYFIELD.
%

% Doug Harriman (doug.harriman@gmail.com)

function D = struct2dict(S,key)

% Validate scalar case
switch nargin
    case 1
        if ~isscalar(S)
            error('Key field specification required for structure arrays.');
        end
        
        D = dict;
        
        % Loop over fields.
        keys = fields(S);
        vals = struct2cell(S);
        for i = 1:length(keys)
            D(keys{i}) = vals{i};
        end
        
    case 2
        if isscalar(S)
            warning('Key field specification ignored for scalar structures.');
        end
        
        % Make sure struct has key field.
        if ~isfield(S,key)
            error(['Structure has no field: ' key]);
        end
        
        % Make sure that data values in key field are unique.
        keys = {S(:).(key)};
        if length(keys) ~= length(unique(keys))
            error('Structure key field contains non-unique values.');
        end
        
        % Generate dict by looping over struct array.
        D = dict;
        for i = 1:length(S)
            D(S(i).(key)) = S(i);
        end
end

