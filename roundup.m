%ROUNDUP  Rounds the value up to the specified value.
%  ROUNDVAL=ROUNDUP(VALUE,PLACE) rounds the given value to the specified
%  PLACE.  Magnitude of the number will alway increase, regardless of sign.
%  Limitations: If PLACE is not integer, then INV(PLACE) must be integer.
%
%  PLACE may be any value.  
%  Examples:
%  >> roundup(12.6,0.25) -> 12.75
%  >> roundup(0.33,1)    -> 1
%  >> roundup(0.33,5)    -> 5
%

function out = roundup(val,place)

% Error checks
error(nargchk(2,2,nargin));

% Place must be scalar
if ~isscalar(place)
    error('PLACE must be scalar.');
end

% Make sure place is a valid value.
if ~isint(place)
if ~isint(inv(place))
    error('Inverse of PLACE must be an integer value.');
end
end


% Save out sign.
sgn = sign(val);
val = abs(val);

% Do the rounding
out = place*ceil(val./place);
out = sgn.*out;