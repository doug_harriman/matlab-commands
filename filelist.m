%FILELIST  Returns a list of all files in a list of directories.
%   LIST=FILELIST(FILTER,DIRLIST) returns a LIST of all files in a list of
%   subdirectories DIRLIST matching the file filter specification FILTER.  
%
%   LIST=FILELIST(FILTER) returns a LIST of files in the curent directory
%   matching file filter specification FILTER.
%
%   LIST=FILELIST returns a LIST of all files in the current directory.
%
%   See also: DIR, SUBDIRS
%

% DLH (doug.harriman@comcast.net)
% 11-DEC-06 - Created

function [file_name_list] = filelist(filter,dirlist)

% Error check inputs
if nargin > 2
    error('Two inputs maximum.');
end
if ~ischar(filter)
    error('File filter specification must be a string.');
end
if ~(ischar(dirlist) || iscellstr(dirlist))
    error('Directory list must be string or cellstring.');
end

% Search the directories in the list for files and build a list of file
% names.
file_name_list   = {};
for i = 1:length(dirlist);
    dir_name = [dirlist{i} filesep];
    dir_data = dir([dir_name filter]);

    for j = 1:length(dir_data)
        file_name = [dir_name dir_data(j).name];
        file_name_list{length(file_name_list)+1} = file_name;
    end
    
end % file name for

file_name_list = file_name_list';
