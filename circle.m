%CIRCLE  Draw a circle of specified radius at specified center.
%

classdef circle < hgsetget
    properties
        % Name
        name
        
        % Center X coordinate.
        x
        
        % Center Y coordinate.
        y
        
        % Radius.
        r
        
        % Sector
        % Circle sector of interest in degrees.
        sector = [0 360];
        
        % Plot color
        color = 'None';
        
        % Plot face alpha.
        alpha = 0.5;
        
    end
    
    properties (SetAccess=private)
        % Handle of patch object.
        handle_patch
        
        % Handle of center point.
        handle_point
        
    end % properties - read only
    
    methods
        function obj = circle
            %CIRCLE  Circle object constructor.
            %
            
            % Default values
            obj.x = 0;
            obj.y = 0;
            obj.r = 1;
            
            obj.handle_patch = [];
            obj.handle_point = [];
            
        end % constructor
        
        function delete(obj)
            %DELETE  Destructor for circle object.
            %
            
            % Delete the graphics object.
            if ~isempty(obj.handle_patch) && ishandle(obj.handle_patch)
                delete(obj.handle_patch);
            end
            if ~isempty(obj.handle_point) && ishandle(obj.handle_point)
                delete(obj.handle_point);
            end
            
        end % destructor
        
        function plot(obj,varargin)
            %PLOT  Plot a circle object.
            %
            
            % Generate X,Y points about origin.
            [x,y] = obj.points;

            % Plot it
            if isempty(obj.handle_patch) || ~ishandle(obj.handle_patch)
                % Create patch
                hold('on');
                obj.handle_patch = patch(x,y,'b','FaceAlpha',obj.alpha);
                set(obj.handle_patch,'FaceColor',obj.color);
                obj.handle_point = plot(obj.x,obj.y,'k+');
                
                % Set callback
                set(obj.handle_patch,'ButtonDownFcn',@obj.OnClick);
                
            else
                set(obj.handle_patch,'XData',x,'YData',y);
                set(obj.handle_point,'XData',obj.x,'YData',obj.y);
            end

        end % plot
        
        function set.x(obj,val)
            %SET.X  Set circle center X coordinate.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            if ~isnumeric(val)
                error('Numeric value expected');
            end
            if ~isscalar(val)
                error('Scalar expected.');
            end
            
            % If the object was plotted, update it.
            if ishandle(obj.handle_patch)
                x = get(obj.handle_patch,'XData');
                x = x - obj.x;
                x = x + val;
                set(obj.handle_patch,'XData',x);
                set(obj.handle_point,'XData',val);
            end
            
            % Set the property
            obj.x = val;
            
        end % set.x
        
        function set.y(obj,val)
            %SET.Y  Set circle center Y coordinate.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            if ~isnumeric(val)
                error('Numeric value expected');
            end
            if ~isscalar(val)
                error('Scalar expected.');
            end
            
            % If the object was plotted, update it.
            if ishandle(obj.handle_patch) %#ok<*MCSUP>
                y = get(obj.handle_patch,'YData');
                y = y - obj.y;
                y = y + val;
                set(obj.handle_patch,'YData',y);
                set(obj.handle_point,'YData',val);
            end
            
            % Set the property
            obj.y = val;
            
        end % set.y

        function set.r(obj,val)
            %SET.R  Set circle radius.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            if ~isnumeric(val)
                error('Numeric value expected');
            end
            if ~isscalar(val)
                error('Scalar expected.');
            end
            
            % Set the property
            obj.r = val;
            
            % If the object was plotted, update it.
            if ishandle(obj.handle_patch)
                obj.plot;
            end
        end % set.y
        
        function set.color(obj,val)
            %SET.COLOR  Sets circle patch face color.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            
            set(obj.handle_patch,'FaceColor',val);
            
        end % set.color
        
        function set.alpha(obj,val)
            %SET.ALPHA  Sets circle patch face alpha.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            
            set(obj.handle_patch,'FaceAlpha',val);
            
        end % set.color

        function [x,y] = points(obj)
            %POINTS  Returns X & Y coordinates of circle points.
            %
            
            % Error checks
            error(nargchk(1,1,nargin));
            
            % Regenerate points.
            if obj.sector(1) < obj.sector(2)
                theta = [obj.sector(1):obj.sector(2)]*pi/180;
            else
                theta = [obj.sector(1):obj.sector(2)+360]*pi/180;
            end
            x     = obj.r*cos(theta); %#ok<*PROP>
            y     = obj.r*sin(theta);
            
            % Add lines into center.
            if mod(range(obj.sector),360) ~= 0
                % Need the points.
                x = [0 x 0];
                y = [0 y 0];
            end
            
            % Offset.
            x = x + obj.x;
            y = y + obj.y;
            
        end

        function OnClick(obj,varargin)
            %ONCLICK  Handles user clicking on circle.
            %
            
            % Set window button motion handler.
            set(gcf,'WindowButtonMotionFcn',@obj.OnDrag);
            
            % Clear handler on release.
            set(gcf,'WindowButtonUpFcn',@obj.OnRelease);
            
            % Show selection active.
            set(obj.handle_patch,'EdgeColor','r');
            
            % Set keypress fcn.
            set(gcf,'WindowKeyPressFcn',@obj.OnKeyPress);
            
        end % OnClick
        
        function OnDrag(obj,varargin)
            %ONDRAG  Handles user dragging circle.
            %
            
            % Get current point.
            p = get(gca,'CurrentPoint');
            
            % Set the circle center.
            obj.x = p(1,1);
            obj.y = p(1,2);
            
        end % OnDrag
        
        function OnRelease(obj,varargin)
            %ONRELEASE  Handles user releasing mouse button.
            %
            
            % Clear the Window handlers.
            set(gcf,'WindowButtonMotionFcn',[]);
            set(gcf,'WindowButtonUpFcn',[]);
            
        end % OnRelease
        
        function OnKeyPress(obj,~,event)
            
            % Postion change is a percentage of radius.
            delta = 0.10 * obj.r;
            
            % Handle key press
            switch(event.Key)
                case 'uparrow'
                    obj.y = obj.y + delta;
                
                case 'downarrow'
                    obj.y = obj.y - delta;

                case 'leftarrow'
                    obj.x = obj.x - delta;
                
                case 'rightarrow'
                    obj.x = obj.x + delta;
                
                case 'escape'
                    % Show selection active.
                    set(obj.handle_patch,'EdgeColor','k');
                    
                    % Set keypress fcn to disable.
                    set(gcf,'WindowKeyPressFcn',[]);

                case 'delete'
                    delete(obj);
                    
                case 'backspace'
                    delete(obj);
                    
                otherwise
                    disp(['Key: ' event.Key]);
            end
        end
        
    end
    
end % classdef