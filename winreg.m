%WINREG  Read/write string values to/from Windows registry.
%  S=WINREG(CMD,PARAM)
%
%  Read    - Reads all param/value pairs stored in the current keypath and
%            returns them as a structure S.
%  Write   - Writes the values of all the fields in the input structure PARAM
%            to the current keypath.
%  SetPath - Sets the current keypath.
%  GetPath - Returns the current keypath.
%  SetFile - Sets the temporary file used to read/write from the registry.
%  GetFile - Returns the temporary file used to read/write from the
%            registry.
%

% TODO - Add SetDefaultPath and SetDefaultFile using setpref to save preferences

%Modified from:
%SETTINGS: Windows registry access from MATLAB.
%(c) 2005 Mihai Moldovan (M.Moldovan@mfi.ku.dk)
%Only string (REG_SZ) values are implemented
%-maximum size of a key name is 255 characters
%-maximum size of a value name is 255 characters (Windows Me/98/95 compatible)
%-there is a theoretical 64K limit for the total size of all values of a key (exceptions are not handled)

function [varargout]=winreg(msg,varargin)

persistent keypath
persistent regfile

% Hard coded values for now
if isempty(keypath)
    keypath = 'HKEY_USERS\S-1-5-21-839522115-1383384898-515967899-309405\Software\VB and VBA Program Settings\FlextoolMatlabComm\CarriageServoProfile';
end
if isempty(regfile)
    regfile = 'c:\temp.reg';
end

% Default behavior is to read.
if nargin == 0
    msg = 'display';
end

switch lower(msg)
    case 'display'
        disp(['Key path: ' keypath]);
        disp(['Reg file: ' regfile]);
    
    case 'read'
        varargout{1}=loadsettings(keypath,regfile);

    case 'write'
        savesettings(keypath, regfile, varargin{1});

    case 'setpath'
        keypath = varargin{1};
        
    case 'getpath'
        varargout{1} = keypath;
        
    case 'setfile' 
        keypath = varargin{1};

    case 'getfile'
        varargout{1} = keypath;
        
    otherwise
        error(['Unknown message: ' msg]);
end

%----------------------------------------
% function e=deletesettings(keyname)
% 
% s0=loadsettings (keyname);
% if isempty(s0)
%     %no such key
%     return
% end
% 
% %create the command
% regfile=[tempdir 'temp.reg'];
% nl=char([13 10]); %CRLF
% regh=['REGEDIT4'  nl nl];
% regh=[regh '[-HKEY_LOCAL_MACHINE\\SOFTWARE\\' keyname ']'];
% 
% savestring (regfile, regh);
% 
% cmd=['REGEDIT /S ' regfile ];
% dos(cmd);

%----------------------------------------
function []=savesettings (keyname,regfile,s)

if ~ischar(keyname) || ~isstruct (s) || ~ischar(regfile)
    error ('Expecting keyname, filename, structure')
end

s0=loadsettings (keyname,regfile);
f0={};

if ~isempty(s0)
    f0=fieldnames(s0)';
end

f=fieldnames(s)';
nl=char([13 10]); %CRLF
regtext='';

for i=1:length(f)
    key = f{i};

    value=s.(key);
    if ~ischar(value)
        %make it a string here!
        value=num2str(value);
    end

    value=escape(value); %escape symbols

    if ismember (key, f0)
        if strcmp(value,s0.(key))==0
            %change only if new
            regtext=[regtext '"' key '"="' value '"'  nl];
        end
    else
        %it is a new one
        regtext=[regtext '"' key '"="' value '"'  nl];
    end


end

if isempty(regtext)
    return
end

%create the header
regh=['REGEDIT4'  nl nl];
regh=[regh '[' keyname ']' nl];
regh=[regh '@=""' nl]; %default mainkey value

regtext=[regh regtext];
savestring (regfile, regtext);

cmd=['REGEDIT /S ' regfile ];
dos(cmd);


%----------------------------------------
function s=loadsettings(keypath,regfile);

if nargin < 2
    error('Two inputs required');
end
if ~ischar(regfile)
    error('File name must be a string');
end

s=[];

if ~ischar(keypath)
    error ('settings(): Expecting a keyname')
end

cmd=['REGEDIT /E ' regfile ' "' keypath '"'];

dos(cmd);

t=loadlines (regfile);

%try
%clenup temp file
%delete (regfile);
%end

n=size(t,2);

if n < 2

    return
end

% truncate to "key"="value" pairs
% exception for non-string keys is not handled


for i=3:n
    line=t{i};
    if strcmp(line(1),'@')==0
        %skip default value
        k=find(line=='"');
        key=line(k(1)+1:k(2)-1);
        value=line(k(3)+1:k(end)-1);
        value=unescape(value);

        %make it a number if possible
        nvalue=str2num(value);
        if ~isempty(nvalue)
            s.(key)=nvalue;
        else
            s.(key)=value;
        end


    end
end


%----------------------------------------
function vout=escape(vin)

vout='';

vout=strrep(vin, '\', '\\\\');
vout=strrep(vout, '"','\"');


%----------------------------------------
function vout=unescape(vin)

vout='';

i=1;
while i <=length(vin)

    if vin(i)=='\'
        i=i+1;
    end

    vout=[vout vin(i)];

    i=i+1;
end

%----------------------------------------
function savestring(file, t)

try
    fid = fopen(file,'w');
    fprintf(fid,'%s',t);
end

%make sure the file is closed on exit
try
    fclose(fid);
end


%----------------------------------------
function t=loadlines (filename)

t={};

try
    fid = fopen(filename, 'r');
    i=1;
    while feof(fid) == 0

        line = fgetl(fid);
        %it may be a UNICODE text file (2 bytes)
        %expecting latin alphabet
        %ignore first line

        k=find(line>=' ' & line <='~');
        line=line(k);
        if ~isempty(line)
            t{i}=line;
            i=i+1;
        end
    end

end

%make sure the file is closed on exit
try
    fclose(fid);
end
