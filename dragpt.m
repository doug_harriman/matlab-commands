%DRAGPT  Draggable point.
%

classdef dragpt < hgsetget
    properties
        % Name
        name
        
        % Center X coordinate.
        x
        
        % Center Y coordinate.
        y

        % Marker shape
        marker = 'o';
        
        % Marker size
        size = 6;
        
        % Marker edge color
        edgecolor = 'r';
        
        % Marker face color
        facecolor = 'none';
        
    end
    
    properties (SetAccess=private)
        % Handle of center point.
        handle
        
    end % properties - read only
    
    events (NotifyAccess=protected)
        PositionUpdate  % Fires when a position is set programatically or a drag event ends.
        DragUpdate      % Fires at each drag event.
    end % events
    
    methods
        function obj = dragpt
            %DRAGPT  Draggable point object constructor.
            %
            
            % Default values
            obj.x = 0;
            obj.y = 0;
            
            obj.handle = [];
            
        end % constructor
        
        function delete(obj)
            %DELETE  Destructor for circle object.
            %
            
            % Delete the graphics object.
            if ~isempty(obj.handle) && ishandle(obj.handle)
                delete(obj.handle);
            end
            
        end % destructor
        
        function plot(obj,varargin)
            %PLOT  Plot a draggable point object.
            %
            
            % Plot it
            if ~isempty(obj.handle)
                if ishandle(obj.handle)
                    % Already plotted
                    return;
                end
            end
            
            % Initial plot
            hold('on');
            obj.handle = plot(obj.x,obj.y);
            obj.update;
                
            % Set callback
            set(obj.handle,'ButtonDownFcn',@obj.OnClick);
                
        end % plot
        
        function update(obj)
            %UPDATE  Updates plot to latest object data.
            %
            
            if isempty(obj.handle)  || ~ishandle(obj.handle)
                return;
            end
            
            % Update data
            set(obj.handle,...
                'XData',obj.x,...
                'YData',obj.y,...
                'Marker',obj.marker,...
                'MarkerSize',obj.size,...
                'MarkerFaceColor',obj.facecolor,...
                'MarkerEdgeColor',obj.edgecolor);
            
        end % update
        
        function set.x(obj,val)
            %SET.X  Set piont X coordinate.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            if ~isnumeric(val)
                error('Numeric value expected');
            end
            if ~isscalar(val)
                error('Scalar expected.');
            end
            
            % Set the property
            obj.x = val;
            obj.update;
            
            % Fire event
            notify(obj,'PositionUpdate');

        end % set.x
        
        function set.y(obj,val)
            %SET.Y  Set point Y coordinate.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            if ~isnumeric(val)
                error('Numeric value expected');
            end
            if ~isscalar(val)
                error('Scalar expected.');
            end
            
            % Set the property
            obj.y = val;
            obj.update;
            
            % Fire event
            notify(obj,'PositionUpdate');

        end % set.y

        function set.edgecolor(obj,val)
            %SET.EDGECOLOR  Sets point edge color.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            
            % Store & set
            obj.edgecolor = val;
            obj.update;
            
        end % set.edgecolor
        
        function set.facecolor(obj,val)
            %SET.FACECOLOR  Sets point face color.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            
            % Store it & set it
            obj.facecolor = val;
            obj.update;
            
        end % set.color

        function set.marker(obj,val)
            %SET.MARKER Sets point marker type.
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            
            if ~ischar(val)
                error('char value expected.');
            end
            
            % Valid marker types
            valid = {'+','o','*','.','x','square','s','diamond','d',...
                '^','v','>','<','pentagram','p','hexagram','h'};
            if ~ismember(val,valid)
                error('Invalid point marker type.');
            end
            
            % Set it
            obj.marker = val;
            obj.update;
            
        end % set.marker
        
        function set.size(obj,val)
            %SET.SIZE  Set point marker size
            %
            
            % Error checks
            error(nargchk(2,2,nargin));
            if ~isnumeric(val)
                error('Numeric value expected');
            end
            if ~isscalar(val)
                error('Scalar expected.');
            end
            
            % Set the property
            obj.size = val;
            obj.update;
            
        end % set.x
        
        function OnClick(obj,varargin)
            %ONCLICK  Handles user clicking on circle.
            %
            
            % Set window button motion handler.
            set(gcf,'WindowButtonMotionFcn',@obj.OnDrag);
            
            % Clear handler on release.
            set(gcf,'WindowButtonUpFcn',@obj.OnRelease);
            
            % Set keypress fcn.
            set(gcf,'WindowKeyPressFcn',@obj.OnKeyPress);
            
        end % OnClick
        
        function OnDrag(obj,varargin)
            %ONDRAG  Handles user dragging circle.
            %
            
            % Get current point.
            p = get(gca,'CurrentPoint');
            
            % Set the circle center.
            obj.x = p(1,1);
            obj.y = p(1,2);
            obj.update;
            
            % Fire event
            notify(obj,'DragUpdate');
            
        end % OnDrag
        
        function OnRelease(obj,varargin)
            %ONRELEASE  Handles user releasing mouse button.
            %
            
            % Clear the Window handlers.
            set(gcf,'WindowButtonMotionFcn',[]);
            set(gcf,'WindowButtonUpFcn',[]);
            
            % Fire event
            notify(obj,'PositionUpdate');
            
        end % OnRelease
        
        function OnKeyPress(obj,~,event)
            
            % Postion change is a percentage of radius.
            ax = get(obj.handle,'parent');
            xl = get(ax,'XLim');
            yl = get(ax,'YLim');

            delta_small = 0.005;
            delta_big   = 0.025;
            dx = diff(xl) * delta_small;
            dy = diff(yl) * delta_small;
            
            % Handle key press
            switch(event.Key)
                case 'uparrow'
                    obj.y = obj.y + dy;
                
                case 'downarrow'
                    obj.y = obj.y - dy;

                case 'leftarrow'
                    obj.x = obj.x - dx;
                
                case 'rightarrow'
                    obj.x = obj.x + dx;
                
                case 'escape'
                    % Set keypress fcn to disable.
                    set(gcf,'WindowKeyPressFcn',[]);

                case 'delete'
                    delete(obj);
                    
                case 'backspace'
                    delete(obj);
                    
                otherwise
                    disp(['Key: ' event.Key]);
            end
        end
        
    end
    
end % classdef