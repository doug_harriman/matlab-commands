%ASSUMPTIONS  Document M-file model assumptions.
%
%   ASSUMPTIONS(ASSUMPTION_TEXT) stores message.
%   ASSUMPTIONS('Clear')  clears all stored assumptions.
%   ASSUMPTIONS('Report') displays list of recorded assumptions.
%

% DLH
% 10/03/05 - Created

function [varargout] = assumptions(msg)

persistent list
persistent window_handle

% JPRINTF window handle.
window_fid = -1;

if nargin == 0,
    msg = 'report';
end

if nargin > 1,  error('Single input expected'); end
if ~isstr(msg), error('String input expected'); end

if isempty(list), list = struct(); end

switch(lower(msg)),
    case 'clear',
        list = struct();
        set(window_handle,'Text','');
%         set(window_handle,'Name','Model Assumptions');
        window_handle = jprintf(window_fid,'Model Assumtions\n');
    case 'report',
        if nargout == 0,
            disp(list);
        else,
            varargout{1} = list;
        end
        
    otherwise,
        % Get caller info
        [st,i] = dbstack;
        file = st(i+1).file;
        fcn  = st(i+1).name;
        line = st(i+1).line;

        % Build unique field name based on calling file and function.
        file = file(1:end-2);
        if strcmp(file,fcn),
            name = [fcn '_' num2str(line)];
        else,
            name = [file '_' fcn '_' num2str(line)];
        end

        % Don't overwrite message if no change.
        if ~isfield(list,name),
            list = setfield(list,name,msg);
            
            msg = strrep(msg,'%','%%');
            jprintf(window_fid,[msg '\n']);
        end
end



