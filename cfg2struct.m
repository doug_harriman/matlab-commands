%CFG2STRUCT  Reads a Unix standard configuration file into a structure.
%  S=CFG2STRUCT(FILENAME) read the configuration file FILENAME into a
%  structure S.  
%
%  See also: cfg2dict, dict2struct.
%

% Doug Harriman (doug.harriman@gmail.com)

function S = cfg2struct(filename)

% Error checks
error(nargchk(1,1,nargin));

if ~exist(filename,'file')
    error(['Unable to find file: ' filename]);
end

% Data conversion
D = cfg2dict(filename);
S = dict2struct(D);
