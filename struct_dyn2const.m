%STRUCT_DYN2CONST  Converts constant valued dynamic field names to constant field names for structures.
%  STRUCT_DYN2CONST(FILENAME) converts instances where dynamic field name
%  syntax is used with a constant string value to constant field name
%  syntax.  The constant field name syntax operates approximately 1000x
%  faster than the dynamic case.
%
%  Dynamic field name syntax with constant value example:
%  mystruct.('constant') = 12;
%
%  Constant field name example:
%  mystruct.constant = 12;
%

function struct_dyn2const(filename)

% Load the string
str = file2str(filename);

% Swap syntax
exp = '\.\(''(\w+)''\)';
str = regexprep(str,exp,'.$1');

% Re-write the file.
str2file(str,filename);