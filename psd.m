%PSD Power Spectral Density for an arbitrary data set.
% 
%>>[mag,w] = psd(time,data)
%            w in Hz
%

function [p,x] = psd(t,data)

if nargin == 1,
  data = t ;  % Parse input
  y = fft(data) ;  % do FFT
  ind = round(length(y)/2) ;  % First half of the data set
  p = abs(y(1:ind)) ; % 2 Norm of coeffs
  x=[0:1:length(p)-1] ;
  
else
   
   % Make sure time starts at zero
   if t(1) ~= 0,
      t = t - t(1);
   end
   
  y = fft(data) ;  % do FFT
  x = [0:length(t)-1]/max(t);  % Frequencies
  ind = round(length(x)/2) ;  % First half of the data set
  x = x(1:ind) ;  % First half of frequencies
  p = abs(y(1:ind)) ; % 2 Norm of coeffs
  
end

if nargout == 0,
  % Plot
  plot(x,p) % plot it
  ylabel('PSD') ;
  xlabel('Frequency (Hz)') ;
end

