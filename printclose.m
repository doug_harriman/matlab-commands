%PRINTCLOSE Prints the given list of figures, then closes them.
%           If no figure handle(s) provided, will print and close 
%           the current figure.
%

% DLH

function [] = printclose(figs)

% Error check
if nargin == 0,
   figs = gcf ;
elseif strcmp(figs,'all')
   figs = get(0,'children') ;
end

% Cycle through
for i = 1:length(figs),
   figure(figs(i)) ;
   pp
   %print -dwinc
   close
end
