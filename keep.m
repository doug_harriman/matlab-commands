function keep(varargin)
%KEEP  Quicker to type than 'clearvars -except <var>'
%

% Build string of vars to keep.
str = '';
for i = 1:nargin,
   str = [str ' ' varargin{i}]; %#ok<AGROW>
end

% Build cmd
str = ['clearvars -except ' str];
evalin('base',str);