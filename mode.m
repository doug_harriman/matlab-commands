%MODE  Statistical mode of a population.
%      This returns the most common element of a 
%      vector or matrix, and the number of times
%      that element occurs.
%

% Douglas Harriman
% 08/29/01 - Created

function [element, count] = mode(data)

% Convert matrix to vector
if ~isvector(data)
    data = reshape(data,size(data,1)*size(data,2),1);
end

members = unique(data);
numMembers = length(members);

count = 0;
element = NaN;
for i = 1:numMembers,
    thisElement = members(i);
    thisCount = length(find(thisElement == data));
    if thisCount > count,
        count = thisCount;
        element = thisElement;
    end
end
