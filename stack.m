%STACK  Stack container object.
%  Stack container implementation.  Supports typical stack operations.
%
%  Supported methods:
%    push    - Pushes arguments onto stack.
%    pop     - Pops value off of first level of stack.
%    peek    - Returns value on first level of stack, but leaves value on stack.
%    isempty - Returns true if stack is empty.
%    length  - Returns number of elements on stack.
%    flipud  - Flips stack top to bottom.
%    cell    - Returns contents of stack in a cell array.
%

% Doug Harriman (doug.harriman@gmail.com)

classdef stack < handle
    properties
        % User specified data.
        userdata;
        
    end % properties - public
    
    properties (Access=private)
        % Data storage cell.  Stack grows down.
        storage = {};  
        
    end % properties - private
    
    methods
        function obj = push(obj,varargin)
            %PUSH  Push a new value onto the stack.
            %
            
            % Push values onto stack.  Stack grows down.
            for i = 1:length(varargin)
                obj.storage{end+1,1} = varargin{i};
            end
            
        end % push
        
        function val = pop(obj)
            %POP  Pop a value off of the stack.
            %

            % Return last value
            val = obj.peek;
            
            % Remove last value.
            obj.storage = obj.storage(1:end-1);
            
        end % pop
        
        function val = peek(obj,idx)
            %PEEK  Peek at any value in the stack without removing.  
            %  PEEK(LEVEL) returns the value at the requested stack level.
            %  Default LEVEL is 1.
            %
            
            % Check for empty stack.
            if isempty(obj)
                val = [];
                return;
            end
            
            % Check for level request.
            if(nargin < 2)
                idx = 1;
            end
            
            % Error check
            % Scalar
            if length(idx) > 1
                error('Index must be scalar.');
            end
            
            % Integer
            if ~isint(idx)
                error('Stack level must be specified with an integer');
            end
            
            % In bounds
            if idx > length(obj)
                error('Stack index exceeds stack height.');
            end
            
            % Stack stored in opposite order, so calculate acutal index.
            idx = [1:length(obj)] == idx;
            idx = fliplr(idx);
            
            % Return value.
            val = obj.storage{idx};
            
        end % peek
        
        function val = isempty(obj)
            %ISEMPTY  Returns true if stack is empty.
            %
            
            % Just map to cell isempty
            val = isempty(obj.storage);
            
        end % isempty
        
        function val = length(obj)
            %LENGTH  Returns number of objects on the stack.
            %
            
            % Just map to cell length.
            val = length(obj.storage);
            
        end % length
        
        function flipud(obj)
            %FLIPUD  Flips the stack.
            %
            
            % Handle empty case
            if isempty(obj)
                return;
            end
            
            % Create reverse indecies.
            idx = 1:length(obj);
            idx = fliplr(idx);
            obj.storage = obj.storage(idx);
            
        end % flipud
        
        function disp(obj)
            %DISP  Display stack in Matlab syntax.
            %
            
            % Handle empty case.
            if isempty(obj)
                disp('Empty stack.');
                return;
            end
            
            % Header
            disp('Stack');
            
            % Start from top of stack.
            len = obj.length;
            for i = 1:len
               % Prefix
               disp([int2str(len-i+1) ': ']);
               disp(obj.storage(i));
            end
            
        end % disp
        
        function display(obj)
            %DISPLAY  Full display control.
            %
            
            disp(obj);
            
        end % display
        
        function val = cell(obj)
            %CELL  Converts stack to cell array.
            %
            
            % Just return the internal cell array.
            val = obj.storage;
            
        end % cell
        
    end % methods - public
    
end % classdef