%PARALLEL_R  Calculates the equivalent resistance of N parallel resistors.
%  R_EQ=PARALLEL_R(R1,R2,...,Rn)
%

% Doug Harriman (doug.harriman@gmail.com)
% 01-Dec-2011 - Created.

function r_eq = parallel_r(varargin)

% Input error check loop
for i = 1:nargin
    % Make sure we have a unit object
    if ~isa(varargin{i},'unit')
        varargin{i} = unit(varargin{i},'ohm');
    end
    
    % Check unit consistency
    if ~isconsistent(varargin{i},'ohm')
        error(['input ' int2str(i) ' should have units of ohms.']);
    end
    
    % Scalars only.
    if ~isscalar(varargin{i})
        error('Inputs must be scalar.');
    end
end

% Calculation loop
S = 1/varargin{1};
for i = 2:nargin
    S = S + 1/varargin{i};
end

% Calc equivalent
r_eq = 1/S;