%HLINE  Draw horizontal line at given level.
%

function [han] = hline(level,varargin)

xlim = get(gca,'xlim');
hold('on');
for i = 1:numel(level)
    han(i)=plot(xlim,[1 1]*level(i),varargin{:});
end
