%MOVEAVE Moving average of a regularly sampled signal.
% syntax: out = moveave(channel, points, type)
%	out     = moving average signal.
%	channel = input signal.
%	points  = number of points for average.
%   avetype = 'center' or 'trailing'.  
%             Default is trailing. Trailing is ~100x faster.
%

% DLH

% Ported from Channel specific version.

function avg = moveave(in,pts,avetype)

% Error checks
narginchk(2,3);

% Get data
data    = in;
data    = reshape(data,1,length(data));

avg = ones(size(data))*NaN;

if nargin < 3
    avetype = 'trailing'; 
end

switch avetype
    case 'center'
        totpts  = length(data);
        halfPts = round((pts-1)/2);
        % Loop through all data points
        for i = halfPts+1:totpts-halfPts-1,
            % Calculate the ave
            avg(i)   = mean(data(i-halfPts:i+halfPts));
        end   
        
    case 'trailing'
        % Fully vectorized and accelerated.
        avg = filter(ones(1,pts)/pts,1,data);
        
    otherwise
        error(['Unsupported averaging type: ' upper(avetype)]);
end

% Make sure output is same shape as input.
avg = reshape(avg,size(in));

