%OPTIONS  Handles setting default and user specified options.
%  OPT_STRUCT = OPTIONS(DEFAULT_STRUCT,PARAM_1,VALUE_1,...,PARAM_N,VALUE_N)
%  Returns the structure OPT_STRUCT with a field for each parameter, and
%  each field value set to value.  DEFAULT_STRUCT defines which fields are
%  allowed, and default vaules for each field.  Default values are
%  overridden by the passed in PARAM/VALUE pairs.
%
%  The returned options structure will have field names in all lower
%  case letters.
%
%  [OPT_STRUCT,CHANGE_LIST] = OPTIONS(...) returns a string list
%  CHANGE_LIST containing the names of the properties that were modified.
%
%  [OPT_STRUCT,CHANGE_LIST,UNHANDLED] = OPTIONS(...) returns a cell array
%  UNHANDLED of options that were not present in the DEFAULT_STRUCT and
%  therefore were not copied into it.
%  
%  See also: struct, commonfields, copyfields.

% TODO - Convert to using COMMONFIELDS.M & COPYFIELDS.M where appropriate.

% Possible extensions:
% - Basic error checking based on simple input types.
% - Automatically handle user set default values via non-volatile storage.

% <USER_ID>
% </USER_ID>

function [opt_struct,change_list,unhandled] = options(opt_struct,varargin)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'CodeTools';
TOOLBOX_MODULE  = '';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Basic error checks
if ~isstruct(opt_struct)
    error('Default values must be in structure.'); 
end
if isodd(length(varargin))
    error('PARAM/VALUE list length must be even.'); 
end

% Force options parameter strings to lower case.
idx = [1:2:length(varargin)];
varargin(idx) = lower(varargin(idx));

% Convert passed in to the requested change structure
idx = cellfun('isclass',varargin,'cell');
varargin(idx) = {varargin(idx)};
req_struct = struct(varargin{:});

% Find common fields to pass.
[change_list,req_only_fields] = commonfields(req_struct,opt_struct);

% Copy the valid requested changes to the options
opt_struct = copyfields(req_struct,opt_struct,change_list);

% Early exit
if nargout < 3
    return;
end

% Generate a new cell of unhandled options
tf  = ismember(varargin(1:2:end),req_only_fields);
idx = find(tf);
idx = (idx-1)*2 + 1;     % Convert to indecies into varargin
idx = sort([idx idx+1]); % varargin elements to extract.
unhandled = varargin(idx);