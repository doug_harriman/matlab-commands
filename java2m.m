%JAVA2M  Converts a subset of Java objects to native Matlab objects.
%  MATLABDATA=JAVA2M(JAVADATA) converts the simple JAVADATA Java object to 
%  native Matlab objects.
%
%  NOTE: This function is intended to support a few Java object types that
%  are emmitted by the SnakeYaml Java library.  This function should be
%  extended as needed to support other Java objecd types.
%

% Doug Harriman (doug.harriman@gmail.com)
% Created: 23-Aug-2010

function data = java2m(data)

switch(class(data))
    case 'java.util.LinkedHashMap'
        data = LinkedHashMap2struct(data);
        
        % Call recursively on each structure item.
        fields = fieldnames(data);
        n = numel(fields);
        for i = 1:n
            field = fields{i};
            data.(field) = java2m(data.(field));
        end
        
    case 'java.util.ArrayList'
        data = ArrayList2cell(data);
        
        % Call recursively on each cell item.
        n = numel(data);
        for i = 1:n
            data{i} = java2m(data{i});
        end
        
    case 'java.util.Date'
        data = char(data.toString);
end


% Convert a Java LinkedHashMap to a Matlab structure.
function s = LinkedHashMap2struct(lhm)

% Type check
assert(isa(lhm,'java.util.LinkedHashMap'));

% Get keys & values
keys = cell(lhm.keySet.toArray);
vals = cell(lhm.values.toArray);

% Make sure key names are valid matlab values.
keys = genvarname(keys);

% Create a struct
s = cell2struct(vals,keys,1);

% Convert a Java ArrayList to a Matlab cell array.
function c = ArrayList2cell(al)

c = cell(size(al),1);

for i = 1:size(al)
    c{i} = al.get(i-1);
end