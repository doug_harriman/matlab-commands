%SLEGEND  Legend capturing line display names.
%   H=CLEGEND(LOCATION) creates a legend for 
%   lines plotted in the current axes.  H is
%   the legend handle, and LOCATION sets the 
%   placement location of the axes. See 
%   HELP LEGEND for more information.
%

% DLH
% 06-Jan-2012 - Created

function [han] = slegend(loc)

% Error check
if nargin > 1
    error('One input expected.'); 
end

% Get display name strings from plot
lineHan = get(gca,'Children');
lineHan = findobj(lineHan,'Type','line');
lineHan = flipud(lineHan);
nLines = length(lineHan);

for i = 1:nLines
    str{i} = get(lineHan(i),'DisplayName');
end

if nargin == 1
    clicklegend(str,loc);
else
    clicklegend(str);
end