%DICT  Dictonary data storage.  Stores data by key/value pairs.
% Creates a key/value dictionary similar to the DICT object in Python.
% Keys and values may be any valid Matlab data type.
%
% Examples:
%   x=dict;          % Create dictionary object
%   x(12)='hello';   % Store value.
%   x('hi')=[1 2 3]; % Store value.
%
%   x('hi')          % Lookup value
%     ans =
%        1 2 3
%
%   x(12)            % Lookup value
%     ans =
%   hello
%
%   lookup(x,12)     % Lookup value
%     ans =
%   hello
%
%  Other methods:
%    KEYS    - Returns cell array of all keys.
%    VALUES  - Returns cell array of all values.
%
%    ISEMPTY - Returns true if dictionary is empty.
%    LENGTH  - Returns number of elements stored in dictionary.
%    ISKEY   - Determines if a key exists in dictionary.
%    INDEX   - Returns index of key in key array.
%
%    PLUS    - Combines two dictionaries.
%    MINUS   - Creates dictionary of unique keys.
%    RMKEY   - Removes key/value pair from dictionary.
%
%    RENAME  - Renames a key.
%
%    CFG     - Generate a Unix like configuration string from key value
%              pairs.  Returns a string.
%    REPORT  - Generate a Unix like configuration file from key value
%              pairs.  If no filename provided, opens temporary file in
%              editor.
%    STRUCT  - Returns a structure with the dictionary keys as fields.
%              All keys must be valid Matlab variable names.
%
% Credits: Inspired by and based on LOOKUPTABLE by Dan Cohn, available on
%          Matlab Central:
%          http://www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=19381&objectType=file
%

% Written by: Doug Harriman (doug <dot> harriman <at> gmail <dot> com)
% 25-Apr-08 - Added RENAME.
% 20-Apr-08 - '.' access to methods working.  Allethods have help text.
%             Added '+' and '-' methods to composite DICT's.
% 03-Apr-08 - More methods to make more similar to Python dict object.
% 02-Apr-08 - Initial version.

%TODO - Add feature: error on read failure.  Generate an error if a read is
%       attempted on a non-existant key.  Helps to debug applications using
%       dict.
%TODO - Add feature: read only.  Can fill a dict then set to R/O mode.
%       Keeps important data from getting overwritten.

classdef dict < handle

    properties (SetAccess=protected)

        % Cell arrays to hold keys and values of any Matlab data type
        key   = {};
        value = {};

        % Log file name.  Empty if no logging disabled.
        logfile
        
    end % properties - protected

    properties (Access=private)
        % Log file identifier.
        logfid
        
    end % properties - private
    
    methods
        function obj = dict
            %DICT  Dictionary constructor.
            %
            
            % Logging defaults.
            obj.logfile = 'dictlog.txt';
            obj.logfid  = [];
            
        end % dict
        
        function delete(obj)
            %DELETE  Dictionary object destructor.
            %
            
            % Try to close the log file.
            if ~isempty(obj.logfid)
                try %#ok<TRYNC>
                    fclose(obj.logfid);
                end
            end
            
        end % delete - object destructor.
        
        function val = subsref(obj,ref)
            %DICT/SUBSREF Subscript referencing for DICT objects.
            %
            %  See also: DICT
            %

            % Re-implement dot referencing.
            if strcmp(ref(1).type,'.')
                % User trying to access a property or method.

                % Property access
                props = properties(obj);
                if ismember(ref(1).subs,props)
                    val = obj.(ref.subs);
                    return;
                end

                % Methods access
                if ismember(ref(1).subs,methods(obj))
                    if length(ref) > 1
                        % Call with args
                        val = obj.(ref(1).subs)(ref(2).subs{:});
                    else
                        % No args
                        val = obj.(ref.subs);
                    end
                    return;
                end

                % User trying to access something else.
                error(['Reference to non-existant property or method ''' ref.subs '''']);
            end

            if ~strcmp(ref.type,'()')
                error('Cell indexing not supported.');
            end

            % Vectorized calls not supported
            if length(ref.subs) > 1
                error('DICT only supports storing key/value pairs one at a time.');
            end
            key = ref.subs{1};

            if false
                if ~iskey(obj,key)
                    error(['Invalid key: ' key]);
                end
            end
            val = lookup(obj,key);

        end % subsref

        function obj = subsasgn(obj,ref,value)
            %DICT/SUBSASGN  Subscript assignment for DICT objects.
            %
            %  See also: DICT
            %

            if ~strcmp(ref.type,'()')
                error('Cell and dot indexing for assignment not supported.');
            end

            % Vectorized calls not supported
            if length(ref.subs) > 1
                error('DICT only supports storing key/value pairs one at a time.');
            end
            key = ref.subs{1};

            % Existing or new key?
            idx = index(obj,key);
            if isempty(idx)
                % New
                idx = length(obj.key) + 1;
            end

            obj.key{idx}   = key;
            obj.value{idx} = value;

        end % subsasgn

        function val = length(obj)
            %DICT/LENGTH  Number of key/value pairs in dictionary.
            %
            %  See also: DICT
            %

            val = length(obj.key);
        end

        function idx = index(obj,key)
            %DICT/INDEX  Find index of a key in the dictionary key array.
            %
            %  See also: DICT
            %

            idx = cellfun(@(x)isequal(x,key),obj.key);
            idx = find(idx);
        end % index

        function [] = disp(obj)
            %DICT/DISP  Display a DICT object.
            %
            %  See also: DICT
            %

            disp('Dictionary:');
            disp('  keys: ');
            disp(obj.key');
%             disp('  values: ');
%             disp(obj.values);
        end % disp

        function [] = display(obj)
            %DICT/DISPLAY  Display a DICT object.
            %
            %  See also: DICT
            %
            disp(obj);
        end

        function val = lookup(obj,key)
            %DICT/LOOKUP  Return a value given a key.
            %
            %  See also: DICT, DICT/INDEX
            %

            % Make sure key exists
            idx = index(obj,key);
            if isempty(idx)
                val = [];
                return;
            end

            % Extract value
            val = obj.value(idx);
            val = val{1};
            
            % Logging.
            if isempty(obj.logfid)
                return;
            end
               
            % Figure out who called us.
            s = dbstack;
            caller = '';
            for i = length(s):-1:1
                % Looking for first non-dict function.
                if ~strncmp(s(i).name,'dict.',5)
                    % Found it.
                    caller = s(i).name;
                    break;
                end
            end
            
            % If no caller identified, must have been base workspace.
            if isempty(caller)
                caller = 'Matlab-Workspace';
            end
            
            % Write the query to the log.
            t = now;
            fprintf(obj.logfid,'%s,%s,%s,%s\n',...
                datestr(t,1),...
                datestr(t,13),...
                key,...
                caller);
            
        end

        function data = keys(obj)
            %DICT/KEYS  Returns cell array of all keys in a DICT object.
            %
            %  See also: DICT, DICT/VALUES
            %

            data = obj.key';
        end % keys

        function data = values(obj)
            %DICT/VALUES  Returns cell array of all values in a DICT object.
            %
            %  See also: DICT, DICT/KEYS
            %

            data = obj.value';
        end % values

        function tf = iskey(obj,keys)
            %DICT/ISKEY  Returns bool array denoting if key(s) exist in dictionary.
            %
            %  See also: DICT, DICT/KEYS
            %

            if ~iscell(keys)
                keys = {keys};
            end
            nkeys = length(keys);
            tf    = false(nkeys,1);
            for i = 1:nkeys
                key = keys{i};
                tf(i) = any(cellfun(@(x)isequal(x,key),obj.key));
            end
        end % iskey

        function obj = rmkey(obj,key)
            %DICT/RMKEY  Removes a key/value pair from a dictionary by key.
            %
            %  See also: DICT, DICT/ISKEY
            %

            idx = index(obj,key);
            obj.key(idx)   = [];
            obj.value(idx) = [];

            % Make the display look nice
            if isempty(obj.key)
                obj.key   = {};
                obj.value = {};
            end
        end % rmkey

        function tf = isempty(obj)
            %DICT/ISEMPTY  Returns true if dictionary isempty.
            %
            %  See also: DICT, DICT/KEYS
            %

            tf = isempty(obj.key);
        end % isempty

        function obj1 = plus(obj1,obj2)
            %DICT/PLUS  Creates composite dictionary.
            %  DICT1+DICT2 returns a dictionary composed of key value pairs
            %  from both DICT1 and DICT2.  If both dictionaries contain the
            %  same key, the value stored in DIC1 will be used.
            %
            %  See also: DICT, DICT/MINUS, DICT/KEYS, DICT/ISKEY
            %

            % Error check inputs
            error(nargchk(2,2,nargin));
            if ~isa(obj2,'dict')
                error('Both inputs must be DICT objects.');
            end

            % Look for key collisions
            idx = obj1.iskey(obj2.keys);

            % Tack on unique keys and values to x.
            obj1.key   = {obj1.key{:} obj2.key{~idx}};
            obj1.value = {obj1.value{:} obj2.value{~idx}};

        end % plus

        function obj1 = minus(obj1,obj2)
            %DICT/MINUS  Creates dictionary of unique keys.
            %  DICT1-DICT2 returns a dictionary composed of key value pairs
            %  which have keys that only exist in DICT1.
            %
            %  See also: DICT, DICT/PLUS, DICT/KEYS, DICT/ISKEY
            %

            % Error check inputs
            error(nargchk(2,2,nargin));
            if ~isa(obj2,'dict')
                error('Both inputs must be DICT objects.');
            end

            % Look for key collisions
            idx = ~obj2.iskey(obj1.keys);

            % Tack on unique keys and values to x.
            obj1.key   = obj1.key(idx);
            obj1.value = obj1.value(idx);

        end % minus

        function obj = rename(obj,key_old,key_new)
            %DICT/RENAME  Renames a key.
            %  RENAME(DICT,KEY_OLD,KEY_NEW)
            %
            %  See also: DICT, DICT/ISKEY
            %

            idx = index(obj,key_old);

            if isempty(idx)
                return;
            end

            obj.key{idx} = key_new;

        end % rename

        function str = cfg(D)
            %CFG  Generates Unix like configuration strings.
            %
            %  See also: dict/report.
            %

            % Get keys in sorted order, store order for later.
            [keys,idx] = sort(D.keys);

            % Get the data in sorted order.
            values = D.value(idx)';

            % Convert values to strings.
            values = cellfun(@(x)Val2String(x),values,...
                'UniformOutput',false);

            % Create justified string arrays.
            keys   = strjust(char(keys),'left');
            values = strjust(char(values),'left');

            % Create the final string
            sz  = ones(size(keys,1),1);
            eq  = char(sz*' = ');
            cr  = char(sz*char(10));
            str = [keys eq values cr];

            % Cleanup of linefeeds
            str = reshape(str',1,numel(str));

            % Helper function
            function [val] = Val2String(val)
                % Early exit for strings
                if ischar(val)
                    return;
                end

                % Char conversion.
                try
                    % Numeric values
                    if isnumeric(val)
                        val = num2str(val);
                        return;
                    end

                    % Units
                    if isa(val,'unit')
                        val = num2str(val);
                        return;
                    end

                    % Cell string arrays.
                    if iscellstr(val)
                        % Handle empty cells
                        if isempty(val)
                            val = '';
                            return;
                        end

                        % Make sure row vector.
                        val = reshape(val,1,numel(val));

                        % Add in separators
                        val2 = cellfun(@(x)[x ', '],val(1:end-1),...
                            'UniformOutput',false);
                        val2(end+1) = val(end);
                        val = cell2mat(val2);
                        return;
                    end

                    % Boolean
                    if isbool(val)
                        if val
                            val = 'True';
                        else
                            val = 'False';
                        end
                        return;
                    end
                    

                    % Unable to handle it.
                    val = '#UNABLE TO CONVERT#';

                catch %#ok<CTCH>
                    val = '#UNABLE TO CONVERT#';
                end
            end % Val2String
        end % cfg

        function obj = report(obj, filename)
            %REPORT  Genrate a Unix like configuration file from key value pairs.
            %  REPORT(DICT,FILENAME) writes the configuration file to the
            %  given file name.
            %  REPORT(DICT) writes the configuration file to a temporary
            %  file, opens that file in the editor, then deletes the file
            %  from disk.
            %
            %  See also: dict/cfg.
            %

            % Temp file or not?
            tempfile = false;
            if (nargin < 2) || isempty(filename)
                tempfile = true;
                filename = tempname;
            end

            str = obj.cfg;

            % Try to open the file.
            fid = fopen(filename,'w');
            if fid < 0
                if tempfile
                    error('Unable to open temporary file for writing.');
                else
                    error(['Unable to open file for writing: ' filename]);
                end
            end
            fprintf(fid,'%s\n',str);
            fclose(fid);

            % Delete tempfiles
            if tempfile
                edit(filename);
                delete(filename);
            end

        end % report
        
        function S = struct(D)
            %STRUCT  Convert dictionary to structure.
            %  S=STRUCT(D) converts the keys of D to the fieldnames of S if
            %  the keys are convertable to valid Matlab variable names.
            %
            
            % Error checks
            error(nargchk(1,1,nargin));
            if ~isa(D,'dict')
                error('Input must me a DICT object.');
            end
            
            % Extract data
            keys = D.keys;
            vals = D.value;
            
            % Drop keys that we can't convert.
            tf = cellfun(@(x)ischar(x),keys);
            keys(~tf) = [];
            vals(~tf) = [];
            
            % Make sure keys are valid variable names.
            keys = genvarname(keys);
            
            % Create the structure
            S = cell2struct(vals,keys,2);
            
        end % struct
            
        function obj = logging(obj,status,filename)
            %LOGGING  Log dictionary reads and writes.
            %  LOGGING('on',FILENAME) turns logging on.  FILENAME is
            %  optional.  If not specified a file name "dictlog.txt" will
            %  be created in the current directory.  Logging will continue
            %  until the dictionary object is destroyed, or logging is
            %  explicitly disabled.
            %
            %  LOGGING('off') will disable file logging.
            %
            
            % Error checks
            error(nargchk(2,3,nargin));
            if ~ischar(status)
                error('status must be a character array.');
            end
            status = lower(status);
            if ~ismember(status,{'on','off'})
                error('status value must be either ''on'' or ''off''.');
            end
            
            
            % Simple case, turning off logging.
            if strcmp(status,'off')

                % Warn that file name is not used.
                if nargin > 2
                    warning('filename is only specified if enabling logging.'); %#ok<WNTAG>
                end
                
                % Try to close the file.
                try  %#ok<TRYNC>
                    fclose(obj.logfid);
                end
                
                % Mark file as closed.
                obj.logfid = [];
                return;
            end
                
            %% Start logging.
            % Default file name?
            if nargin < 3
                filename = obj.logfile;
            end
            
            % Validate file name.
            if ~ischar(filename)
                error('Log file name must be a string.');
            end
            
            % Try to open the file.
            [fid,msg] = fopen(filename,'a');
            if fid < 0
                error(msg);
            end
            
            % Store the file info.
            obj.logfile = filename;
            obj.logfid  = fid;
            
        end % logging
        
        end % methods

end % classdef

