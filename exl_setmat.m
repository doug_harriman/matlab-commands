%EXL_SETMAT  Writes a matrix to an Excel file.
% conf=exl_setmat(a,target,region,host)
% - The 'a' is a numeric input Matrix or cell array.
%   allows the user to insert the data.
%    Version 5.3 allows also strings in the cell array
% - The target is the filespec of the file to read from. default directory = pwd
%                                                        default extension = 'xls'
%        if the file not exist it will be created.
%        if there is no target file a default [pwd 'mat_get_set.xls'] will be used.
% - Region should be of the form:'Sheet2!R2C4:R3C6'
%    For the first worksheet of the workbook, you can omit
%    the worksheet address.
%    the size defined by the region should fit the size of A or
%    contains only the first cell, e,g,. 'Sheet2!R2C4'
%    If you specify too big region Excel will pad it with #N/A
%    If you specify too small region only part of the information will be transferred.
% - host is needed only for network.
% The output is [ O.K | Brara ] for successful or failure operation, respectivelly.
% This program was tested under versions 5.2 and 5.3 on PC with WIN95 and OFFICE97.
% examples:
%------------
% 1)using nodefaults
%� matri=magic(4);
%� g=exl_setmat(matri,'c:\mydocu~1\tryset.xls','R3C5:R6C8')
%g =
%O.K
% 2)Using the default file and the internal cell array size calculation:
%   � g=exl_setmat(matri,[],'R3C5')
%   g =
%   O.K
% In this example the default file was created (if not exist before) in the
% present working directory and the input transfered into a cells region started
% at R3C5 (third row fifth column) in the first worksheet.

% This program was tested under versions 5.2 and 5.3 on PC with WIN95 and OFFICE97.
%

% Created at: 20-10-99  by
%                        |
%                        V
%          e-mails: jonathan@ndc.soreq.gov.il
%                   bar-sagi@actcom.co.il

% Modifications
% Sep-10-2002 - Doug Harriman (doug_harriman@hp.com)
%       - Changed version checking to numerical only (was character based).
%       - Modified region specification to allow cell addresses with more than 1 digit.
%       - Changed return value to 0 or 1.

function conf=exl_setmat(a,target,region,host)

v=version;
v1=str2num(v(1));
if v1<5,
    disp(' Incorrect Matlab version')
    return
end
v2=str2num(v(3));
if v1==5, v2 < 2,
    disp('Warning! Incorrect Matlab version')
    dang = input('Do you want to continue anyway [Y,N] ? ')
    if dang(1) == 'Y' | dang(1) == 'y'
    else
        return
    end
end

if isempty(a)
    bull=0;
elseif iscell(a),
    A=a;
    bull = 1;
else
    A=num2cell(a);
    bull = 2;
end

if isempty(region) | bull==0,
    disp('Not enough information');
    return
end
% Initiating link to Excel 
if nargin==4
    s=actxserver('Excel.Application',host);
else
    s=actxserver('Excel.Application');
end
if nargin>=2 & ~isempty(target),
    [D,fil,ext]=fileparts(target);
    if isempty(D)
        %set default directory
        D=pwd;
    end
    if isempty(ext)
        % set default extension
        ext = '.xls';
    end
    target = fullfile(D,[fil  ext]); 
    
else
    target = fullfile(pwd,'mat_get_set.xls');
    
end
if ~exist(target,'file');
    % Create new workbook:
    sb = invoke(s.workbooks,'Add');
else
    sb = invoke(s.workbooks,'Open',target);
end
cu = findstr(region,':');
if isempty(cu),
    % Define the region into which data will be transferred
    [M N] = size(A);
    ru = findstr(region,'R');
    cu = findstr(region,'C');
    M1 = str2num(region(max(ru)+1:cu-1));
    M2 = M1+M-1;
    N1 = str2num(region(max(cu)+1:end));
    N2 = N1+N-1;
    region = [region ':R' num2str(M2) 'C' num2str(N2)];
end

invoke(s,'goto',region);
GLO_OK=set(s.selection,'Value',A);
if strcmp(class(GLO_OK),'activex'),
    if ~exist(target,'file')
        % Save file as new:
        GLO_OK=invoke(sb,'SaveAs',target,1);
    else
        GLO_OK=invoke(sb,'Save');
    end
    
    end
if GLO_OK ~= 0,
    conf = 0;
else
    conf = 1;
end

invoke(sb,'Close');
% Quit Excel
invoke(s,'Quit');

% Close Excel and terminate ActiveX:
delete(s);