function dragzoom(hObj)
%DRAGZOOM Drag and zoom tool
%
% Description:
%   DRAGZOOM allows you to interactively manage the axes in figure.
%   This simple function for usable draging and zooming of axes, using the 
%   mouse and the keyboard shortcuts.
%
%   - 2D-Plots, 3D-plots and Images are supported;
%   - Supports multiple mixed (subplots) (2D, 3D) axes on one figure.
%
% Using:
%   dragzoom()
%   dragzoom(hFig)
%   dragzoom(hAx)
%
% Input:
%   hObj -- Figure or axes handle
%
%
% Interactive mode:
%   Available the following actions:
% 
%   Mouse actions in 2D mode:
%   Normal mode:
%       single-click and holding LB : Activation Drag mode
%       single-click and holding RB : Activation Rubber Band for region zooming
%       single-click MB             : Activation 'Extend' Zoom mode
%       scroll wheel MB             : Activation Zoom mode
%       double-click LB, RB, MB     : Reset to Original View
% 
%   Magnifier mode:
%       single-click LB             : Not Used
%       single-click RB             : Not Used
%       single-click MB             : Reset Magnifier to Original View
%       scroll MB                   : Change Magnifier Zoom
%       double-click LB             : Increase Magnifier Size
%       double-click RB             : Decrease Magnifier Size
% 
%   Hotkeys in 2D mode:
%       '+'                         : Zoom plus
%       '-'                         : Zoom minus
%       '0'                         : Set default axes (reset to original view)
%       'uparrow'                   : Up or down (inrerse) drag
%       'downarrow'                 : Down or up (inverse) drag
%       'leftarrow'                 : Left or right (inverse) drag
%       'rightarrow'                : Right or left (inverse) drag
%       'c'                         : On/Off Pointer Symbol 'fullcrosshair'
%       'g'                         : On/Off axes grid (only plots)
%       'x'                         : If pressed and holding, zoom and drag works only for X axis
%       'y'                         : If pressed and holding, zoom and drag works only for Y axis
%       'm'                         : If pressed and holding, Magnifier mode on
% 
% 
%   Mouse actions in 3D mode:
%       single-click and holding LB : Activation Drag mode
%       single-click and holding MB : Activation 'Extend' Zoom mode
%       single-click and holding RB : Activation Rotate mode
%       scroll wheel MB             : Activation Zoom mode
%       double-click LB, RB, MB     : Reset to Original View
% 
%   Hotkeys in 3D mode:
%       '+'                         : Zoom plus
%       '-'                         : Zoom minus  
%       '0'                         : Set default axes (reset to original view)
%       'uparrow'                   : Rotate up-down
%       'downarrow'                 : Rotate down-up
%       'leftarrow'                 : Rotate left-right
%       'rightarrow'                : Rotate right-left
%       'ctrl'+'uparrow'            : Up or down (inrerse) drag
%       'ctrl'+'downarrow'          : Down or up (inverse) drag
%       'ctrl'+'leftarrow'          : Left or right (inverse) drag
%       'ctrl'+'rightarrow'         : Right or left (inverse) drag
%       '1'                         : Go to X-Y view
%       '2'                         : Go to X-Z view
%       '3'                         : Go to Y-Z view
%       'v'                         : On/Off Visible Axes
%       'f'                         : On/Off Fixed Aspect Ratio
% 
%
% Example:
%   x = -pi:0.1:pi;
%   y = sin(x);
%   figure; plot(x, y);
%   dragzoom
%
% Example:
%   I = imread('cameraman.tif');
%   figure; imshow(I, []);
%   dragzoom
%
% Example:
%   figure;
%   x = -pi*2:0.1:pi*2;
%   y1 = sin(x);
%   y2 = cos(x);
%   subplot(2,1,1); plot(x,y1, '.-r')
%   title('Income')
%   subplot(2,1,2); plot(x, y2, 'o-b')
%   title('Outgo')
%   dragzoom;
%
% Example:
%   figure;
%   x = -pi*2:0.1:pi*2;
%   y1 = sin(x);
%   y2 = cos(x);
%   hax1 = subplot(2,1,1); plot(x,y1, '.-r')
%   title('Income')
%   hax2 = subplot(2,1,2); plot(x, y2, 'o-b')
%   title('Outgo')
%   dragzoom(hax1); % manage only axes 1
%
% Example:
%   figure;
%   k = 5;
%   n = 2^k-1;
%   [x,y,z] = sphere(n);
%   surf(x,y,z);
%   dragzoom;
%
%
% See Also PAN, ZOOM, PANZOOM
%

% -------------------------------------------------------------------------
%   Version   : 0.7.1
%   Author    : Evgeny Pr aka iroln <esp.home@gmail.com>
%   Created   : 10.07.10
%   Updated   : 13.11.10
%
%   Copyright : Evgeny Pr (c) 2010
% -------------------------------------------------------------------------


error(nargchk(0, 1, nargin));

if (nargin < 1), hObj = gcf; end

validateattributes(hObj, {'double'}, {'scalar'}, ...
    mfilename('fullpath'), '"Figure or Axes handle"', 1);

if ishandle(hObj)
    if ~strcmpi(get(hObj,'Type'), {'figure', 'axes'})
        error('Input argument must be a figure or axes handle');
    end
else
    error('Input argument must be a figure or axes handle');
end


% handles
hFig = [];
hAx = [];

% variables
mOrigCallbacks = [];
mAxesInfo = [];
mDragStartX = [];
mDragStartY = [];
mDragKeysX = [];
mDragKeysY = [];
mDragShiftStep = [];
mDrag3DShiftStep = [];
mZoomScroll = [];
mZoomMinPow = [];
mZoomMaxPow = [];
mZoomNum = [];
mZoomExtendNum = [];
mZoomKeysNum = [];
mZoom3DExtendNum = [];
mZoom3DKeysNum = [];
mZoom3DIndex = [];
mDefaultZoomGrid = [];
mDefaultZoomSteps = [];
mZoomGrid = [];
mZoomSteps = [];
mZoomIndexX = [];
mZoomIndexY = [];
mZoom3DStartX = [];
mZoom3DStartY = [];
mZoom3DBindX = [];
mZoom3DBindY = [];
mDefaultXLim = [];
mDefaultYLim = [];
mDefaultAxPos = [];
mRotStartAZ = [];
mRotStartEL = [];
mRotStartX = [];
mRotStartY = [];
mRot3DKeysInc = [];
mPointerCross = [];
mRubberBand = [];
mRbEdgeColor = [];
mRbFaceColor = [];
mRbFaceAlpha = [];
mMagnifier = [];
mMgSize = [];
mMgMinSize = [];
mMgMaxSize = [];
mMgZoom = [];
mMgMinZoom = [];
mMgMaxZoom = [];
mMgLinesWidth = [];
mMgShadow = [];
mMgSizeStep = [];
mMgZoomStep = [];
mMgDirection = [];

% flags
fIsSelectedCurrentAxes = true;
fIsDragAllowed = false;
fIsZoomExtendAllowed = false;
fIsZoomExtend3DAllowed = false;
fIsRotate3DAllowed = false;
fIsRubberBandOn = false;
fIsPointerCross = false;
fIsAxesGrid = false;
fIsSmoothing = false;
fIsEnableDragX = true;
fIsEnableDragY = true;
fIsEnableZoomX = true;
fIsEnableZoomY = true;
fIsAxes2D = false;
fIsImage = false;
fIsMagnifierOn = false;
fIsEnableControl = false;

% Initialize and Setup
if ~Initialize()
    % some errors...
    return;
end
%--------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%==========================================================================
    function Setup()
        %Setup setup options
        
        % Drag Options
        mDragKeysX = 'normal';      % 'normal', 'reverse'
        mDragKeysY = 'normal';      % 'normal', 'reverse'
        mDragShiftStep = 3;         % step dragging on keys
        mDrag3DShiftStep = 10;      % step dragging 3D on keys
        
        % Zoom Options
        mZoomScroll = 'normal';     % 'normal', 'reverse'
        mZoomMinPow = 0;            % min zoom percent 10 ^ mZoomMinPow
        mZoomMaxPow = 5;            % max zoom perzent 10 ^ mZoomMaxPow
        mZoomNum = 51;              % count steps of log zoom grid
        mZoomExtendNum = 301;       % count steps of log grid zoom extend for 2D
        mZoomKeysNum = 181;         % count steps of log grid zoom for keys for 2D
        mZoom3DExtendNum = 201;     % count steps of log grid zoom extend for 3D
        mZoom3DKeysNum = 181;       % count steps of log grid zoom for keys for 3D
        
        % Rubber Band Options
        mRbEdgeColor = 'k';         % rubber band edge color
        mRbFaceColor = 'none';      % rubber band face color
        mRbFaceAlpha = 1;           % rubber band face alpha (transparency)
        
        % Magnifier Options
        mMgSize = 0.2;              % default relative size of magnifier
        mMgMinSize = 0.05;          % min size of magnifier
        mMgMaxSize = 0.4;           % max size of magnifier
        mMgZoom = 2;                % default zoom on magnifier
        mMgMinZoom = 1;             % min zoom on magnifier
        mMgMaxZoom = 100;           % max zoom on magnifier
        mMgLinesWidth = 1;          % lines width on magnifier
        mMgShadow = 0.95;           % shadow area without magnifier
        mMgSizeStep = 1.2;          % step change in the size
        mMgZoomStep = 1.2;          % step change in the zoom
                
        % Rotate Options
        mRot3DKeysInc = 3;          % rotate increase for keys
    end
%--------------------------------------------------------------------------

%==========================================================================
    function tf = Initialize()
        %Initialize initialize tool
        
        tf = true;
        
        if ~ParseInputs()
            tf = false;
            return;
        end
        
        % save initialize view
        resetplotview(hAx, 'InitializeCurrentView');
        
        % setup tool
        Setup();
        
        % save original callbacks
        SaveOriginalCallbacks();
        
        % get info about all axes and create axes info struct
        mAxesInfo = GetAxesInfo();
        axi = GetCurrentAxesIndex();
        
        SetCurrentAxes(axi);
        SetDefaultZoomGrid();
        EnableCallbacks();
        
        % In case the figure will be saved
        set(hFig, 'CreateFcn', {@CreateFigure});
        set(hFig, 'UserData', mAxesInfo); 
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SaveOriginalCallbacks()
        %SaveOriginalCallbacks
        
        mOrigCallbacks.window_button_down_fcn   = get(hFig, 'WindowButtonDownFcn');
        mOrigCallbacks.window_button_up_fcn     = get(hFig, 'WindowButtonUpFcn');
        mOrigCallbacks.window_button_motion_fcn = get(hFig, 'WindowButtonMotionFcn');
        mOrigCallbacks.window_scroll_whell_fcn  = get(hFig, 'WindowScrollWheelFcn');
        mOrigCallbacks.window_key_press_fcn     = get(hFig, 'WindowKeyPressFcn');
        mOrigCallbacks.window_key_release_fcn   = get(hFig, 'WindowKeyReleaseFcn');
    end
%--------------------------------------------------------------------------

%==========================================================================
    function EnableCallbacks()
        %EnableCallbacks
        
        if fIsAxes2D
            % 2D mode
            SetFigureCallbacks2D();
        else
            % 3D mode
            SetFigureCallbacks3D();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
%     function DisableCallbacks()
%         %DisableCallbacks
%         
%         % set original callbacks
%         set(hFig, ...
%             'WindowButtonDownFcn',      mOrigCallbacks.window_button_down_fcn, ...
%             'WindowButtonUpFcn',        mOrigCallbacks.window_button_up_fcn, ...
%             'WindowButtonMotionFcn',    mOrigCallbacks.window_button_motion_fcn, ...
%             'WindowScrollWheelFcn',     mOrigCallbacks.window_scroll_whell_fcn, ...
%             'WindowKeyPressFcn',        mOrigCallbacks.window_key_press_fcn, ...
%             'WindowKeyReleaseFcn',      mOrigCallbacks.window_key_release_fcn);
%     end
%--------------------------------------------------------------------------

%==========================================================================
    function SetFigureCallbacks2D()
        %SetFigureCallbacks2D set callback-functions for processing figure events in mode 2D
        
        set(hFig, ...
            'WindowButtonDownFcn',      {@WindowButtonDownCallback2D}, ...
            'WindowButtonUpFcn',        {@WindowButtonUpCallback2D}, ...
            'WindowButtonMotionFcn',    {@WindowButtonMotionCallback2D}, ...
            'WindowScrollWheelFcn',     {@WindowScrollWheelFcn2D}, ...
            'WindowKeyPressFcn',        {@WindowKeyPressCallback2D}, ...
            'WindowKeyReleaseFcn',      {@WindowKeyReleaseCallback2D});
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetFigureCallbacks3D()
        %SetFigureCallbacks3D set callback-functions for processing figure events in mode 3D
        
        set(hFig, ...
            'WindowButtonDownFcn',      {@WindowButtonDownCallback3D}, ...
            'WindowButtonUpFcn',        {@WindowButtonUpCallback3D}, ...
            'WindowButtonMotionFcn',    {@WindowButtonMotionCallback3D}, ...
            'WindowScrollWheelFcn',     {@WindowScrollWheelFcn3D}, ...
            'WindowKeyPressFcn',        {@WindowKeyPressCallback3D}, ...
            'WindowKeyReleaseFcn',      {@WindowKeyReleaseCallback3D});
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowButtonDownCallback2D(src, evnt)    %#ok
        %WindowButtonDownCallback2D

        clickType = get(src, 'SelectionType');
        
        switch clickType
            case 'normal'
                DragMouseBegin();
                mMgDirection = 'plus';
            case 'open'
                if fIsMagnifierOn
                    MagnifierSizeChange(mMgDirection);
                else
                    ResetAxesToOrigView();
                end
            case 'alt'
                RubberBandBegin();
                mMgDirection = 'minus';
            case 'extend'
                if fIsMagnifierOn
                    MagnifierReset();
                else
                    ZoomMouseExtendBegin();
                end
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowButtonUpCallback2D(src, evnt)      %#ok
        %WindowButtonUpCallback2D
        
        DragMouseEnd();
        ZoomMouseExtendEnd();
        RubberBandEnd();
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowButtonMotionCallback2D(src, evnt)  %#ok
        %WindowButtonMotionCallback2D
        
        if ~(fIsMagnifierOn || fIsDragAllowed || fIsRubberBandOn)
            % set current axes under cursor
            SelectAxesUnderCursor();
        end
        
        if fIsEnableControl
            DragMouse();
            RubberBandUpdate();
            MagnifierUpdate();
        end
        
        ZoomMouseExtend();
        PointerCrossUpdate();
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowScrollWheelFcn2D(src, evnt)        %#ok
        %WindowScrollWheelFcn2D
        
        % Update Zoom Info
        % because it can be changed function 'zoom'
        UpdateCurrentZoomAxes();
        
        switch mZoomScroll
            case 'normal'
                directions = {'minus', 'plus'};
            case 'reverse'
                directions = {'plus', 'minus'};
        end
        
        verScrollCount = evnt.VerticalScrollCount;
        
        if (verScrollCount > 0)
            direction = directions{1};
        elseif (verScrollCount < 0)
            direction = directions{2};
        else
            return;
        end
        
        % if fIsEnableControl
        ZoomMouse(direction);
        PointerCrossUpdate();
        % end
        MagnifierZoomChange(direction);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowKeyPressCallback2D(src, evnt)      %#ok
        %WindowKeyPressCallback2D
        
        switch evnt.Key
            case '0'
                ResetAxesToOrigView();
            case {'equal', 'add'}
                ZoomKeys('plus');
            case {'hyphen', 'subtract'}
                ZoomKeys('minus');
            case 'leftarrow'
                DragKeys('left');
            case 'rightarrow'
                DragKeys('right');
            case 'uparrow'
                DragKeys('up');
            case 'downarrow'
                DragKeys('down');
            case 'c'
                SetPointerCrossKeys();
            case 'g'
                SetAxesGridKeys();
            case 'x'
                DragEnable('y', 'off');
                ZoomEnable('y', 'off');
            case 'y'
                DragEnable('x', 'off');
                ZoomEnable('x', 'off');
            case 'm'
                if fIsEnableControl
                    MagnifierOn();
                end
            case 'z'
                
            case 's'
                % smooth plot
                SetSmoothKeys();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowKeyReleaseCallback2D(src, evnt)    %#ok
        %WindowKeyReleaseCallback2D
        
        switch evnt.Key
            case 'x'
                DragEnable('y', 'on');
                ZoomEnable('y', 'on');
            case 'y'
                DragEnable('x', 'on');
                ZoomEnable('x', 'on');
            case 'm'
                MagnifierOff();
            case 'z'
                %TODO: one-time increase
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowButtonDownCallback3D(src, evnt)    %#ok
        %WindowButtonDownCallback3D
        
        clickType = get(src, 'SelectionType');
        
        switch clickType
            case 'normal'
                DragMouseBegin3D();
            case 'open'
                ResetAxesToOrigView3D();
            case 'alt'
                RotateMouseBegin3D();
            case 'extend'
                ZoomMouseExtendBegin3D();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowButtonUpCallback3D(src, evnt)    %#ok
        %WindowButtonUpCallback3D
        
        DragMouseEnd3D();
        ZoomMouseExtendEnd3D();
        RotateMouseEnd3D();
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowButtonMotionCallback3D(src, evnt)    %#ok
        %WindowButtonMotionCallback3D
        
        if ~(fIsDragAllowed || fIsZoomExtend3DAllowed || fIsRotate3DAllowed)
            SelectAxesUnderCursor();
        end
        
        if fIsEnableControl
            DragMouse3D();
            RotateMouse3D();
        end
        ZoomMouseExtend3D();
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowScrollWheelFcn3D(src, evnt)    %#ok
        %WindowScrollWheelFcn3D
        
        UpdateCurrentZoomAxes3D();
        
        switch mZoomScroll
            case 'normal'
                directions = {'minus', 'plus'};
            case 'reverse'
                directions = {'plus', 'minus'};
        end
        
        verScrollCount = evnt.VerticalScrollCount;
        
        if (verScrollCount > 0)
            direction = directions{1};
        elseif (verScrollCount < 0)
            direction = directions{2};
        else
            return;
        end
        
        % if fIsEnableControl
        ZoomMouse3D(direction);
        % end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowKeyPressCallback3D(src, evnt)    %#ok
        %WindowKeyPressCallback3D
        
        isModifier = ~isempty(intersect(evnt.Modifier, {'control', 'command'}));
        
        switch evnt.Key
            case {'equal', 'add'}
                ZoomKeys3D('plus');
            case {'hyphen', 'subtract'}
                ZoomKeys3D('minus');
            case 'leftarrow'
                if isModifier
                    DragKeys3D('left');
                else
                    RotateKeys3D('az-');
                end
            case 'rightarrow'
                if isModifier
                    DragKeys3D('right');
                else
                    RotateKeys3D('az+');
                end
            case 'uparrow'
                if isModifier
                    DragKeys3D('up');
                else
                    RotateKeys3D('el-');
                end
            case 'downarrow'
                if isModifier
                    DragKeys3D('down');
                else
                    RotateKeys3D('el+');
                end
            case '0'
                ResetAxesToOrigView3D();
            case '1'
                RotateKeys3D('xy');
            case '2'
                RotateKeys3D('xz');
            case '3'
                RotateKeys3D('yz');
            case 'v'
                VisibleAxesKeys3D();
            case 'f'
                SwitchAspectRatioKeys3D()
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function WindowKeyReleaseCallback3D(src, evnt)    %#ok
        %WindowKeyReleaseCallback3D
        
        switch evnt.Key
            case {'equal', 'add', 'hyphen', 'subtract'}
                SetDefaultZoomGrid3D();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function CreateFigure(src, ~)
        %CreateFigure
        % To fix error "Invalid handle object" work with the saved figures.
        
        hFig = src;
        mAxesInfo = get(hFig, 'UserData');
        axi = GetCurrentAxesIndex();
        SetCurrentAxes(axi)
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragMouseBegin()
        %DragMouseBegin begin draging

        if (~fIsDragAllowed && ~fIsMagnifierOn)
            [acx, acy] = GetCursorCoordOnAxes();
            
            mDragStartX = acx;
            mDragStartY = acy;
            
            fIsDragAllowed = true;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragMouseEnd()
        %DragMouseEnd end draging

        if fIsDragAllowed
            fIsDragAllowed = false;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragMouse()
        %DragMouse �������������� ���� ������

        if fIsDragAllowed
            [acx, acy] = GetCursorCoordOnAxes();
            
            dx = mDragStartX - acx;
            dy = mDragStartY - acy;
            
            DragAxes(dx, dy);
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragKeys(direction)
        %DragKeys
        
        [xLim, yLim] = GetAxesLimits();
        
        xRange = range(xLim);
        yRange = range(yLim);
        
        dx = mDragShiftStep * xRange / 100;
        dy = mDragShiftStep * yRange / 100;
        
        directionsX = {'right', 'left'};
        directionsY = {'down', 'up'};
        
        switch mDragKeysX
            case 'normal'
            case 'reverse'
                directionsX = fliplr(directionsX);
        end
        switch mDragKeysY
            case 'normal'
            case 'reverse'
                directionsY = fliplr(directionsY);
        end
        
        switch direction
            case directionsX{1}
                DragAxes(-dx, 0);
            case directionsX{2}
                DragAxes(dx, 0);
            case directionsY{1}
                DragAxes(0, dy);
            case directionsY{2}
                DragAxes(0, -dy);
        end
        
        PointerCrossUpdate();
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragAxes(dx, dy)
        %DragAxes
        
        [xLim, yLim] = GetAxesLimits();
        
        if fIsEnableDragX
            xLim = xLim + dx;
        end
        if fIsEnableDragY
            yLim = yLim + dy;
        end
        
        SetAxesLimits(xLim, yLim);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragMouseBegin3D()
        %DragMouseBegin3D
        
        if ~fIsDragAllowed
            [wcx, wcy] = GetCursorCoordOnWindow('pixels');
            
            mDragStartX = wcx;
            mDragStartY = wcy;
            
            fIsDragAllowed = true;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragMouseEnd3D()
        %DragMouseEnd3D
        
        if fIsDragAllowed
            fIsDragAllowed = false;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragMouse3D()
        %DragMouse3D
        
        if fIsDragAllowed
            [wcx, wcy] = GetCursorCoordOnWindow('pixels');
            
            dx = mDragStartX - wcx;
            dy = mDragStartY - wcy;
            
            DragAxes3D(dx, dy);
            
            mDragStartX = wcx;
            mDragStartY = wcy;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragKeys3D(direction)
        %DragKeys3D
        
        dx = mDrag3DShiftStep;
        dy = mDrag3DShiftStep;
        
        directionsX = {'right', 'left'};
        directionsY = {'down', 'up'};
        
        switch mDragKeysX
            case 'normal'
            case 'reverse'
                directionsX = fliplr(directionsX);
        end
        switch mDragKeysY
            case 'normal'
            case 'reverse'
                directionsY = fliplr(directionsY);
        end
        
        switch direction
            case directionsX{1}
                DragAxes3D(-dx, 0);
            case directionsX{2}
                DragAxes3D(dx, 0);
            case directionsY{1}
                DragAxes3D(0, dy);
            case directionsY{2}
                DragAxes3D(0, -dy);
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragAxes3D(dx, dy)
        %DragAxes3D
        
        axPos = GetObjPos(hAx, 'pixels');
        
        axPos(1) = axPos(1) - dx;
        axPos(2) = axPos(2) - dy;
        
        SetObjPos(hAx, axPos, 'pixels');
    end
%--------------------------------------------------------------------------

%==========================================================================
    function DragEnable(ax, action)
        %DragEnable
        
        switch lower(action)
            case 'on'
                tf = true;
            case 'off'
                tf = false;
        end
                
        switch lower(ax)
            case 'x'
                fIsEnableDragX = tf;
            case 'y'
                fIsEnableDragY = tf;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomEnable(ax, action)
        %ZoomEnable
        
        switch lower(action)
            case 'on'
                tf = true;
            case 'off'
                tf = false;
        end
                
        switch lower(ax)
            case 'x'
                fIsEnableZoomX = tf;
            case 'y'
                fIsEnableZoomY = tf;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomMouse(direction)
        %ZoomMouse zooming axes with mouse
        
        if (IsZoomMouseAllowed && ~fIsMagnifierOn)
            [acx, acy] = GetCursorCoordOnAxes();
            ZoomAxes(direction, acx, acy)
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomMouseExtendBegin()
        %ZoomMouseExtendBegin
        
        if ~fIsZoomExtendAllowed
            UpdateCurrentZoomAxes();
            
            % set new zoom grid for extend zoom
            [mZoomGrid, mZoomSteps] = ZoomLogGrid(mZoomMinPow, mZoomMaxPow, mZoomExtendNum);
            UpdateCurrentZoomAxes();
            
            [wcx, wcy] = GetCursorCoordOnWindow('pixels');
            [acx, acy] = GetCursorCoordOnAxes();
            
            mZoom3DStartX = wcx;
            mZoom3DStartY = wcy;
            
            mZoom3DBindX = acx;
            mZoom3DBindY = acy;
            
            fIsZoomExtendAllowed = true;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomMouseExtendEnd()
        %ZoomMouseExtendEnd
        
        if fIsZoomExtendAllowed
            % set default zoom grid
            SetDefaultZoomGrid();
            fIsZoomExtendAllowed = false;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomMouseExtend()
        %ZoomMouseExtend
        
        if fIsZoomExtendAllowed
            directions = {'minus', 'plus'};
            
            switch mZoomScroll
                case 'normal'
                case 'reverse'
                    directions = fliplr(directions);
            end
        
            % Heuristic for pixel change to camera zoom factor 
            % (taken from function ZOOM)
            [wcx, wcy] = GetCursorCoordOnWindow('pixels');
            
            xy(1) = wcx - mZoom3DStartX;
            xy(2) = wcy - mZoom3DStartY;
            q = max(-0.9, min(0.9, sum(xy)/70)) + 1;
   
            if (q < 1)
                direction = directions{1};
            elseif (q > 1)
                direction = directions{2};
            else
                return;
            end
            
            ZoomAxes(direction, mZoom3DBindX, mZoom3DBindY)
            
            mZoom3DStartX = wcx;
            mZoom3DStartY = wcy;            
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomKeys(direction)
        %ZoomKeys zooming axes with keyboard
        
        UpdateCurrentZoomAxes();
        
        [mZoomGrid, mZoomSteps] = ZoomLogGrid(mZoomMinPow, mZoomMaxPow, mZoomKeysNum);
        UpdateCurrentZoomAxes();
        
        [acx, acy] = GetCursorCoordOnAxes();
        
        ZoomAxes(direction, acx, acy)
        PointerCrossUpdate();
        SetDefaultZoomGrid();
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomAxes(direction, cx, cy)
        %ZoomAxes
        
        [xLim, yLim] = GetAxesLimits();
        
        if ~fIsImage
            if fIsEnableZoomX
                mZoomIndexX = ChangeZoomIndex(direction, mZoomIndexX);
                zoomPct = GetZoomPercent(mZoomIndexX);
                xLim = RecalcZoomAxesLimits(xLim, mDefaultXLim, cx, zoomPct);
            end
            if fIsEnableZoomY
                mZoomIndexY = ChangeZoomIndex(direction, mZoomIndexY);
                zoomPct = GetZoomPercent(mZoomIndexY);
                yLim = RecalcZoomAxesLimits(yLim, mDefaultYLim, cy, zoomPct);
            end
        else
            mZoomIndexX = ChangeZoomIndex(direction, mZoomIndexX);
            mZoomIndexY = mZoomIndexX;
            zoomPct = GetZoomPercent(mZoomIndexX);
            xLim = RecalcZoomAxesLimits(xLim, mDefaultXLim, cx, zoomPct);
            yLim = RecalcZoomAxesLimits(yLim, mDefaultYLim, cy, zoomPct);
        end
        
        SetAxesLimits(xLim, yLim);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function zoomPct = GetZoomPercent(zoomIndex, zoomGrid)
        %GetZoomPercent get zoom percent

        if (nargin < 2)
            zoomGrid = mZoomGrid;
        end
        
        zoomPct = zoomGrid(zoomIndex);       
    end
%--------------------------------------------------------------------------

%==========================================================================
    function zoomIndex = ChangeZoomIndex(direction, zoomIndex, zoomSteps)
        %ChangeZoomIndex
        
        if (nargin < 3)
            zoomSteps = mZoomSteps;
        end
        
        switch direction
            case 'plus'
                if (zoomIndex < zoomSteps)
                    zoomIndex = zoomIndex + 1;
                end
            case 'minus'
                if (zoomIndex > 1)
                    zoomIndex = zoomIndex - 1;
                end
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function axLim = RecalcZoomAxesLimits(axLim, axLimDflt, zcCrd, zoomPct)
        %RecalcZoomAxesLimits recalc axes limits
        
        if (zcCrd < axLim(1)), zcCrd = axLim(1); end
        if (zcCrd > axLim(2)), zcCrd = axLim(2); end
        
        rf = range(axLim);
        ra = range([axLim(1), zcCrd]);
        rb = range([zcCrd, axLim(2)]);
        
        cfa = ra / rf; 
        cfb = rb / rf;
        
        newRange = range(axLimDflt) * 100 / zoomPct;
        dRange = newRange - rf;
        
        axLim(1) = axLim(1) - dRange * cfa;
        axLim(2) = axLim(2) + dRange * cfb;
    end
%--------------------------------------------------------------------------

%==========================================================================
    function UpdateCurrentZoomAxes()
        %UpdateCurrentZoomAxes
        
        [xLim, yLim] = GetAxesLimits();
        
        [curentZoomX, curentZoomY] = GetCurrentZoomAxesPercent(xLim, yLim);
        
        if (curentZoomX ~= GetZoomPercent(mZoomIndexX))
            [nu, mZoomIndexX] = min(abs(mZoomGrid - curentZoomX));  %#ok ([~, ...])
        end
        if (curentZoomY ~= GetZoomPercent(mZoomIndexY))
            [nu, mZoomIndexY] = min(abs(mZoomGrid - curentZoomY));  %#ok ([~, ...])
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function [curentZoomX, curentZoomY] = GetCurrentZoomAxesPercent(xLim, yLim)
        %GetCurrentZoomAxesPercent
        
        curentZoomX = range(mDefaultXLim) * 100 / range(xLim);
        curentZoomY = range(mDefaultYLim) * 100 / range(yLim);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomMouse3D(direction)
        %ZoomMouse3D
        
        if IsZoomMouseAllowed
            [wcx, wcy] = GetCursorCoordOnWindow('pixels');
            ZoomAxes3D(direction, wcx, wcy)
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomMouseExtendBegin3D()
        %ZoomMouseExtendBegin3D
        
        if ~fIsZoomExtend3DAllowed
            UpdateCurrentZoomAxes3D();
            [wcx, wcy] = GetCursorCoordOnWindow('pixels');
            
            % set new zoom grid for extend zoom
            SetNewZoomGrid3D(mZoomMinPow, mZoomMaxPow, mZoom3DExtendNum);
            
            mZoom3DStartX = wcx;
            mZoom3DStartY = wcy;
            
            mZoom3DBindX = wcx;
            mZoom3DBindY = wcy;
            
            fIsZoomExtend3DAllowed = true;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomMouseExtendEnd3D()
        %ZoomMouseExtendEnd3D
        
        if fIsZoomExtend3DAllowed
            SetDefaultZoomGrid3D();
            
            fIsZoomExtend3DAllowed = false;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomMouseExtend3D()
        %ZoomMouseExtended3D
        
        if fIsZoomExtend3DAllowed
            directions = {'minus', 'plus'};
            
            switch mZoomScroll
                case 'normal'
                case 'reverse'
                    directions = fliplr(directions);
            end
        
            % Heuristic for pixel change to camera zoom factor 
            % (taken from function ZOOM)
            [wcx, wcy] = GetCursorCoordOnWindow('pixels');
            
            xy(1) = wcx - mZoom3DStartX;
            xy(2) = wcy - mZoom3DStartY;
            q = max(-0.9, min(0.9, sum(xy)/70)) + 1;
   
            if (q < 1)
                direction = directions{1};
            elseif (q > 1)
                direction = directions{2};
            else
                return;
            end
            
            ZoomAxes3D(direction, mZoom3DBindX, mZoom3DBindY)
            
            mZoom3DStartX = wcx;
            mZoom3DStartY = wcy;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomAxes3D(direction, cx, cy)
        %ZoomAxes3D
        
        mZoom3DIndex = ChangeZoomIndex(direction, mZoom3DIndex, mZoomSteps);
        zoomPct = GetZoomPercent(mZoom3DIndex, mZoomGrid);
        
        axPos = GetObjPos(hAx, 'pixels');
        
        [axPos(1), axPos(3)] = RecalcZoomAxesPos3D(axPos(1), axPos(3), ...
            mDefaultAxPos(3), cx, zoomPct);
        
        [axPos(2), axPos(4)] = RecalcZoomAxesPos3D(axPos(2), axPos(4), ...
            mDefaultAxPos(4), cy, zoomPct);
        
        SetObjPos(hAx, axPos, 'pixels');
    end
%--------------------------------------------------------------------------

%==========================================================================
    function [npc, nsz] = RecalcZoomAxesPos3D(pc, sz, dfltSz, zcCrd, zoomPct)
        %RecalcZoomAxesPos3D
        
        dd = dfltSz * zoomPct / 100 - sz;
        rng = range([pc zcCrd]);
        cf = rng / sz; 
        nsz = sz + dd;
        npc = pc - dd * cf;
    end
%--------------------------------------------------------------------------
        
%==========================================================================
    function UpdateCurrentZoomAxes3D()
        %UpdateCurrentZoomAxes3D
        
        curentZoom = GetCurrentZoomAxesPercent3D();
        [nu, mZoom3DIndex] = min(abs(mZoomGrid - curentZoom));  %#ok ([~, ...])
    end
%--------------------------------------------------------------------------

%==========================================================================
    function currentZoom = GetCurrentZoomAxesPercent3D()
        %GetCurrentZoomAxesPercent3D

        axPos = GetObjPos(hAx, 'pixels');
        currentZoom = axPos(3) / mDefaultAxPos(3) * 100;
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ZoomKeys3D(direction)
        %ZoomKeys3D
        
        UpdateCurrentZoomAxes3D();
        SetNewZoomGrid3D(mZoomMinPow, mZoomMaxPow, mZoom3DKeysNum);
        
        [wcx, wcy] = GetCursorCoordOnWindow('pixels');
        ZoomAxes3D(direction, wcx, wcy)
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetNewZoomGrid3D(minPow, maxPow, num)
        %SetNewZoomGrid3D set new zoom grid for 3D mode
        
        [mZoomGrid, mZoomSteps] = ZoomLogGrid(minPow, maxPow, num);
        UpdateCurrentZoomAxes3D();
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetDefaultZoomGrid()
        %SetDefaultZoomGrid set default zoom grid
        
        [mDefaultZoomGrid, mDefaultZoomSteps] = ...
            ZoomLogGrid(mZoomMinPow, mZoomMaxPow, mZoomNum);
        
        mZoomGrid = mDefaultZoomGrid;
        mZoomSteps = mDefaultZoomSteps;
        
        mZoomIndexX = find(mZoomGrid == 100);
        mZoomIndexY = mZoomIndexX;
        mZoom3DIndex = mZoomIndexX;
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetDefaultZoomGrid3D()
        %SetDefaultZoomGrid for 3D mode
        
        mZoomGrid = mDefaultZoomGrid;
        mZoomSteps = mDefaultZoomSteps;
        UpdateCurrentZoomAxes3D();
    end
%--------------------------------------------------------------------------

%==========================================================================
    function VisibleAxesKeys3D()
        %VisibleAxesKeys3D
        
        axi = GetCurrentAxesIndex();
        
        if mAxesInfo(axi).isvisible
            set(hAx, 'Visible', 'off');
            mAxesInfo(axi).isvisible = false;
        else
            set(hAx, 'Visible', 'on');
            mAxesInfo(axi).isvisible = true;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SwitchAspectRatioKeys3D()
        %SwitchAspectRatioKeys3D
        
        axi = GetCurrentAxesIndex();
        
        if mAxesInfo(axi).isvis3d
            axis(hAx, 'normal');
            mAxesInfo(axi).isvis3d = false;
        else
            axis(hAx, 'vis3d');
            mAxesInfo(axi).isvis3d = true;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function RotateMouseBegin3D()
        %RotateMouseBegin3D
        
        if ~fIsRotate3DAllowed
            [wcx, wcy] = GetCursorCoordOnWindow('pixels');
            
            [az, el] = view(hAx);
            
            mRotStartAZ = az;
            mRotStartEL = el;
            mRotStartX = wcx;
            mRotStartY = wcy;
                        
            fIsRotate3DAllowed = true;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function RotateMouseEnd3D()
        %RotateMouseEnd3D
        
        if fIsRotate3DAllowed
            fIsRotate3DAllowed = false;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function RotateMouse3D()
        %RotateMouse3D
        
        if fIsRotate3DAllowed
            [wcx, wcy] = GetCursorCoordOnWindow('pixels');
            
            dAZ = mRotStartX - wcx;
            dEL = mRotStartY - wcy;
            
            az = mRotStartAZ + dAZ;
            el = mRotStartEL + dEL;
            
            SetView3D(az, el);
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function RotateKeys3D(mode)
        %RotateKeys3D
        
        [az, el] = view(hAx);
                
        switch lower(mode)
            case 'xy'
                az = 0;
                el = 90;                
            case 'xz'
                az = 0;
                el = 0;
            case 'yz'
                az = 90;
                el = 0;
            case 'az+'
                az = az + mRot3DKeysInc;
            case 'az-'
                az = az - mRot3DKeysInc;
            case 'el+'
                el = el + mRot3DKeysInc;
            case 'el-'
                el = el - mRot3DKeysInc;
        end
        
        SetView3D(az, el)
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetView3D(az, el)
        %SetView3D
        
        if (el > 90), el = 90; end
        if (el < -90), el = -90; end
        
        view(hAx, [az, el]);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function PointerCrossOn()
        %PointerCrossOn
        
        if ~fIsPointerCross
            SetPointer('fullcrosshair');
            
            % text objects
            h = [
                text('Parent', hAx)     % left
                text('Parent', hAx)     % right
                text('Parent', hAx)     % bottom
                text('Parent', hAx)     % top
                ];
            
            % create pointer cross struct
            mPointerCross = struct(...
                'htext',    h, ...
                'left',     1, ...
                'right',    2, ...
                'bottom',   3, ...
                'top',      4);
            
            PointerCrossSetup();
            fIsPointerCross = true;
            PointerCrossUpdate();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function PointerCrossOff()
        %PointerCrossOff
        
        delete(mPointerCross.htext);
        SetPointer('arrow');
        fIsPointerCross = false;
    end
%--------------------------------------------------------------------------

%==========================================================================
    function PointerCrossSetup()
        %PointerCrossSetup
        
        left = mPointerCross.left;
        right = mPointerCross.right;
        bottom = mPointerCross.bottom;
        top = mPointerCross.top;
        
        vabt = {'top', 'bottom'};
        if fIsImage
            vabt = fliplr(vabt);
        end
        
        set(mPointerCross.htext(left), 'VerticalAlignment', 'bottom');
        set(mPointerCross.htext(right), 'VerticalAlignment', 'bottom');
        set(mPointerCross.htext(bottom), 'VerticalAlignment', vabt{1});
        set(mPointerCross.htext(top), 'VerticalAlignment', vabt{2});
        
        bgColor = [251 248 230]/255;
        set(mPointerCross.htext(left), 'BackgroundColor', bgColor);
        set(mPointerCross.htext(right), 'BackgroundColor', bgColor);
        set(mPointerCross.htext(bottom), 'BackgroundColor', bgColor);
        set(mPointerCross.htext(top), 'BackgroundColor', bgColor);
        
        edColor = [180 180 180]/255;
        set(mPointerCross.htext(left), 'EdgeColor', edColor);
        set(mPointerCross.htext(right), 'EdgeColor', edColor);
        set(mPointerCross.htext(bottom), 'EdgeColor', edColor);
        set(mPointerCross.htext(top), 'EdgeColor', edColor);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function PointerCrossUpdate()
        %PointerCrossUpdate
        
        if fIsPointerCross
            [xlim, ylim] = GetAxesLimits();
            [acx, acy] = GetCursorCoordOnAxes();
            
            left = mPointerCross.left;
            right = mPointerCross.right;
            bottom = mPointerCross.bottom;
            top = mPointerCross.top;
                        
            if fIsImage
                xValStr = sprintf(' %d ', round(acx));
                yValStr = sprintf(' %d ', round(acy));
            else                
                xValStr = sprintf(' %5.3g ', acx);
                yValStr = sprintf(' %5.3g ', acy);
            end
            
            set(mPointerCross.htext(left), 'String', yValStr);
            set(mPointerCross.htext(right), 'String', yValStr);
            set(mPointerCross.htext(bottom), 'String', xValStr);
            set(mPointerCross.htext(top), 'String', xValStr);
            
            extent = get(mPointerCross.htext(left), 'Extent');
            xx = extent(3);
            
            set(mPointerCross.htext(left), 'Position', [xlim(1)-xx, acy]);
            set(mPointerCross.htext(right), 'Position', [xlim(2) acy]);
            set(mPointerCross.htext(bottom), 'Position', [acx, ylim(1)]);
            set(mPointerCross.htext(top), 'Position', [acx, ylim(2)]);
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function RubberBandBegin()
        %RubberBandBegin
        
        if (~fIsRubberBandOn && ~fIsMagnifierOn)
            [acx, acy] = GetCursorCoordOnAxes();
            
            % create rubber band struct
            mRubberBand = struct(...
                'obj',	[patch('Parent', hAx), patch('Parent', hAx)], ...
                'x1',  	acx, ...
                'y1',  	acy, ...
                'x2',  	acx, ...
                'y2',  	acy);
            
            RubberBandSetPos();
            RubberBandSetup();
            fIsRubberBandOn = true;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function RubberBandEnd()
        %RubberBandEnd
        
        if fIsRubberBandOn
            fIsRubberBandOn = false;
        
            delete(mRubberBand.obj);            
            RubberBandZoomAxes();
            PointerCrossUpdate();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function RubberBandUpdate()
        %RubberBandUpdate
        
        if fIsRubberBandOn
            [acx, acy] = GetCursorCoordOnAxes();
            
            mRubberBand.x2 = acx;
            mRubberBand.y2 = acy;
            RubberBandSetPos();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function RubberBandSetPos()
        %RubberBandSetPos set position of rubber band
        
        x1 = mRubberBand.x1;
        y1 = mRubberBand.y1;
        x2 = mRubberBand.x2;
        y2 = mRubberBand.y2;
        
        set(mRubberBand.obj, ...
            'XData', [x1 x2 x2 x1], ...
            'YData', [y1 y1 y2 y2]);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function RubberBandSetup()
        %RubberBandSetup
        
        set(mRubberBand.obj(1), ...
            'EdgeColor', 'w', ...
            'FaceColor', 'none', ...
            'LineWidth', 1.5, ...
            'LineStyle', '-');
        
        set(mRubberBand.obj(2), ...
            'EdgeColor', mRbEdgeColor, ...
            'FaceColor', mRbFaceColor, ...
            'FaceAlpha', mRbFaceAlpha, ...
            'LineWidth', 0.5, ...
            'LineStyle', '-');    
    end
%--------------------------------------------------------------------------

%==========================================================================
    function RubberBandZoomAxes()
        %RubberBandZoomAxes apply zoom from rubber band
        
        xLim = sort([mRubberBand.x1, mRubberBand.x2]);
        yLim = sort([mRubberBand.y1, mRubberBand.y2]);
        
        if (range(xLim) == 0 || range(yLim) == 0)
            return;
        end
        
        [zoomPctX, zoomPctY] = GetCurrentZoomAxesPercent(xLim, yLim);
        
        if fIsImage
            zoomPctX = min(zoomPctX, zoomPctY);
            zoomPctY = zoomPctX;
        end
        
        cx = mean(xLim);
        cy = mean(yLim);
        
        xLim = RecalcZoomAxesLimits(xLim, mDefaultXLim, cx, zoomPctX);
        yLim = RecalcZoomAxesLimits(yLim, mDefaultYLim, cy, zoomPctY);
        
        SetAxesLimits(xLim, yLim);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function MagnifierOn()
        %MagnifierCreate
        
        if ~fIsMagnifierOn    
            if fIsPointerCross
                isPointerCross = true;
                PointerCrossOff();
            else
                isPointerCross = false;
            end
            
            mMgDirection = 'plus';
            
            % create magnifier struct
            mMagnifier = struct(...
                'obj',          copyobj(hAx, hFig), ...
                'frame_obj',    [], ...
                'size',         mMgSize, ...
                'zoom',         mMgZoom);
            
            fIsMagnifierOn = true;
            MagnifierSetup();
            MagnifierUpdate();
            
            if isPointerCross
                PointerCrossOn();
            end
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function MagnifierOff()
        %MagnifierOff
        
        if fIsMagnifierOn
            fIsMagnifierOn = false;
            set(hAx, 'Color', get(mMagnifier.obj, 'Color'));
            
            delete(mMagnifier.obj);
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function MagnifierUpdate()
        %MagnifierUpdate
        
        if fIsMagnifierOn            
            % see magnify Rick Hindman - 7/29/04
            % http://www.mathworks.com/matlabcentral/fileexchange/5961
            
            mgSize = mMagnifier.size;
            mgZoom = mMagnifier.zoom;
            
            figPos = GetObjPos(hFig, 'pixels');
            axPos = GetObjPos(hAx, 'normalized');
            
            [acx, acy] = GetCursorCoordOnAxes();
            [wcx, wcy] = GetCursorCoordOnWindow('pixels');
            
            [xLim, yLim] = GetAxesLimits();
                        
            mgPos = [([wcx, wcy] ./ figPos(3:4)) 0 0] + mgSize * axPos(3) * [-1 -1 2 2];

            mgXLim = acx + (1 / mgZoom) * (mgPos(3) / axPos(3)) * diff(xLim) * [-0.5 0.5];
            mgYLim = acy + (1 / mgZoom) * (mgPos(4) / axPos(4)) * diff(yLim) * [-0.5 0.5];
            
            SetObjPos(mMagnifier.obj, mgPos, 'normalized');
                                    
            set(mMagnifier.obj, ...
                'XLim', mgXLim, ...
                'YLim', mgYLim);
            
            MagnifierBorderUpdate();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function MagnifierSetup()
        %MagnifierSetup

        set(mMagnifier.obj, ...
            'Box', 'on', ...
            'XMinorTick', 'on', ...
            'YMinorTick', 'on');
        
        title(mMagnifier.obj, '');
        xlabel(mMagnifier.obj, ''); 
        ylabel(mMagnifier.obj, '');
        
        if fIsImage
            mMagnifier.frame_obj = ...
                [patch('Parent', mMagnifier.obj), ...
                patch('Parent', mMagnifier.obj)];
            
            set(mMagnifier.frame_obj, 'FaceColor', 'none');
            
            set(mMagnifier.frame_obj(1), ...
                'LineWidth', 1.5, ...
                'EdgeColor', 'w')
            set(mMagnifier.frame_obj(2), ...
                'LineWidth', 1, ...
                'EdgeColor', 'k')
            
            MagnifierBorderUpdate();
        end
        
        hLines = findobj(mMagnifier.obj, 'Type', 'line');
        if ~isempty(hLines)
            if (mMgLinesWidth ~= 1)
                set(hLines, 'LineWidth', mMgLinesWidth);
            end
        end
        
        set(hAx, 'Color', get(hAx, 'Color')*mMgShadow);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function MagnifierBorderUpdate()
        %MagnifierBorderUpdate
        
        if fIsImage
            x = get(mMagnifier.obj, 'XLim');
            y = get(mMagnifier.obj, 'YLim');
            
            set(mMagnifier.frame_obj, ...
                'XData', [x(1) x(2) x(2) x(1)], ...
                'YData', [y(1) y(1) y(2) y(2)]);
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function MagnifierSizeChange(direction)
        %MagnifierSizeChange
        
        if fIsMagnifierOn
            switch direction
                case 'plus'
                    if (mMagnifier.size < mMgMaxSize)
                        mMagnifier.size = mMagnifier.size * mMgSizeStep;
                    end
                case 'minus'
                    if (mMagnifier.size > mMgMinSize)
                        mMagnifier.size = mMagnifier.size / mMgSizeStep;
                    end
            end
            
            MagnifierUpdate();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function MagnifierZoomChange(direction)
        %MagnifierZoomChange
        
        if fIsMagnifierOn
            switch direction
                case 'plus'
                    if (mMagnifier.zoom < mMgMaxZoom)
                        mMagnifier.zoom = mMagnifier.zoom * mMgZoomStep;
                    end
                case 'minus'
                    if (mMagnifier.zoom > mMgMinZoom)
                        mMagnifier.zoom = mMagnifier.zoom / mMgZoomStep;
                    end
            end
            
            MagnifierUpdate();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function MagnifierReset
        %MagnifierReset
        
        if fIsMagnifierOn
            mMagnifier.size = mMgSize;
            mMagnifier.zoom = mMgZoom;
            MagnifierUpdate();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ResetAxesToOrigView()
        %ResetAxesToOrigView reset axes to original limits
        
        SetAxesLimits(mDefaultXLim, mDefaultYLim);
        PointerCrossUpdate();
        
        mZoomIndexX = find(mZoomGrid == 100);
        mZoomIndexY = mZoomIndexX;
    end
%--------------------------------------------------------------------------

%==========================================================================
    function ResetAxesToOrigView3D()
        %ResetAxesToOrigView3D reset axes to original position in 3D mode
        
        % position reset
        axi = GetCurrentAxesIndex();
        pos = mAxesInfo(axi).normposition;
        SetObjPos(hAx, pos, 'normalized');
        
        % view reset
        resetplotview(hAx, 'ApplyStoredView'); % (!!!) undocumented function
        
        if mAxesInfo(axi).isvis3d
            axis(hAx, 'vis3d');
        end
        
        % zoom reset
        mZoom3DIndex = find(mZoomGrid == 100);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function [x, y, z] = GetCursorCoordOnAxes()
        %GetCursorCoordOnAxImg
        
        crd = get(hAx, 'CurrentPoint');
        x = crd(2,1);
        y = crd(2,2);
        z = crd(2,3);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function [x, y] = GetCursorCoordOnWindow(units)
        %GetCursorCoordOnWindow
        
        if (nargin < 1), units = 'pixels'; end
        
        dfltUnits = get(hFig, 'Units');
        set(hFig, 'Units', units);
        
        crd = get(hFig, 'CurrentPoint');
        x = crd(1); 
        y = crd(2);
        
        set(hFig, 'Units', dfltUnits);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function pos = GetObjPos(h, units)
        %GetObjPos get object position
        
        if (nargin < 2), units = get(h, 'Units'); end
        
        dfltUnits = get(h, 'Units');
        set(h, 'Units', units);
        pos = get(h, 'Position');
        set(h, 'Units', dfltUnits);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetObjPos(h, pos, units)
        %SetObjPos set object position
        
        if (nargin < 3), units = get(h, 'Units'); end
        
        dfltUnits = get(h, 'Units');
        set(h, 'Units', units);
        set(h, 'Position', pos);
        set(h, 'Units', dfltUnits);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function [xLim, yLim] = GetAxesLimits()
        %GetAxesLimits
        
        xLim = get(hAx, 'XLim');
        yLim = get(hAx, 'YLim');
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetAxesLimits(xLim, yLim)
        %SetAxesLimits

        set(hAx, 'XLim', xLim);
        set(hAx, 'YLim', yLim);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetPointerCrossKeys()
        %SetPointerCrossKeys set pointer fullcross
        
        if fIsPointerCross
            PointerCrossOff();
        else
            PointerCrossOn();
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetPointer(pointerType)
        %SetPointer set pointer symbol
        
        set(hFig, 'Pointer', pointerType);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetAxesGridKeys()
        %SetAxesGridKeys on/off axes grid
        
        if fIsAxesGrid
            action = 'off';
            fIsAxesGrid = false;
        else
            action = 'on';
            fIsAxesGrid = true;
        end
        
        set(hAx, 'XGrid', action, 'YGrid', action);
        
        if fIsMagnifierOn
            set(mMagnifier.obj, 'XGrid', action, 'YGrid', action);
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetSmoothKeys()
        %SetSmoothKeys on/off cmoothing plots
        
        if fIsSmoothing
            action = 'off';
            fIsSmoothing = false;
        else
            action = 'on';
            fIsSmoothing = true;
        end
        
        if ~fIsImage
            %FIXME: bug with switching opengl/painter renderer here
            %Lost figure focus
            hLine = findobj(hAx, 'Type', 'Line');
            if ~isempty(hLine)
                set(hLine, 'LineSmooth', action);   % !!! Undocumented property
            end
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function [zg, st] = ZoomLogGrid(a, b, n)
        %ZoomLogGrid log zoom grid
        
        zg = unique(round(logspace(a, b, n)));
        
        zg(zg<10) = [];	% begin zoom == 10%
        st = length(zg);
        
        if isempty(find(zg == 100, 1))
            error('dragzoom:badZoomGridOptions', 'Options for zoom grid is bad.')
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function tf = IsZoomMouseAllowed()
        %IsZoomMouseAllowed
        
        [wcx, wcy] = GetCursorCoordOnWindow();
        figPos = get(hFig, 'Position');
        
        if (wcx >= 1 && wcx <= figPos(3) && wcy >= 1 && wcy <= figPos(4))
            tf = true;
        else
            tf = false;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function tf = IsImageOnAxes(hAxes)
        %IsImageOnAxes
        
        if (nargin < 1), hAxes = hAx; end
        
        h = findobj(hAxes, 'Type', 'Image');
        
        if isempty(h)
            tf = false;
        else
            tf = true;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function tf = IsAxes2D(hAxes)
        %IsAxes2D
        
        if (nargin < 1), hAxes = hAx; end
        
        tf = is2D(hAxes); % (!!!) internal undocumented function
    end
%--------------------------------------------------------------------------

%==========================================================================
    function targetInBounds = IsInBoundsAxes(hAxes)
        %InBoundsAxes Check if the user clicked within the bounds of the axes. If not, do nothing
        
        targetInBounds = true;
        tol = 3e-16;
        cp = get(hAxes, 'CurrentPoint');
        
        XLims = get(hAxes, 'XLim');
        if ((cp(1,1) - min(XLims)) < -tol || (cp(1,1) - max(XLims)) > tol) && ...
                ((cp(2,1) - min(XLims)) < -tol || (cp(2,1) - max(XLims)) > tol)
            targetInBounds = false;
        end
        
        YLims = get(hAxes, 'YLim');
        if ((cp(1,2) - min(YLims)) < -tol || (cp(1,2) - max(YLims)) > tol) && ...
                ((cp(2,2) - min(YLims)) < -tol || (cp(2,2) - max(YLims)) > tol)
            targetInBounds = false;
        end
        
        ZLims = get(hAxes, 'ZLim');
        if ((cp(1,3) - min(ZLims)) < -tol || (cp(1,3) - max(ZLims)) > tol) && ...
                ((cp(2,3) - min(ZLims)) < -tol || (cp(2,3) - max(ZLims)) > tol)
            targetInBounds = false;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function tf = IsCurrentAxes(hAxes, isInit)
        %IsCurrentAxes
        
        if (nargin < 2), isInit = false; end
        
        if isInit
            hcAx = hAx;
        else
            hcAx = get(hFig, 'CurrentAxes');
        end
        
        if isequal(hAxes, hcAx)
            tf = true;
        else
            tf = false;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function tf = IsAxesVis3D(hAxes)
        %IsAxesVis3D
        
        visProp = {
            get(hAxes, 'PlotBoxAspectRatioMode')
            get(hAxes, 'DataAspectRatioMode')
            get(hAxes, 'CameraViewAngleMode')
            };
        
        if all(strcmpi(visProp, 'manual'))
            tf = true;
        else
            tf = false;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function AxesInfo = GetAxesInfo()
        %GetAxesInfo
        
        AxesInfo = [];
        hAxes = findobj(hFig, 'Type', 'axes');
        
        if isempty(hAxes), return; end
        
        countAxes = length(hAxes);
        
        AxesInfo = struct(...
            'handle',       cell(1, countAxes), ...
            'iscurrent',    cell(1, countAxes), ...
            'is2d',         cell(1, countAxes), ...
            'isimage',      cell(1, countAxes), ...  
            'isvisible',    cell(1, countAxes), ...  
            'isvis3d',      cell(1, countAxes), ...
            'position',     cell(1, countAxes), ...
            'normposition', cell(1, countAxes), ...
            'xlim',         cell(1, countAxes), ...
            'ylim',         cell(1, countAxes), ...
            'camtarget',    cell(1, countAxes), ...
            'camposition',  cell(1, countAxes));
        
        for i = 1:countAxes
            h = hAxes(i);
            AxesInfo(i).handle = h;
            
            AxesInfo(i).iscurrent = IsCurrentAxes(h, 1);
            AxesInfo(i).is2d = IsAxes2D(h);
            AxesInfo(i).isimage = IsImageOnAxes(h);
            AxesInfo(i).isvisible = strcmpi(get(h, 'Visible'), 'on');
            AxesInfo(i).isvis3d = IsAxesVis3D(h);
            
            AxesInfo(i).position = GetObjPos(h, 'pixels');
            AxesInfo(i).normposition = GetObjPos(h, 'normalized');
            AxesInfo(i).xlim = get(h, 'XLim');
            AxesInfo(i).ylim = get(h, 'YLim');
            AxesInfo(i).camtarget = get(h, 'CameraTarget');
            AxesInfo(i).camposition = get(h, 'CameraPosition');
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SelectAxesUnderCursor()
        %SelectAxesUnderCursor select axes under cursor as current
        
        axi = GetAxesIndexUnderCursor();
        
        if (axi > 0)
            fIsEnableControl = true;
            
            if ~mAxesInfo(axi).iscurrent
                caxi = GetCurrentAxesIndex();
                isCax2d = mAxesInfo(caxi).is2d;
                
                SetCurrentAxes(axi);
                
                % check callbacks
                if (isCax2d ~= mAxesInfo(axi).is2d)
                    % if dimension of axes has changed
                    EnableCallbacks();
                end
            end
        else
            fIsEnableControl = false;
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function SetCurrentAxes(axi)
        %SetCurrentAxes set current axes and work mode
        
        hAx = mAxesInfo(axi).handle;
        
        set(hFig, 'CurrentAxes', hAx);
        for i = 1:numel(mAxesInfo)
            mAxesInfo(i).iscurrent = false;
        end
        mAxesInfo(axi).iscurrent = true;
        
        fIsAxes2D = mAxesInfo(axi).is2d;
        fIsImage = mAxesInfo(axi).isimage;
        
        mDefaultAxPos = mAxesInfo(axi).position;
        mDefaultXLim = mAxesInfo(axi).xlim;
        mDefaultYLim = mAxesInfo(axi).ylim;
        
        % save info to work correctly after saving figures
        set(hFig, 'UserData', mAxesInfo);
    end
%--------------------------------------------------------------------------

%==========================================================================
    function axi = GetCurrentAxesIndex()
        %GetCurrentAxesIndex
        
        axi = [];
        for i = 1:numel(mAxesInfo)
            if mAxesInfo(i).iscurrent
                axi = i;
                return;
            end
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function axi = GetAxesIndexUnderCursor()
        %FindAxesUnderCursor find current axes under cursor
        
        axi = GetCurrentAxesIndex();
        
        if ~fIsSelectedCurrentAxes
            caxi = GetCurrentAxesIndex();
            if ~IsInBoundsAxes(mAxesInfo(caxi).handle)
                axi = 0;
            end
            return; 
        end
        
        for i = 1:numel(mAxesInfo)         
            if IsInBoundsAxes(mAxesInfo(i).handle)
                axi = i;
                return;
            else
                axi = 0; % without axes
            end
        end
    end
%--------------------------------------------------------------------------

%==========================================================================
    function tf = ParseInputs()
        %ParseInputs parse input arguments
        
        tf = true;
        
        if ~ishandle(hObj)
            error('dragzoom:invalidType', 'Input Argument must be handle.')
        end
        
        handleType = get(hObj, 'Type');
        
        switch handleType
            case 'axes'
                fIsSelectedCurrentAxes = false;
                
                hAx = hObj;
                hFig = ancestor(hAx, 'figure');
            case 'figure'                
                hFig = hObj;
                hAx = get(hFig, 'CurrentAxes');
                
                if isempty(hAx)
                    tf = false;
                    warning('dragzoom:notFoundAxes', 'Not found axes objects.')
                    return;
                end
            otherwise
                error('dragzoom:invalidHandle', ...
                    'Input Argument must be figure or axes handle. Input handle "%s".', handleType)
        end
    end
%--------------------------------------------------------------------------

end % DRAGZOOM

