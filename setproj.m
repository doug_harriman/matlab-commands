%SETPROJ     Sets Matlab search path for a specific project.
%
%  Syntax: >> setproj <project_name>
%

% DLH, 
% 11/08/05 - Now using a root object application data field.
% 04/15/04 - Removed path stuff.  Trying to convert Matlab Tools and just
%            label with a revision control system.
% 11/17/99 - Created


function project = setproj(project)

field = 'DLHToolsCurrentProject';

% Make sure we have an app data field
if ~isappdata(0,field),
    setappdata(0,field,'');
end

% Set the project if a name passed in.
if nargin > 0,
    setappdata(0,field,project);
end

% Provied output if requested.
project = getappdata(0,field);

% Auto set if no project is set.
if isempty(project)
    dirs = words(pwd,filesep);
    dirs = lower(dirs);
    [tf,idx] = ismember('projects',dirs);
    
    if tf && (length(dirs) > idx)
        project = dirs{idx+1};
    end
end

if nargout < 1,
    disp(['Current project: ' project]);
    clear project;
end