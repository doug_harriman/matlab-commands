%DICTSTR

classdef dictstr < dict
    methods
        function obj = dictstr
            %DICTSTR  String dictionary constructor.
            %

            % Convert values field to a structure.
            obj.value = struct;

        end % constructor

        function obj = subsasgn(obj,ref,value)
            %DICT/SUBSASGN  Subscript assignment for DICT objects.
            %
            %  See also: DICT
            %

            if ~strcmp(ref.type,'()')
                error('Cell and dot indexing for assignment not supported.');
            end

            % Vectorized calls not supported
            if length(ref.subs) > 1
                error('DICT only supports storing key/value pairs one at a time.');
            end
            key = ref.subs{1};

            % Existing or new key?
            if ~isfield(obj.value,key)
                % Validate the key
                if ~ischar(key)
                   error('Key values must be strings.');
                end
                
                % Non-valid string
                if ~isvarname(key)
                    error('Keys must be valid Matlab variable names');
                end
            end

            obj.value.(key) = value;

        end % subsasgn

        function idx = index(obj,key)
            %DICT/INDEX  Find index of a key in the dictionary key array.
            %
            %  See also: DICT
            %
            
            error('Invalid method for DICTSTR');
            
        end % index

        function [] = disp(obj)
            %DICT/DISP  Display a DICT object.
            %
            %  See also: DICT
            %

            disp('Dictionary:');
            disp('  keys: ');
            disp(fieldnames(obj.value));
            disp('  values: ');
            disp(struct2cell(obj.value));
        end % disp

        function [] = display(obj)
            %DICT/DISPLAY  Display a DICT object.
            %
            %  See also: DICT
            %
            disp(obj);
        end

        function val = lookup(obj,key)
            %DICT/LOOKUP  Return a value given a key.
            %
            %  See also: DICT.
            %

            try
                val = obj.value.(key);
            catch %#ok<CTCH>
                % Report error
                
                % Non-string key
                if ~ischar(key)
                   error('Key values must be strings.');
                end
                
                % Non-valid string
                if ~isvarname(key)
                    error('Keys must be valid Matlab variable names');
                end
                
                % Return empty
                val = [];
            end
            
        end

        function tf = iskey(obj,keys)
            %DICT/ISKEY  Returns bool array denoting if key(s) exist in dictionary.
            %
            %  See also: DICT, DICT/KEYS
            %

            tf = isfield(obj.value,keys);

        end % iskey

        function data = keys(obj)
            %DICT/KEYS  Returns cell array of all keys in a DICT object.
            %
            %  See also: DICT, DICT/VALUES
            %

            data = fieldnames(obj.value);
        end % keys

        function data = values(obj)
            %DICT/VALUES  Returns cell array of all values in a DICT object.
            %
            %  See also: DICT, DICT/KEYS
            %

            data = struct2cell(obj.value);
        end % values

        function obj1 = plus(obj1,obj2)
            %DICT/PLUS  Creates composite dictionary.
            %  DICT1+DICT2 returns a dictionary composed of key value pairs
            %  from both DICT1 and DICT2.  If both dictionaries contain the
            %  same key, the value stored in DIC1 will be used.
            %
            %  See also: DICT, DICT/MINUS, DICT/KEYS, DICT/ISKEY
            %

            % Error check inputs
            error(nargchk(2,2,nargin));
            if ~isa(obj2,'dictstr')
                error('Both inputs must be DICTSTR objects.');
            end

            % Look for unique keys to copy over.
            idx = find(~obj1.iskey(obj2.keys));

            % Copy those values over
            fields = obj2.keys;
            for i = 1:length(idx)
                field = fields{idx(i)};
                obj1.value.(field) = obj2.value.(field);
            end
            
        end % plus

        function obj1 = minus(obj1,obj2)
            %DICT/MINUS  Creates dictionary of unique keys.
            %  DICT1-DICT2 returns a dictionary composed of key value pairs
            %  which have keys that only exist in DICT1.
            %
            %  See also: DICT, DICT/PLUS, DICT/KEYS, DICT/ISKEY
            %

            % Error check inputs
            error(nargchk(2,2,nargin));
            if ~isa(obj2,'dict')
                error('Both inputs must be DICTSTR objects.');
            end

            % Look for common keys to eliminate.
            idx = find(obj1.iskey(obj2.keys));

            % Copy those values over
            fields = obj2.keys;
            for i = 1:length(idx)
                field = fields{idx(i)};
                obj1.value = rmfield(obj1.value,field);
            end

        end % minus

    end % methods - public
end % classdef
