%FFT_T  FFT for an arbitrary data set.
% 
%>>[c,w] = fft_t(time,data)
%  Where:  c - FFT coefficients
%          w - Frequency vector (Hz)
%

function [y,x] = fft_t(t,data) ;

y = fft(data) ;  % do FFT
x = [0:length(t)-1]/max(t);  % Frequencies
ind = round(length(x)/2) ;  % First half of the data set
x = x(1:ind) ;  % First half of frequencies
y = y(1:ind) ;  % coeffs
