%BKGUI  GUI front end for BK.
%
%  See also: bk.
%

% Doug Harriman (doug.harriman@hp.com)
% 16-Feb-07 - Created


function [] = bkgui

% Bottom to top
x = 1;
y = 1;
text_width = 8;
x_spacing  = 2;
pop_width  = 30;
height = 2;

% Figure
fig_han = figure('Visible','Off',...
    'Units','Characters',...
    'Position',[20 20 text_width+pop_width+2*x_spacing+x 3*(height+1)],...
    'NumberTitle','Off',...
    'Name','BK GUI',...
    'Menubar','None',...
    'Resize','Off');


uicontrol('Style','Text',...
    'Units','Characters',...
    'String','Mech',...
    'Position',[x y text_width height]);
data.mech.han = uicontrol('Style','Popup',...
    'Units','Characters',...
    'Callback',@SetMech,...
    'Position',[x+text_width+x_spacing y pop_width height]);

y = y + 2.5;
uicontrol('Style','Text',...
    'Units','Characters',...
    'String','Servo',...
    'Position',[x y text_width height]);
data.servo.han = uicontrol('Style','Popup',...
    'Units','Characters',...
    'Callback',@SetServo,...
    'Position',[x+text_width+x_spacing y pop_width height]);

y = y + 2.5;
uicontrol('Style','Text',...
    'Units','Characters',...
    'String','Repo',...
    'Position',[x y text_width height]);
data.repo.han = uicontrol('Style','Popup',...
    'Units','Characters',...
    'Callback',@SetRepo,...
    'Position',[x+text_width+x_spacing y pop_width height]);

% Fill in the lists
data = UpdateRepo(data);
set(fig_han,'UserData',data,'Visible','On');


function [data] = UpdateRepo(data)
data.repo.list = bk('ListRepo');
data.repo.cur  = bk('GetRepo');
if isempty(data.repo.cur)
    data.repo.cur = data.repo.list{1};
    bk('SetRepo',data.repo.cur);
end

data.repo.idx  = strmatch(data.repo.cur,data.repo.list,'exact');
if isempty(data.repo.idx)
    data.repo.idx = 1;
end    
set(data.repo.han,'String',data.repo.list,'Value',data.repo.idx);
data = UpdateServo(data);

function [data] = UpdateServo(data)
data.servo.list = bk('ListServo');
data.servo.cur  = bk('GetServo');
if isempty(data.servo.cur)
    data.servo.cur = data.servo.list{1};
    bk('SetServo',data.servo.cur);
end

data.servo.idx  = strmatch(data.servo.cur,data.servo.list);
if isempty(data.servo.idx)
    data.servo.idx = 1;
end
set(data.servo.han,'String',data.servo.list,'Value',data.servo.idx);
data = UpdateMech(data);

function [data] = UpdateMech(data)
data.mech.list = bk('Listmech');
data.mech.cur  = bk('Getmech');
if isempty(data.mech.cur)
    data.mech.cur = data.mech.list{1};
    bk('SetMech',data.mech.cur);
end

data.mech.idx  = strmatch(data.mech.cur,data.mech.list);
if isempty(data.mech.idx)
    data.mech.idx = 1;
end
set(data.mech.han,'String',data.mech.list,'Value',data.mech.idx);


function [] = SetRepo(varargin)
fig_han   = get(gcbo,'Parent');
list = get(gcbo,'String');
new_val  = list{get(gcbo,'Value')};
bk('SetRepo',new_val);
data = get(fig_han,'UserData');
data = UpdateRepo(data);
set(fig_han,'UserData',data);

function [] = SetServo(varargin)
fig_han   = get(gcbo,'Parent');
list = get(gcbo,'String');
new_val  = list{get(gcbo,'Value')};
bk('SetServo',new_val);
data = get(fig_han,'UserData');
data = UpdateServo(data);
set(fig_han,'UserData',data);

function [] = SetMech(varargin)
fig_han   = get(gcbo,'Parent');
list = get(gcbo,'String');
new_val  = list{get(gcbo,'Value')};
bk('SetMech',new_val);
data = get(fig_han,'UserData');
data = UpdateMech(data);
set(fig_han,'UserData',data);

