%PLOTLABEL  Adds metadata to plot.
%

% DLH
% 01/20/05 - Extracted from pp.m

function [axHan] = plotlabel(msg)

if nargin == 0
    msg = 'on';
end

switch(msg)
    case 'on'
        % Create axes for metadata
        origAx = gca;
        axHan = axes;
        setappdata(gcf,'plotlabel',axHan);
        set(axHan,...
            'Visible','Off',...
            'Position',[0 0 1 1]);

        % Initials and date
        str = {'Doug Harriman',datestr(date,1)};
        initHan = text(0.91, 0.033,str);
        set(initHan,'FontSize',6);


        % Project Info
        proj = setproj;
        if ~isempty(proj)
            proj = strrep(proj,'_',' ');
            proj_words = words(proj);
            proj = '';
            for i = 1:length(proj_words),
                temp = proj_words{i};
                temp(1) = upper(temp(1));
                proj = [proj temp ' '];
            end
            proj = deblank(proj);
%             proj = [proj ' Project'];
        end

        str = {'Simplexity Product Development',...
            proj};
        projHan = text(0.02, .04, str);
        set(projHan,'FontSize',6);

        % Return to original axes
%         axes(origAx);
        
    case 'off'
        if ~isappdata(gcf,'plotlabel')
            disp('Current figure does not contain a plot label.');
            return;
        end
        
        han = getappdata(gcf,'plotlabel');
        if ~isempty(han)
            delete(han);
        end
    otherwise
        error(['Unsupported message: ' msg]);
end