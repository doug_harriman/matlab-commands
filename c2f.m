%C2F  Centigrade to Fahrenheit conversion.
%

function F = c2f(C)

F = C*(9/5) + 32;
