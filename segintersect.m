%SEGINTERSECT  Line segment intersection detection.
%  XY=SEGINTERSECT(S1,S2) determines the segment intersection point XY of
%  line segments S1 and S2.  S1 and S2 are defined as:
%  S = [x1 y1; x2 y2];
%  XY = [x y];
%  XY is empty if segments are colinear, parallel or do not intesect within
%  the segment bounds.
%
% Algorithm from:
% http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
%

function [xy] = segintersect(s1,s2)

% Assume segments don't cross
xy = [];

% Break segment definitions into pt, delta pt representation.
p = [s1(1,:)  0];
r = [diff(s1) 0];

q = [s2(1,:)  0];
s = [diff(s2) 0];

% Check for parallel
m = cross(r,s);
if m(3) == 0
    return;
end

% Check for collinear
m = cross(q-p,r); 
if m(3) == 0
    return;
end

% Find the ratio along each segment that the intersection would occur.
t = cross((q - p),s) / cross(r,s);
u = m / cross(r,s);

% Make sure intersection lies on both segments.
if (t<0) || (t>1)
    return;
end

if (u<0) || (u>1)
    return;
end

% Calc the intersection point.
xy = p(1:2) + t*r(1:2);
