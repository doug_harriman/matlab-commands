%FILE2STR  Reads a text file and returns a string.
%  STR=FILE2STR(FILENAME) reads the contents of FILENAME and returns them
%  in STR.
%  [STR,FILENAME]=FILE2STR prompts the user to select a file, then returns
%  the name of the selected file.
%

% Doug Harriman
% Senior Systems Engineer
% Simplexity Product Development
% doug.harriman@simplexitypd.com

function [str,filename] = file2str(filename)

% Error checks
str = '';  % Default out.
narginchk(0,1);
if nargin == 0 || isempty(filename)
    [file,path] = uigetfile('*.*','File to read');
    if file == 0
        disp('No file selected');
        return;
    end
    
    % Build file name
    filename = fullfile(path,file);
end
if ~ischar(filename)
    error('File name must be specified with a string value.');
end
if ~exist(filename,'file')
    error(['Unable to find file: ' filename]);
end

% Try to open.
fid = fopen(filename,'r');
if fid < 0
    error(['Unable to open file: ' filename]);
end
c = onCleanup(@()fclose(fid));

% Read in string
str = fread(fid);

% Convert
str = char(str');