% JPRINTF general print utility.
% function h = JPRINTF(dev,fmt,p1,p2,...) is a general replacement
% for the built-in fprintf function.  In addition to printing to a
% file or the command window, JPRINTF can print formatted output to
% one or more text windows.  The text windows resemble the command
% window and can be independantly positioned, editted, and printed.
%
% Parameter dev is a device specifier which controls the destination
% of the output:
%    dev = 1      -- standard output
%    dev = 2      -- standard error
%    dev = fid    -- open file with handle = fid
%    dev = -n     -- text window n (note the minus sign!)
%
% The optional return argument h contains a handle to the Java TextArea
% object.  Use get(h), set(h), inspect(h), or methods(h) to access its
% properties and methods.  To retrive the complete text string, use
% h.getText or get(h,'Text').  Furthermore, getappdata(0) will return a
% structure with fields containing the java TextArea objects for all open 
% windows.
%
% Example:
%   >> jprintf(-1,'pi is approximately = %6.4f\n',pi)
%   >> jprintf(-7,'HELLO WORLD\n')
%   >> jprintf(-1,'Added Text')
% creates two text windows.  The first window, named "Java Box 1", contains
% the string "pi is approximately = 3.1416" on the first line and "Added Text"
% on the second line. The second window, named "Java Box 7", contains the
% string "HELLO WORLD".
%
% Use the pull-down menus to perform these functions:
%     File: open, save, save as, page setup, print, close
%     Edit: cut, copy, paste, select all, clear all
%     Format: font, foreground color, background color, wrap (on/off)
%
% JPRINTF is programmed entirely in Matlab and Java.  It uses the Java
% classes supplied with Matlab R14, including Wildcrest J2PrinterWorks for
% implementation of all hardcopy printer functions.  Usage of Wildcrest is
% unlicensed and for evaluation purposes only.  All printouts will have a
% mandatory header/footer marked "EVALUATION USE ONLY" and an extra nag
% page at the end of the printjob.  The author cannot delete these!
%
% See MYDISPLAY.M for an S-function wrapper for use in Simulink.
% JPRINTF supercedes GPRINTF which will no longer be maintained.
% Help FPRINTF for more information.
%
% Version 1.0
% July 2004
% Mark W. Brown
% mwbrown@ieee.org

% Tested under Matlab Version 7.0 (R14), Java 1.4.2, and Windows 2000 SP2.

function hrtn = jprintf(varargin)

persistent JavaBoxPos

% User has to input something:
if isempty(varargin);
  error('Not enough input arguments.')
  return
end

% Get device (dev) and format (fmt) specifiers
% and create a properly formatted string:
if isnumeric(varargin{1}) % first argument is a device
  if length(varargin) < 2
    error('No format string.');
    return
  end
  dev = varargin{1};
  fmt = varargin{2};
  str = sprintf(varargin{2:end});
else % first argument is a format string
  dev = 1;
  fmt = varargin{1};
  str = sprintf(varargin{:});
end

% If device specifier is positive number,
% then do a regular built-in fprintf:
if dev > 0
  fprintf(dev,str);

% Else if its a negative number, then
% we must create or update a java window:
elseif dev < 0
  
  % Get the name of this java window:
  dev = abs(dev);
  namestr = ['JavaBox',num2str(dev)];
  
  % See if the java window already exists:
  text = getappdata(0,namestr);
  if isempty(text)
  
    % The window doesn't exist, so we need to create it:
    import java.awt.*
		import javax.swing.*
    import com.wildcrest.j2printerworks.*
		
		% Create frame object:
		frame = javax.swing.JFrame(['Java Box ',num2str(dev)]);
		frame.setSize(400,300)

    % Stagger position so frames don't overlap.  Restart
    % if the new frame will be off the screen.
    ScreenDim = get(0,'ScreenSize');
    if isempty(JavaBoxPos)
      JavaBoxPos = [0 0];
    else
      JavaBoxPos = JavaBoxPos + [30 30];
      if any(JavaBoxPos+[400 300] > ScreenDim(3:4))
        JavaBoxPos = [0 0];
      end
    end
    frame.setLocation(JavaBoxPos(1),JavaBoxPos(2));
    
		% Create text object:
		text = javax.swing.JTextArea;
    
    % Create printer:
    if version('-release') > 13
      printer = J2Printer;
      textPrinter = J2TextPrinter(text);
    end

		% Create scroller object with text:
		scroller = javax.swing.JScrollPane(text);
		
		% Create menu bar:
		mymenu = javax.swing.JMenuBar;
		
		% Create FILE menu:
		menu1 = javax.swing.JMenu('File');
		mymenu.add(menu1);
		
		% Add items under FILE menu:
		menuitem1a = javax.swing.JMenuItem('Open...');
		set(menuitem1a,'ActionPerformedCallback',{@DoOpenFile, menuitem1a})
    set(menuitem1a,'UserData',text);
		menu1.add(menuitem1a);
	
		menuitem1b = javax.swing.JMenuItem('Save');
		set(menuitem1b,'ActionPerformedCallback',{@DoSaveFile, menuitem1b})
    set(menuitem1b,'UserData',text);
		menu1.add(menuitem1b);
		
		menuitem1c = javax.swing.JMenuItem('Save As...');
		set(menuitem1c,'ActionPerformedCallback',{@DoSaveFileAs, menuitem1c})
    set(menuitem1c,'UserData',text);
		menu1.add(menuitem1c);
		
		if version('-release') > 13
      menu1.addSeparator;
      menuitem1e = javax.swing.JMenuItem('Page Setup...');
      set(menuitem1e,'ActionPerformedCallback',{@DoPageSetup, menuitem1e, printer})
      menu1.add(menuitem1e);

      menuitem1g = javax.swing.JMenuItem('Print...');
      set(menuitem1g,'ActionPerformedCallback',{@DoPrint, menuitem1g, printer, textPrinter})
      menu1.add(menuitem1g);
    end
    
    menu1.addSeparator;
		menuitem1d = javax.swing.JMenuItem('Close');
		set(menuitem1d,'ActionPerformedCallback',{@DoClose, menuitem1d, frame})
		menu1.add(menuitem1d);
		
		% Create EDIT menu:
		menu2 = javax.swing.JMenu('Edit');
		mymenu.add(menu2);
		
		% Add items under EDIT menu:
		menuitem2a = javax.swing.JMenuItem('Cut');
		set(menuitem2a,'ActionPerformedCallback',{@DoCut, menuitem2a})
    set(menuitem2a,'UserData',text);
		menu2.add(menuitem2a);
		
		menuitem2b = javax.swing.JMenuItem('Copy');
		set(menuitem2b,'ActionPerformedCallback',{@DoCopy, menuitem2b})
    set(menuitem2b,'UserData',text);
		menu2.add(menuitem2b);
		
		menuitem2c = javax.swing.JMenuItem('Paste');
		set(menuitem2c,'ActionPerformedCallback',{@DoPaste, menuitem2c})
    set(menuitem2c,'UserData',text);
		menu2.add(menuitem2c);
    
    menu2.addSeparator;
    menuitem2e = javax.swing.JMenuItem('Select All');
		set(menuitem2e,'ActionPerformedCallback',{@DoSelectAll, menuitem2e})
    set(menuitem2e,'UserData',text);
		menu2.add(menuitem2e);
		
		menu2.addSeparator;
		menuitem2d = javax.swing.JMenuItem('Clear All');
		set(menuitem2d,'ActionPerformedCallback',{@DoClearAll, menuitem2d})
    set(menuitem2d,'UserData',text);
		menu2.add(menuitem2d);
		
		% Create FORMAT menu:
		menu3 = javax.swing.JMenu('Format');
		mymenu.add(menu3);
		
		% Add items under FORMAT menu:
		menuitem3a = javax.swing.JMenuItem('Font...');
		set(menuitem3a,'ActionPerformedCallback',{@DoFont, menuitem3a})
    set(menuitem3a,'UserData',text);
		menu3.add(menuitem3a);

		menu3.addSeparator;
		menuitem3b = javax.swing.JMenuItem('Foreground Color...');
		set(menuitem3b,'ActionPerformedCallback',{@DoForeColor, menuitem3b})
    set(menuitem3b,'UserData',text);
		menu3.add(menuitem3b);
		
		menuitem3c = javax.swing.JMenuItem('Background Color...');
		set(menuitem3c,'ActionPerformedCallback',{@DoBackColor, menuitem3c})
    set(menuitem3c,'UserData',text);
		menu3.add(menuitem3c);
		
		menu3.addSeparator;
		menuitem3g = javax.swing.JCheckBoxMenuItem('Word Wrap');
		set(menuitem3g,'ItemStateChangedCallback',{@DoWordWrap, menuitem3g});
    set(menuitem3g,'UserData',text);
		menu3.add(menuitem3g);
		
	  % Create HELP menu:
		menu4 = javax.swing.JMenu('Help');
		mymenu.add(menu4);
		
		% Add items under FORMAT menu:
		menuitem4a = javax.swing.JMenuItem('JPRINTF Help');
		set(menuitem4a,'ActionPerformedCallback',{@DoHelpUsing, menuitem4a})
		menu4.add(menuitem4a);
    
		menu4.addSeparator;
		menuitem4b = javax.swing.JMenuItem('About JPRINTF');
		set(menuitem4b,'ActionPerformedCallback',{@DoHelpAbout, menuitem4b})
		menu4.add(menuitem4b);

    % Add the widgets to the frame and make visible:
		frame.getContentPane.add(BorderLayout.CENTER,scroller);
		frame.getContentPane.add(BorderLayout.NORTH,mymenu);
		frame.show
    
    % Add the input text to the java TextArea:
    text.append(str);
    
    % Enable some handy features:
    if version('-release') > 13
      set(text,'dragEnabled','on');
    end
    
    % Store the text object in appdata so we can find it later.
    % Also define a callback to remove appdata when the box is closed.
    set(text,'name',namestr);
    set(text,'AncestorRemovedCallback','rmappdata(0,get(gcbo,''name''))');
    setappdata(0,namestr,text);

  else
    
    % Add the input text to the java TextArea:
    text.append(str);

  end
end

% Return an optional java TextArea object.
if nargout
  hrtn = text;
end

return

% Read a text file into the window.  The filename
% is NOT remembered, so if you try to save later,
% you will have to supply a filename.
function DoOpenFile(a,b,obj)
  [fname, fpath] = uigetfile('*.*');
  if fpath == 0; return; end
  filespec = fullfile(fpath,fname);
  fid = fopen(filespec,'r');
  ftext = fscanf(fid,'%c');
  fclose(fid);
  htext = get(obj,'UserData');
  set(htext,'Text',ftext);
return

% Write the text to a file.  If a previous
% SAVEAS function was executed, the same
% filename will be used.
function DoSaveFile(a,b,obj)
  htext = get(obj,'UserData');
  fspec = get(htext,'UserData');
  if isempty(fspec);
    DoSaveFileAs(a,b,obj);
  else
    ttext = get(htext,'Text');
    fid = fopen(fspec,'w');
    fprintf(fid,'%s',ttext);
    fclose(fid);
  end
return

% Write the text to a file.  The supplied filename
% is remembered for all subsequent SAVE operations.
function DoSaveFileAs(a,b,obj)
  htext = get(obj,'UserData');
  ttext = get(htext,'Text');
  [fname, fpath] = uiputfile('*.txt','Saving text to...');
  if isstr(fname)
    fspec = fullfile(fpath,fname);
    set(htext,'UserData',fspec);
    fid = fopen(fspec,'w');
    fprintf(fid,'%s',ttext);
    fclose(fid);
  end
return

% Set up page for printing:
function DoPageSetup(a,b,obj,printer)
  printer.showPageSetupDialog;
return

% Print hardcopy to a printer:
function DoPrint(a,b,obj,printer,textPrinter)
  printer.print(textPrinter);
return

% Close the window:
function DoClose(a,b,obj,frame)
  frame.dispose;
return

% Cut selected text.
function DoCut(a,b,obj)
  htext = get(obj,'UserData');
  htext.cut;
  htext.updateUI
return

% Copy selected text to a buffer:
function DoCopy(a,b,obj)
  htext = get(obj,'UserData');
  htext.copy;
return

% Paste the buffered text at cursor location:
function DoPaste(a,b,obj)
  htext = get(obj,'UserData');
  htext.paste;
return

% Select all the text in the window:
function DoSelectAll(a,b,obj)
  htext = get(obj,'UserData');
  htext.selectAll;
return

% Clear all text from the window:
function DoClearAll(a,b,obj)
  htext = get(obj,'UserData');
  set(htext,'Text','');
return

% Change the font of the displayed text.  Note
% that Java cannot do simultaneous Bold and Italic.
% If you select both, you will only get Bold.
function DoFont(a,b,obj)
  htext = get(obj,'UserData');
  hf = get(htext,'Font');
  cf.FontName = get(hf,'Name');  %cf is a struct of current font
  cf.FontUnits = 'points';
  cf.FontSize = get(hf,'Size');
  if strcmp(get(hf,'Bold'),'on')
    cf.FontWeight = 'bold';
  else
    cf.FontWeight = 'normal';
  end
  if strcmp(get(hf,'Italic'),'on')
    cf.FontAngle = 'italic';
  else
    cf.FontAngle = 'normal';
  end
  mf = uisetfont(cf);            %mf is a struct of modified font
  if isstruct(mf)
    style = java.awt.Font.PLAIN;
    if strcmp(mf.FontWeight,'bold')
      style = java.awt.Font.BOLD;
    elseif strcmp(mf.FontAngle,'italic')
      style = java.awt.Font.ITALIC;
    end
    f = java.awt.Font(mf.FontName, style, mf.FontSize);
    htext.setFont(f);
  end
return

% Change the font color:
function DoForeColor(a,b,obj)
  htext = get(obj,'UserData');
  oldrgb = get(htext,'Foreground');
  newrgb = uisetcolor(oldrgb);
  set(htext,'Foreground',newrgb);
return

% Change the background color:
function DoBackColor(a,b,obj)
  htext = get(obj,'UserData');
  oldrgb = get(htext,'Background');
  newrgb = uisetcolor(oldrgb);
  set(htext,'Background',newrgb);
return

% Toggle word wrap:
function DoWordWrap(a,b,obj)
  htext = get(obj,'UserData');
  state = get(obj,'State');
  if strcmp(state,'off')
    set(htext,'LineWrap','off');
  else
    set(htext,'LineWrap','on');
    set(htext,'WrapStyleWord','on');
  end
return

% Display help:
function DoHelpUsing(a,b,obj)
  helpwin jprintf;
return

% Display about:
function DoHelpAbout(a,b,obj)
  helpdlg({'Version 1.0','July 2004','Mark W. Brown','mwbrown@ieee.org'},'About JPRINTF');
return