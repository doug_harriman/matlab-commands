%PUB  Publish specified M-file via PDFLatex.
%  PUB(FILENAME) Publishes to PDF the given FILENAME.
%

function pub(filename)

% Requires PDFLatex to be set up correctly.  
% This is difficult to do on non-unix machines.
% Currently enforce Unix, but this should be removed if
% you know what you're doing with Cygwin or Mac OSX.
if ~isunix
    error('Only supported for UNIX systems.');
end

% Query for filename if needed.
if nargin < 1
    [fn,pn] = uigetfile('*.m','M-File to Publish');
    if fn == 0
        warning('No file selected.'); %#ok<WNTAG>
    end
    filename = [pn fn];
    [pn,fn] = fileparts(filename);
    filename = [pn filesep fn];
end

% Verify file
if ~ischar(filename)
    error('File name must be a string.');
end
if ~exist(filename,'file')
    error(['Cannot find file: ' filename]);
end

% Setup publish options
opt.format = 'latex';

pn = fileparts(which(mfilename));
opt.stylesheet = [pn filesep 'mxdom2latex.xsl'];

pn = fileparts(which(filename));
if isempty(pn)
    pn = '.';
end
opt.outputDir = [pn filesep 'html'];
if ~exist(opt.outputDir,'dir')
    mkdir(opt.outputDir);
end

opt.imageFormat = 'pdf';
opt.showCode    = false;
opt.evalCode    = true;
opt.catchError  = false;  % Stop publish on error.

% Call the publish command
texfile = publish(filename,opt);

% Sub in the document id.
docid(filename,texfile);

% Store current directory & move to publish dir.
curdir = pwd;
cd(opt.outputDir);

% Call pdflatex
cmd = ['pdflatex ' texfile];
[status,result] = system(cmd);
if status ~= 0
    disp(result);
    error('Call to PDFLatex failed.');
end

% Second run to get all references correct.
[status,result] = system(cmd);

% Move back to our original dir.
cd(curdir);