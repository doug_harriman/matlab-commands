function gsclean

filename = 'c:\tmp\timecard.htm';
instr = file2str(filename);

% Output data
CR = char(10);
outstr = ['Date, Task, Duration, Notes' CR];
data = dict;
task_info = [];
weektime = 0;

% Generate list of row data
res1 = regexp(instr,'<tr.*?>[\w\s]*?(?<data>.*?)</tr>','names');
res1(end+1).data = '<b>END</b>';  % Add a fake date string to flush last data point.

% Process list of rows
for i = 1:length(res1)
   % Extract substring
   str = res1(i).data;
   
   % Look for valid cell data.
   res2 = regexp(str,'<b>(?<date>.*?)</b>','names');
   if ~isempty(res2)
      % Have a valid date, this means we have a new entry.
      % If we have an existing data set, write it.
      if ~isempty(data)
         % Get task list
         tasks = sort(data.keys);
         
         % For date total time.
         t = 0;
         
         for j = 1:length(tasks)
            % Grab the task info for the day.
            taskname = tasks{j};
            task = data(taskname);
            
            % Round task time.
            t_task = minutes(task.duration);
            t_task = roundup(t_task,5);
            t_task = minutes(t_task);
            
            % Total time
            t = t + t_task;
            
            outstr = [outstr taskdate ', ' taskname ', ' ... 
               datestr(t_task,'HH:MM') ',"' ...
               task.notes '"' CR]; %#ok<AGROW>
         end

         % Store out date total time
         weektime = weektime + t;
         outstr = [outstr taskdate ', TOTAL, ' ... 
            datestr(t,'HH:MM') CR]; %#ok<AGROW>
         
      end
      
      % Write the new date string & reset data.
      taskdate = res2.date;
      taskdate = strrep(taskdate,',','');
      data = dict;
      task_info = [];
   end
   
   % Look for task & time
   res3 = regexp(str,'<td nowrap>(?<task>.*?)</td>','names');
   if ~isempty(res3)
      
      % Have a new task name.  Write out old if we have it.
      % Create key if needed.
      if ~isempty(task_info)
         if ~data.iskey(taskname)
            data(taskname) = task_info;
         else
            % Otherwise, extract & update.
            prev = data(taskname);
            
            % Append notes
            prev.notes = [prev.notes ' ' task_info.notes];
            
            % Sum time.
            prev.duration = prev.duration + task_info.duration;
            
            % Store back out.
            data(taskname) = prev;
         end
      end
      
      % Grab task name.
      taskname = res3.task;
      task_info = [];
   end
   
   % Grab task duration.
   res4 = regexp(str,'>(?<hr>\d+):(?<min>\d+):(?<sec>\d+)</td>','names');
   if ~isempty(res4)
      task_info.duration = hours(str2double(res4.hr)) ...
         + minutes(str2double(res4.min)) ...
         + seconds(str2double(res4.sec));
      task_info.notes    = ''; % Null out since not all tasks have notes.
   end
   
   % Grab task notes & store data
   res5 = regexp(str,'<td.*?"notes">(?<notes>.*?)</td>','names');
   if ~isempty(res5)
      task_info.notes = res5.notes;
   end
end % res1 loop

% Week total
% outstr = [outstr CR 'WEEK TOTAL, ' ...
%    datestr(dateround(weektime,'minute'),'DD:HH:MM') CR];

% Write out data
disp(outstr)
str2file(outstr,'c:\tmp\timecard-clean.csv');

