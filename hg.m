%HG  Mercurial command access from Matlab.
%  HG passes Mercurical commands to the underlying operating system.
%

function [status,res] = hg(varargin)

% If multiple input args, concat all
cmd = varargin{1};

if strcmp(cmd,'ci')
    error('Can not do checkin through Matlab.  Use Cygwin instead.');
end

arg = '';
if nargin > 1
    fcn = @(x)[' ' x];
    arg = cellfun(fcn,varargin,'UniformOutput',false);
    arg = strcat(arg{2:end});
end

% Error check commands.


% Tack on the 'hg' command and pass on.
if ispc
    cmd = ['python.exe C:\cygwin\usr\local\bin\hg ' cmd ' ' arg];
elseif isunix
    cmd = ['hg ' cmd ' ' arg];
else
    error('Unsupported machine type');
end
[status,res] = system(cmd);

% Clean display if no output.
if nargout == 0
    disp(res);
    clear status res
end

