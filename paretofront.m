%PARETOFRONT  Highlights generates the pareto front for an XY data set.
%  [XF,YF,IDX]=PARETOFRONT(X,Y,FRONT) returns the Pareto front X and Y
%  points, as wells as the index of the points in the original array.
%
%  IDX=PARETOFRONT(X,Y,FRONT) just returns the index of the points.
%
%  PARETOFRONT(X,Y,FRONT) highlights the pareto front points with red
%  circles on the current plot.
%
%  PARETOFRONT(FRONT) selects XY data from the first line in the current axes.
%
%  Front must be which corner the front should lie on: NE, NW, SE or SW.
%  Default value is SE.
%

% Doug Harriman (doug.harriman@hp.com)
% 07-Jul-08 - Initial release.

function [front_x,front_y,front_idx] = paretofront(x,y,quad)

% Error check inputs.
switch nargin
    case 0
        help(mfilename);
        error('Must specify front.');
    case 1
        quad = x;
        x = [];
    case 2
        % Default quadrant.
        quad = 'sw';
    case 3
        % Do nothing
    otherwise
        error('Expect either 1 input or 3 inputs.');
end

% Extract data if needed
if isempty(x)
    kids = get(gca,'Children');
    kids = findobj(kids,'type','line');
    if isempty(kids)
        error('No data on current plot.');
    end
    kids = kids(end);
    
    % Get data from current plot
    x = get(kids,'xdata');
    y = get(kids,'ydata');
    
    if isempty(x) || isempty(y)
        error('Invalid XY data on current axes.');
    end
end

% Check specified quadrant
quadrants = {'ne','nw','se','sw'};
quad = lower(quad);
if ~ismember(quad,quadrants)
    error('Must specify quadrant for front.');
end

% Check each point for meeting the two criteria.
front_x   = [];
front_y   = [];
front_idx = [];
for i = 1:length(x)
    % Y checks
    if quad(1) == 'n'
        y_ind = y(i) < y;
    else
        y_ind = y(i) > y;
    end
    
    % X checks
    if quad(2) == 'e'
        x_ind = x(i) < x;
    else
        x_ind = x(i) > x;
    end
    
    idx = x_ind & y_ind;
    
    if ~any(idx)
        front_idx(end+1) = i;
        front_x(end+1) = x(i);
        front_y(end+1) = y(i);
    end
end

% Handle outputs
switch nargout
    case 0
        hold('on');
        plot(front_x,front_y,'ro');
        
        clear front_x front_y front_idx
        
    case 1
        front_x = front_idx;
        clear front_y front_idx
end