function t=scalarstruct(s,flag)
%SCALARSTRUCT Array of Structures to Scalar Structure.
% T = SCALARSTRUCT(S) converts the array of structures S into a scalar
% structure having the same field names, with the content of each field
% being a cell array of the same dimension as S.
%
% For example, the array of structures:
%
%                      S(1).a = ones(2),  S(1).b = 'abcd'
%                      S(2).a = 1:5,      S(2).b = 'matlab'
%
% returns the scalar structure
%
%                      T.a = {ones(2) 1:5}           (data stored in cells)
%                      T.b = {'abcd' 'matlab'}
% so that
%                      T.a{k} = S(k).a
%                      T.b{k} = S(k).b
% and
%                      size(T.a) = size(S)
%                      size(T.b) = size(S)
%
% T = SCALARSTRUCT(S,Flag) when Flag = True alternatively stores scalar
% numeric and logical values in a standard array if all elements for a
% given field are scalars. For example,
%
%         S(1).a = ones(2),  S(1).b = 'abcd',    S(1).c = -16
%         S(2).a = 5,        S(2).b = 'matlab',  S(2).c = 1.24e-9
%
% returns the scalar structure
%
%                      T.a = {ones(2) 5}             (data stored in cells)
%                      T.b = {'abcd' 'matlab'}       (data stored in cells)
%                      T.c = [-16  1.24e-9]   (scalar data stored in array)
%
% Note:
% MATLAB processes scalar structures much faster than arrays of structures.
%
% See also STRUCT, CELL, STRUCT2CELL, CELL2STRUCT, CELL2MAT.

% D.C. Hanselman, University of Maine, Orono, ME 04469
% MasteringMatlab@yahoo.com
% Mastering MATLAB 7
% 2006-01-15

if nargin==1
   flag=false;
else
   flag=logical(flag(1));
end
if ~isstruct(s)
   error('Input Must be A Structure.')
end
if numel(s)==1 % already scalar, no work to do
   t=s;
   return
end

snames=fieldnames(s);
tmp=cell(size(s)); % create temporary cell array for output fields

for k=1:length(snames)        % create output one field at a time
   
%   [tmp{:}]=deal(s(:).(snames{k})); % R13 (MATLAB 6.5)
   [tmp{:}]=s(:).(snames{k});       % R14 (MATLAB 7.0) and later
   t.(snames{k})=tmp;         % place cells in field
   
   if flag                    % place data in standard array if possible
      % use these before (MATLAB 7.1) R14 SP3
%       numc=~(cellfun('isclass',tmp,'cell')   | ...
%              cellfun('isclass',tmp,'struct') | ...
%              cellfun('isclass',tmp,'char')   | ...
%              cellfun('isclass',tmp,'function_handle')); % likely numeric
%       logc=cellfun('isclass',tmp,'logical');
%       scac=cellfun('prodofsize',tmp)==1;
      
      % use these for (MATLAB 7.1) R14 SP3 and beyond
      numc=cellfun(@isnumeric,tmp); % these work in MATLAB 7.1 (R14 SP3)
      logc=cellfun(@islogical,tmp);
      scac=cellfun(@numel,tmp)==1;
      
      if all(scac(:)) && (all(numc(:)) || all(logc(:)))% build normal array
         t.(snames{k})=reshape(cat(1,tmp{:}),size(tmp));               
      end
   end
end