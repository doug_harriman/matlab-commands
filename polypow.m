%POLYPOW  Raises a polynomial to a power.
%
% >>new_poly = polypow(old_poly,power)
%

function out = polypow(in,pow)

out = in;
for i = 1:pow-1,
   out = conv(out,in);
end
