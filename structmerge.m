%STRUCTMERGE  Merge structures.  
%  S=STRUCTMERGE(S1,S2) is a superset of structures S1 and S2.  If a field
%  exists in both S1 and S2, the value store in S1 will be used.
%
%  See also: commonfields.
%

% Doug Harriman (doug.harriman@hp.com)

function S = structmerge(S1,S2)

% Error checks
narginchk(2,2);

if ~isstruct(S1) || ~isstruct(S2)
    error('Both inputs must be scalar structures.');
end

f1=fieldnames(S1);
f2=fieldnames(S2);
f_copy=setdiff(f2,f1);
idx=ismember(f2,f_copy);
c1=struct2cell(S1);
c2=struct2cell(S2);
c={c1{:} c2{idx}}';
f={f1{:} f2{idx}}';
S=cell2struct(c,f);