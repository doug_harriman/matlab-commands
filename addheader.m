%ADDHEADER  Adds header comments to existing M files.
%  ADDHEADER(STRING,DIR) adds the STRING to the header of all M files in or
%  below directory DIR.  STRING data can be added after the function
%  declaration or the first block of comments.
%

% DLH (doug.harriman@hp.com)
% 10-DEC-06 - Created

function [] = addheader(str,base_dir_name)

% Error check inputs
if nargin ~= 2
    error('Two inputs expected.');
end
if ~ischar(str)
    error('Fisrt input must be of class STRING');
end

% Build list of all directory names
dir_name_list  = subdirs(base_dir_name);
file_name_list = filelist('*.m',dir_name_list);

% Open each file, inserting text.
CR = char(10);
for i = 1:length(file_name_list)
    % Read in full file
    file_name = file_name_list{i};
    fid = fopen(file_name,'r');
    if fid < 0
        error(['Unable to open file for reading: ' file_name]);
    end
    file_str = fread(fid,inf);
    fclose(fid);
    file_str = char(file_str');
    
    % Look for function signature
    idx_function = strfind(file_str,'function');
    if ~isempty(idx_function)
        idx_function = idx_function(1);
    end
    
    idx_cr       = find(double(file_str)==10);
    
    % Three cases to handle:
    % (1) "function" does not exist in file, so just add stuff to the
    %     beginning of the file.
    % (1) "function" is first line of file, new text needs to be prepended.
    % (2) "function" is somewhere down the file, so new text needs to be
    %     inserted.
    if isempty(idx_function) % No function signature
        file_str = [str CR CR file_str]; %#ok<AGROW>

    elseif all(idx_function<idx_cr)
        file_str = [str CR CR file_str]; %#ok<AGROW>

    else
       file_str = [file_str(1:idx_function-1) CR str CR CR file_str(idx_function:end)];
       
    end
        
    % Overwrite the file with the new data.
    fid = fopen(file_name,'w');
    if fid < 0
        error(['Unable to open file for writing: ' file_name]);
    end
    fprintf(fid,'%s',file_str);
    fclose(fid);
    
end % file loop