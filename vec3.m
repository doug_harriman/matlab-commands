%VEC3  3D vector class.
%  V=VEC3(X,Y,Z,TIME)
%

classdef vec3 < handle
    properties
        x
        y
        z
        
        data

        time
        
        name
        
        axeslabel
    end % properties - public
    
    methods
        function obj = vec3(x,y,z,time)
            %VEC3  3D vector constructor.
            %
            
            % Error checks
            narginchk(1,4);
            
            % Handle different input cases.
            switch nargin
               case 1
                  z = x(:,3);
                  y = x(:,2);
                  x = x(:,1);
                  time = [];
                  
               case 2
                  time = y;
                  z = x(:,3);
                  y = x(:,2);
                  x = x(:,1);
                  
               case 3
                  time = [];
            end            
            
            if( (length(x) ~= length(y)) || (length(x) ~= length(z)) )
                error('Component vectors must be same length.');
            end
            
            % Grab time vector from channels
            if isa(x,'channel')
                time = get(x,'time');
            end
            
            % Make sure doubles.
            x = double(x);
            y = double(y);
            z = double(z);
            time = double(time);
            
            % Force column vectors
            x = reshape(x,numel(x),1);
            y = reshape(y,numel(y),1);
            z = reshape(z,numel(z),1);
            if ~isempty(time)
                time = reshape(time,numel(time),1);
            else
                time = [0:length(x)-1]';
            end
            
            % Package it.
            obj.x = x;
            obj.y = y;
            obj.z = z;
            obj.time = time;
            obj.data = [x y z];
            obj.axeslabel = 'xyz';
        end
        
        function val = get.x(obj)
            val = obj.data(:,1);
        end % get.x

        function val = get.y(obj)
            val = obj.data(:,2);
        end % get.y
        
        function val = get.z(obj)
            val = obj.data(:,3);
        end % get.z
        
        function len = length(obj)
            %LENGTH  Length of vector.
            %
            
            len = size(obj.data,1);
            
        end

        function val = norm(obj)
            %NORM  Euclidian norm of each 3D component of the 3D vector.
            %
            
            n = length(obj);
            val = zeros(n,1);
            for i = 1:n
                val(i) = norm(obj.data(i,:),2);
            end
            
        end
        
        function h = plot3(obj)
            %PLOT3
            
            h = plot3(obj.x,obj.y,obj.z);
            title(obj.name);
            xlabel(obj.axeslabel{1});
            ylabel(obj.axeslabel{2});
            zlabel(obj.axeslabel{3});
            
        end % plot3
        
        function set.axeslabel(obj,val)
            %SET.AXESLABEL
            
            narginchk(2,2);
            if ischar(val)
                if length(val) ~= 3 
                    error('Must be a 3 character array.');
                end
                
                obj.axeslabel = {val(1),val(2),val(3)};
                return;
            end
                
            if iscell(val)
                if length(val) ~= 3
                    error('Cell array must have 3 elements.');
                end
                
                obj.axeslabel = val;
                return;
            end
            
            error(['Unsupported data type for AXESLABEL property: ' class(val)]);
            
        end % set.axeslabel
        
        function h = plot(obj)
            %PLOT  Plots X, Y & Z data in subplots on same figure.
            %
            
            figure;
            subplot(3,1,1);
            h(1) = plot(obj.time,obj.x);
            ylabel(obj.axeslabel{1});
            pt;
            title(obj.name);
            set(gca,'XTickLabel',[]);
            
            subplot(3,1,2);
            h(2) = plot(obj.time,obj.y);
            ylabel(obj.axeslabel{2});
            set(gca,'XTickLabel',[]);
            pt;
            
            subplot(3,1,3);
            h(3) = plot(obj.time,obj.z);
            ylabel(obj.axeslabel{3});
            pt;
            
        end
        
        function new = mtimes(obj,val)
            
            new = obj.copy;
            new.data = obj.data .* val;
            
        end % times
        
        function new = copy(obj)
           %COPY  Copy constructor.
           
           new = vec3(0,0,0);
           new.name = obj.name;
           new.data = obj.data;
           new.time = obj.time;
           new.axeslabel = obj.axeslabel;

        end
        
        function animate(obj,delay)
            %ANIMATE  Animates a vector showing 3D data.
            %
            
            % Error check inputs.
            narginchk(1,2);
            if nargin < 2
                delay = 0;
            end
            
            figure;
            x = obj.x;
            y = obj.y;
            z = obj.z;
            prepaxes;
%             set(gca,'XLim',[-1 1]*max(abs(x))*1.1,...
%                 'YLim',[-1 1]*max(abs(y))*1.1,...
%                 'ZLim',[-1 1]*max(abs(z))*1.1,...
%                 'XLimMode','manual',...
%                 'YLimMode','manual',...
%                 'ZLimMode','manual',...
%                 'DataAspectRatio',[1 1 1]);
            view(3);
            
            n = obj.length;
            h = plot3([0 obj.x(1)],[0 obj.y(1)],[0 obj.z(1)],'.-');
            for i = 2:n
                set(h,'XData',[0 x(i)],...
                    'YData',[0 y(i)],...
                    'ZData',[0 z(i)]);
                drawnow;               
                
                pause(delay);
            end
        end
        
        function chanman(obj)
            %CHANMAN  Sends data to Channel Manager
            %
            
            % Default name.
            if isempty(obj.name) 
                name = 'vector'; %#ok<*PROP>
            else
                name = obj.name;
            end
            
            % Create channels.
            time = obj.time;
            if isempty(time)
                time = 1;
            end
            for i = 1:3
                chname = [name ' - ' char(119+i)];
                ch = channel(chname,obj.data(:,i),time);
                chanman('AddChannel',ch);
            end
            
        end % chanman
            
        function int = cumsum(obj)
            %CUMSUM  Cumulative sum of object signals.
            %
            
            % Get integration time step.
            if isempty(obj.time) || (length(obj.time) > 1)
                error('Time step must be fixed.');
            end
            
            int = vec3(cumsum(obj.x)*obj.time,...
                cumsum(obj.y)*obj.time,...
                cumsum(obj.z)*obj.time,...
                obj.time);
            
            if isempty(obj.name)
                return;
            end
            
            int.name = ['Cumulative sum of ' obj.name];
            
        end % cumsum
        
    end % methods - public
    
end % classdef
