%S2  Sets the current axes to subplot 2 of 2.
function [h] = s2(han)

if nargin < 1
   h=subplot(2,1,2);
   return;
end

% Verify handle type
if ~strcmp(get(han,'Type'),'axes') 
   error('Axes handle expected.');
end

% Create a hidden figure, create the axes to get the size, then use that size in
% the handle of interest.
fig = figure('Visible','off');
h = s2;
pos = get(h,'Position');
close(fig);
u = get(han,'units');
set(han,'units','Normalized');
set(han,'Position',pos);
set(han,'units',u);
h = han;
