%AUTOPLACE  Automatic placement of figure windows.
%    AUTOPLACE manages positioning of windows so that they can be placed in
%    predefined positions allowing multiple windows to be opened in
%    non-overlapping positions.
%
%    AUTOPLACE manages only windows that are opened to the default figure
%    position, and windows with handle visibility set to 'on'.  This keeps
%    interactions with GUI's to a minimum.
%
%    AUTOPLACE manages windows in sets of number of open windows.  Thus the
%    position for figure 1 may be different depending on if there are one,
%    two or three windows open.
%
%    AUTOPLACE is screen size aware, so that figure position sets may be
%    different for different screen sizes.  This enhances the usability of
%    AUTOPLACE when used in a mobile environment where screen size is
%    different when the computer is docked or undocked.
%
%    To use AUTOPLACE, open multiple windows and position them as you would
%    like.  Next, store the positions by calling:
%    >> autoplace record
%
%    To activate automatic placement of windows:
%    >> autoplace on
%    This command can be placed in your STARTUP.M file.
%
%    AUTOPLACE called with no arguments will move any currently open figure
%    windows to the predefined positions.
%
%    AUTOPLACE(MESSAGE) controls the behavior of the automatic placement.
%    Supported messages are:
%
%    ON     - Turns the autoplacment on.  Autoplacment must be turned on
%             before any windows will be put into predefined positions.
%    OFF    - Turns the autoplacment off.
%    STATUS - Returns on/off status.
%    RECORD - Captures the positions of currently open windows as the
%             positions to use for windows as they are opened.
%             AUTOPLACE('record') must be called before any automatic
%             placment will be performed.
%    PLACE  - Places currently open windows in defined positions.
%    OPEN   - Opens new figure window in next specified position.  This
%             message is used in the callback for the root window's default
%             figure creation function.
%
%    AUTOPLACE works by setting the root window objects default figure
%    creation.  It will overwrite any pre-existing default figure placement
%    function.
%
%    AUTOPLACE only positions figure windows with their HANDLEVISIBILITY
%    parameter set to ON.  It should not affect any GUI programs which
%    protect their figures integrity with the HANDLEVISIBILITY parameter.
%

% Implementation:
% Figure positions stored and an NxM cell array where row index represents
% the total number of open figures and column index holds the figure
% position for that figure in the configuration N.
% Also, these cells are stored in a dictionary indexed by the screen size
% so that different monitor size are supported for mobile applications.

% DLH (doug.harriman@gmail.com)
% 17-Sep-12 - Changed ScreenSize to MonitorPosition to better handle
%             different screen configurations.
% 20-May-08 - Updated to place windows in position sets and to use screen
%             size to select those sets.
% 13-Dec-06 - Rewritten as a single function utilizing up to date Matlab
%             features.
% 30-Sep-98 - Created.

function autoplace(msg)

% If no message, assume empty string
if nargin == 0
    msg = 'status';
end

% Switch on message
switch lower(msg),
    case 'on'
        % Turn it on by setting the default fcns.
        set(0,'DefaultFigureCreateFcn','autoplace(''Open'');');
        disp('Figure autoplacement activated');
        drawnow
        
    case 'off'
        % Turn it on by setting the default fcns.
        set(0,'DefaultFigureCreateFcn','');
        disp('Figure autoplacement deactivated');drawnow
        
    case 'place'
        % Make sure that positions have been defined.
        if ~ispref('Autoplace','PositionList')
            warning('AUTOPLACE is active but no figure positions have been defined.'); %#ok<WNTAG>
            return;
        end
        
        % Backwards compatilbilty.
        % Make sure we have a dictionary of positions.
        pos_list_dict = getpref('Autoplace','PositionList');
        if ~isa(pos_list_dict,'dict')
            warning('Upgrading to new version of AUTOPLACE.  Previous positions lost.'); %#ok<WNTAG>
            pos_list_dict = dict;
        end
        
        % Make sure we have some positions for this screen size
        sz = sortrows(get(0,'MonitorPositions'));
        pos_list = pos_list_dict(sz);
        if isempty(pos_list)
            warning('The figure position list is empty.  Try >> autoplace record'); %#ok<WNTAG>
            return;
        end
        
        % Number of figures to place
        ch = findobj(get(0,'Children'),'Type','figure');
        
        % Filter out figures with supposedly non-visible handles.
        tf = arrayfun(@(x)strcmp(get(x,'HandleVisibility'),'on'),ch);
        ch = ch(tf);
        
        [~,idx ] = sort([ch.Number],'ascend');
        ch = ch(idx);
        n_fig = length(ch); % Number of open figures.
        
        % Number of positions stored
        n_pos = length(pos_list);
        
        % Use default figure position if number of figures has not been
        % defined.
        if n_fig > n_pos
            return;
        end
        
        % Place the figures.
        units = get(ch,'Units');
        if ~iscell(units)
            units = {units};
        end
        set(ch,'Units','Pixels');
        pos_list = pos_list{n_fig};
        
        % Only position figure if we have a position list for this figure
        % count.
        if ~isempty(pos_list)
            for i = 1:n_fig
                set(ch(i),'Position',pos_list{i},'Units',units{i});
            end
        end
        
        
    case 'status'
        % Check current default fcn
        fcn = get(0,'DefaultFigureCreateFcn');
        
        
        if strcmp(fcn,'autoplace(''Open'');')
            active = 'enabled';
        else
            active = 'disabled';
        end
        fprintf(1,'\nAutoplace: %s\n',active);
        
        % Monitor listing
        mon = sortrows(get(0,'MonitorPositions'));
        fprintf(1,'Monitor configuration:\n');
        for i = 1:size(mon,1)
            fprintf(1,'\tMontior %d: %dx%d\n',i,...
                mon(i,3)-mon(i,1)+1,...
                mon(i,4)-mon(i,2)+1);
        end
        
        % List of positions.
        pos_list_dict = getpref('Autoplace','PositionList');
        if ~isa(pos_list_dict,'dict')
            fprintf(1,'\tNo figure positions defined for this monitor configuration.\n\n');
            return;
        end
        
        % Grab list for this config.
        sz = sortrows(get(0,'MonitorPositions'));
        pos_list = pos_list_dict(sz);
        if isempty(pos_list)
            fprintf(1,'\tNo figure positions defined for this monitor configuration.\n\n');
            return;
        end
        
        n_pos = length(pos_list);
        fprintf(1,'Figure positions defined: %d\n\n',...
            n_pos);
        return
        
    case 'record'
        % Get list of current figure positions.
        children = findobj(get(0,'Children'),'Type','figure');
        
        % Make sure we have at least one figure to record.
        if isempty(children)
            error('No figure windows are open.');
        end
        children = flipud(children);  % first created now on top.
        
        % Set all figures to use common units
        unit_list = get(children,'Units');
        set(children,'Units','pixels');
        
        % Make sure we have a preference to load
        if ~ispref('Autoplace','PositionList')
            addpref('Autoplace','PositionList',dict);
        end
        
        % Backwards compatibility
        pos_list_dict = getpref('Autoplace','PositionList');
        if ~isa(pos_list_dict,'dict')
            warning('Upgrading to new version of AUTOPLACE.  Previous positions lost.'); %#ok<WNTAG>
            pos_list_dict = dict;
        end
        
        % Store away list to preferences.
        pos_list_new  = get(children,'Position');
        if ~iscell(pos_list_new)
            pos_list_new = {pos_list_new};
        end
        sz = sortrows(get(0,'MonitorPositions'));
        pos_list      = pos_list_dict(sz);
        pos_list{length(pos_list_new)} = pos_list_new;
        pos_list_dict(sz) = pos_list;
        setpref('Autoplace','PositionList',pos_list_dict);
        
        % Return to original units
        if ~iscell(unit_list)
            unit_list = {unit_list};
        end
        for i = 1:length(unit_list)
            set(children(i),'Units',unit_list{i});
        end
        
    case 'open'
        % Don't try to position figures where the position is being
        % explicitly set.
        if ~all(get(gcf,'Position') == get(0,'DefaultFigurePosition'))
            return;
        end
        autoplace('place');
        
    otherwise
        error(['Unhandled message: ' upper(msg)]);
end
