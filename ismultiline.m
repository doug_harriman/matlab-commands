%ISMULTILINE  Returns true if string has multiple lines.
%

function tf = ismultiline(str)

% Error checks
error(nargchk(1,1,nargin));
if ~ischar(str)
    tf = false;
    return;
end

str = double(str);
tf = any(str == 10);
tf = tf | any(str ==  13);
