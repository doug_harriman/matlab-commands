%SHIFTLEFT  Takes a binary string and left shifts the number of bits specified.
%
%>> output = shiftleft(input,bitShift)
%

function [out] = shiftleft(in,bits)

% Error check
if ~isstr(in),
   error('SHIFTLEFT works only on binary strings.');
end

% Do the shift
out = in(bits+1:length(in));
out = [out char(ones(1,bits)*48)];
