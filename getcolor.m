%GETCOLOR Returns color of clicked point.
%         Also returns index into the colormap.
%
%>>[color,colormap_index] = getcolor(number_of_points)
%

function [c,index] = getcolor(num) ;

% Error check input
if nargin == 0,
   num = 1 ;
end

% Get image
ch   = get(gca,'children') ;
data = get(ch,'cdata')     ;
map  = colormap            ;

% Loop through points
for i = 1:num,
   % Get data
   disp('Select point')
   [y,x] = ginput(1) ;
   x = (round(x)) ;
   y = (round(y)) ;
   
   index(i) = double(data(x,y)) ;
   c(i,:)  = map(index,:) ;
end

   
   