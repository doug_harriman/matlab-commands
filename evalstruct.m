%EVALSTRUCT  Converts all fields of a structure to Matlab base workspace variables.
%

% Doug Harriman (doug.harriman@hp.com)

function [] = evalstruct(S__)

error(nargchk(1,1,nargin));

% Error checks
if ~isstruct(S__)
    error('Input must be structure.');
end

fields__ = fieldnames(S__);
for i__ = 1:length(fields__)
    var__ = fields__{i__};
    val__ = (S__.(var__));
    assignin('base','temp__',[var__ '=' val__ ';']);
    evalin('base','eval(temp__);');
end