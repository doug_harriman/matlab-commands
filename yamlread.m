%YAMLREAD  Read a YAML formatted data file.
%  DATA=YAMLREAD(FILENAME) reads the YAML formatted text file FILENAME.
%  FILENAME is optional.  The user will be prompted for a file if no file
%  name is provided.
%
%  Requires the SnakeYaml Java library from:
%  http://code.google.com/p/snakeyaml/
%  JAR file should be placed in same directory as this file.
%
%  See also: YAMLPARSE.
%

% Doug Harriman (doug.harriman@gmail.com)
% Created: 23-Aug-2010

function data = yamlread(filename)

% Error checks
error(nargchk(0,1,nargin));

% Prompt user for file
if (nargin < 1) || isempty(filename)
    [file,path] = uigetfile('*.yaml','Select YAML file');
    if file == 0
        error('No file selected.');
    end
    filename = [path filesep file];
end

% Validate filename
if ~ischar(filename)
    error('File name must be a string.');
end

if ~exist(filename,'file')
    error(['Unable to find file: ' filename]);
end

% Load it.
str = file2str(filename);

% Parse it
data = yamlparse(str);