%LOGSPACE2  Generate nicely logrithmically spaced data points.
%  DATA=LOGSPACE2(D1,D2)
%
%  See also: logspace.
%

function [vect] = logspace2(d1,d2)

vect = [];
pts  = [1 1.5 2 3 5 7.5]';
for i = d1:d2
    vect = [vect; pts*10^i];
end