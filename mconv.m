%MCONV  Allows unlimited inputs to 'conv'.
%

% Doug Harriman
% Hewlett-Packard VCD R&D
% doug_harriman@hp.com
% May 12, 1998

function out = mconv(varargin)

% Loop through all arguments
out = 1;
for i = 1:nargin
   out = conv(out,varargin{i});
end
