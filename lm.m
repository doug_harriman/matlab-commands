%LM  Loads 'MAT' file.
%

[LM_FILE,LM_PATH] = uigetfile('*.mat;*.mdl','Load MAT File/Simulink Model');

if LM_FILE~=0,
   % See if we have a simulink file
   if ~isempty( findstr(LM_FILE,'.mdl') ),
      % Load Simulink model
      open_system([LM_PATH  LM_FILE]);
      clear LM_PATH LM_FILE
   else
      % Load MAT File
      load([LM_PATH filesep LM_FILE]);
      clear LM_PATH LM_FILE
   end
else
   clear LM_PATH LM_FILE
   disp('No file selected.');
end
