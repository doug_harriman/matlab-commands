%REPORT  Generates a variable name and value string.
%  STR=REPORT(VAR) returns a string.
%  REPORT(VAR) displays the string to the Matlab command window.
%  REPORT(VAR,FORMAT) formats numeric values with the optional format
%  string.
%

% Doug Harriman (dharriman@xzeres.com)

function str = report(obj,format)

% Handle different data types differently.

% Default
if nargin < 2
    % General floating point format
    format = '%f';
end

% Numerics
if isnumeric(obj)
    valstr = num2str(obj,format);
elseif ischar(obj)
    valstr = ['''' obj ''''];
else
    error(['Unhandled data type: ' class(obj)]);
end

% Build rest of string
str = [inputname(1) ' = ' valstr];

% Just display
disp(str);
clear('str');
    