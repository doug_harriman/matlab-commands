%BINAVG  Calculates binned averages.
%  [XBIN,YBIN]=BINAVG(DATA1,DATA2,SPACING) calculates binned averages values of DATA2
%  based on separating DATA1 into bins.  SPACING default is 1.
%
%  See also: bin
%

% Doug Harriman
% Created: 22-Jul-2011

function [xbin,yavg,idx] = binavg(x,y,spacing)

% Error checks
error(nargchk(2,3,nargin));
if nargin < 1
    spacing = 1;
end
if ~isnumeric(spacing) || ~isscalar(spacing)
    error('numeric scalar expected.');
end

% Clear out NaN's
idx = ~isnan(x);
x = x(idx);
y = y(idx);
idx = ~isnan(y);
x = x(idx);
y = y(idx);

% Bin X data
[xbin,~,idx] = bin(x,spacing);

% Average y data.
yavg = ones(size(xbin))*NaN;
for i = 1:length(xbin)
    yavg(i) = mean(y(idx==i));
end

% Update the plot.
hold('on');
plot(xbin,yavg,'r');
pt;

