%YAMLPARSE  Parses a YAML string and returns a Matlab structure.
%  DATA=YAMLPARSE(STR) parses the YAML formatted string and returns the
%  Matlab structure DATA.
%
%  Requires the SnakeYaml Java library from:
%  http://code.google.com/p/snakeyaml/
%  JAR file should be placed in same directory as this file.
%

% Doug Harriman
% Created: 31-Jan-2011

function data = yamlparse(str)

% Error checks
error(nargchk(1,1,nargin));
if ~ischar(str)
    error('Character input expected.');
end

% Check to makre sure Snake YAML is on Java path
p = javaclasspath;
if isempty(cell2mat(strfind(p,'snakeyaml')))
    % Add JAR to dynamic java path
    path = fileparts(which(mfilename));
    jar = dir([path filesep 'snakeyaml*.jar']);
    javaaddpath([path filesep jar.name]);
end

% Import the file
import('org.yaml.snakeyaml.Yaml')

% Create a parser
yaml = Yaml();

% Get the java data object.
try
   data = yaml.load(str);
catch err
    % Trim java error messages to just the useful part.
    msg = err.message;
    idx = strfind(msg,[char(9) 'at']);
    idx = idx(1);
    msg = strtrim(msg(1:idx));
    error(msg);
end

% Convert java objects to native Matlab objects.
data = java2m(data);


