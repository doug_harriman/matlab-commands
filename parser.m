%PARSER  Parses an infix mathematical expression grammar.
%

% TODO: Handle exponentiaion: '^':OP_EXP.  
%       This needs to have precedence over mult and add. (2*x)^2 != 2*(x^2)
% Next up: need to add edges and node heirarchy as tokens are added.
% TODO: Make graph and lexer read only.
% TODO: Hide cur_node property.


% Token types:
% ID          - Variable name.
% CONST       - Numeric constant.
% OP_ADD      - '+', '-'
% OP_MULT     - '*', '/'
% PAREN_OPEN  - '('
% PAREN_CLOSE - ')'

% Grammar definition.
% <expr>   --> (<term> OP_ADD <term>)*
% <term>   --> (<factor> OP_MULT <factor>)*
% <factor> --> ID | CONST | PAREN_OPEN <expr> PAREN_CLOSE

% Doug Harriman (doug.harriman@gmail.com)

classdef parser < handle
    properties
        % Lexer
        lexer = lexer;
        
        % Parse tree
        graph;
        
        % Current node handle.
        cur_node;
        
    end % properties - public
    
    methods
        function parse(obj,str)
            %PARSE  Parse the given expression string.
            %
            
            % Perform the lexical analysis (tokenize)
            try
                obj.lexer.string = str;
            catch err
                % Minimize error stack.
                throw(err);
            end
                
            % Initialize the graph.
            obj.graph = graph('Name',['Parse tree for: ' str]); %#ok<CPROP,PROP>
            
            % Get the first token.
            tok = obj.lexer.next_token;
            
            % Try to match an expression.
            try
                tok = obj.expression(tok);
            catch err
                % Catch parse errors and throw from here so the user
                % doesn't have to get the full call stack, only their call
                % stack.
                throw(err);
            end
            
            % If there are remaining tokens, something is wrong.
            if ~isempty(tok)
                error(['Parse error at: ' tok.val]);
            end
            
        end % parse

        function plot(obj)
            %PLOT  Plots the parse tree.
            %
            
            plot(obj.graph);
            
        end % plot
        
    end % methods - public
    
    methods (Access=private)
        
        function tok = expression(obj,tok)
            %EXPRESSION  Match an expression.
            % <expr> --> (<term> OP_ADD <term>)*
            
            % Match the first term.
            tok = obj.term(tok);
            
            % The current node is the left argument.
            expr.left = obj.cur_node;
            
            % Now need an additive operator.
            nh = [];
            while ~isempty(tok)
                switch(tok.type)
                    case 'OP_ADD'
                        % Matched the operator, store it.
                        tok = obj.consume(tok);
                        expr.op = obj.cur_node;
                        
                        % Need other expression for term.
                        tok = obj.term(tok);
                        expr.right = obj.cur_node;
                        
                        % Link the expression nodes.
                        n = node('Name','EXPR');
                        [nh,obj.graph] = addnode(obj.graph,n);
                        [~,obj.graph]  = addedge(obj.graph,nh,expr.left);
                        [~,obj.graph]  = addedge(obj.graph,nh,expr.op);
                        [~,obj.graph]  = addedge(obj.graph,nh,expr.right);
                        
                        % If there's more, make this expression node the left.
                        expr.left = nh;
                        
                        % Push down.
                        obj.cur_node = nh;
                        
                    case 'PAREN_CLOSE'
                        % Finishing a () expression, unwind stack.
                        return;
                        
                    otherwise
                        error(['Parse error. Operator expected, got: "' tok.val '"']);
                end
            end
            
        end % expression
        
        function tok = term(obj,tok)
            %TERM  Match a term.
            % <term>   --> (<factor> OP_MULT <factor>)*
            
            % Try to match a factor.
            tok = obj.factor(tok);
            
            % The current node is the left argument.
            expr.left = obj.cur_node;
            
            % Now need a multiplicative operator.
            nh = [];
            while  ~isempty(tok)
                switch(tok.type)
                    case 'OP_MULT'
                        % Matched the opeerator, store it.
                        tok = obj.consume(tok);
                        expr.op = obj.cur_node;
                        
                        % Need other factor for term
                        tok = obj.factor(tok);
                        expr.right = obj.cur_node;
                        
                        % Link the expression nodes.
                        n = node('Name','TERM');
                        [nh,obj.graph] = addnode(obj.graph,n);
                        [~,obj.graph]  = addedge(obj.graph,nh,expr.left);
                        [~,obj.graph]  = addedge(obj.graph,nh,expr.op);
                        [~,obj.graph]  = addedge(obj.graph,nh,expr.right);
                        
                        % If there's more, make this expression node the left.
                        expr.left = nh;

                        % Push this node down.
                        obj.cur_node = nh;

                    case 'OP_ADD'
                        % Not really in a term, pop back up to expression.
                        return;
                        
                    case 'PAREN_CLOSE'
                        % Finishing a () expression, unwind stack.
                        return;
                        
                    otherwise
                        error(['Parse error. Operator expected, got: "' tok.val '"']);
                end
            end
            
        end % term
        
        function tok = factor(obj,tok)
            %FACTOR  Match a factor.
            % <factor> --> ID | CONST | PAREN_OPEN <expr> PAREN_CLOSE
            
            switch(tok.type)
                case 'ID'
                    % Store this token
                    tok = obj.consume(tok);
                    
                case 'CONST'
                    % Store this token
                    tok = obj.consume(tok);
                    
                case 'PAREN_OPEN'
                    % Don't store out parens.
                    % Read the next token directly
                    tok = obj.lexer.next_token;
                    if isempty(tok)
                        error('Missing expression');
                    end
                    
                    % Must match an expression
                    tok = obj.expression(tok);
                    
                    % Catch extra open parenthesis
                    if isempty(tok)
                        error('Mismatched (), have an extra open paren.');
                    end
                    
                    % Must have a close paren.
                    if ~strcmp(tok.type,'PAREN_CLOSE')
                        error('Missing ")"');
                    end
                    
                    % Get next token
                    tok = obj.lexer.next_token;
                    
                case 'PAREN_CLOSE'
                    error('Extra ")" in expression.');
                    
                otherwise
                    error(['Expecting ID, CONST or PAREN, found: ''' tok.val '''']);
            end
            
        end % factor
        
        function tok = consume(obj,tok)
            %CONSUME  Consumes the current token.
            %  Token is stored in the parse tree & the next token is read.
            
            % Create the node and store the token in the node.
            n = node('Name',tok.val,...
                'UserData',tok);
            [nh,obj.graph] = addnode(obj.graph,n);
            obj.cur_node = nh;
            
            % Get the next token
            tok = obj.lexer.next_token;
            
        end
        
    end % methods - private
    
end % parser

