%CODER  Generated code from expression tree.

% This should include a PARSER class so that end user only deals with one
% class.

% TODO: Unify PARSER and LEXER handling of the string.
% TODO: Optimizer - rearrange terms where possible to fold constants together.
% TODO: Optimizer - recognize polynomal evaluation and use Cramer's realization.
%                   See QPOLY and POLY2C.
%

classdef coder < hgsetget
    
    properties
        % Parse tree we're generating from.
        graph;
        
        % Stack for handling unhandled data.
        stack;
        
        % Code string
        string;
        
    end
    
    methods
        function [obj] = coder(G)
            
            % Stack for tracking.
            obj.stack = stack; %#ok<CPROP,*PROP>
            
            % Store the graph we're parsing.
            assert(isa(G,'graph'));
            obj.graph = G;
            
            % Clear the code string.
            obj.string = '';
            
        end % constructor
        
        function [str] = write(obj)
            
            % Get root node of graph
            nh = root(obj.graph);
            n  = getnode(obj.graph,nh);
            
            while(~isempty(nh))
                nh = DoNode(nh);
            end
            
        end
        
        
        function [nh] = DoNode(obj,nh)
            
            % Get the current node name.
            n = getnode(obj.graph,nh);
            
            if strcmp(n.name,{'TERM','EXPR'})
                lh = obj.NodeLeft;
                rh = obj.NodeRight;
                
                oh = obj.NodeOp;
            else
                
            end
            
            nl = obj.NodeLeft;
            
        end

        function [nt] = NodeLeft(obj,nh)
            %NODELEFT  Return left node handle.
            %
            
            % Edges eminating from this node.
            eh = edges(obj.graph,nh);
            
            % Left edge is first.
            eh = eh(1);
            
            % Get the node it points to.
            [~,nt] = nodes(obj.graph,eh);
            
        end % NodeLeft
        
        function [nt] = NodeRight(obj,nh)
            %NODERIGHT  Return right node handle.
            %
            
            % Edges eminating from this node.
            eh = edges(obj.graph,nh);
            
            % right edge is third.
            eh = eh(3);
            
            % Get the node it points to.
            [~,nt] = nodes(obj.graph,eh);
            
        end % NodeRight

        function [nt] = NodeOp(obj,nh)
            %NODEOP  Return the operator node handle.
            %
            
            % Edges eminating from this node.
            eh = edges(obj.graph,nh);
            
            % Operator edge is middle.
            eh = eh(2);
            
            % Get the node it points to.
            [~,nt] = nodes(obj.graph,eh);
            
        end % NodeOp
        
    end % methods - public
    
end % classdef