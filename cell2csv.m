%CELL2CSV  Converts a Cell string array to a CSV string.
%

function [str] = cell2csv(C)

assert(iscellstr(C));

fcn = @(s) [s ','];
C = cellfun(fcn,C,'UniformOutput',false);
str = [C{:}];