%TILE  Tiles all figure windows on the screen.
%

function [] = tile()

% Get screen data
set(0,'Units','Pixels');
screen = get(0,'ScreenSize');
figs   = sort(findobj(get(0,'Children'),'Type','Figure'));

% Calculate row and column data
numFigs = length(figs);
temp    = sqrt(numFigs);
numRows = round(temp);
numCols(1:numRows-1) = ones(1,numRows-1)*ceil(temp);
numCols(numRows) = numFigs-sum(numCols);

% Calc figure sizes
height = (screen(4) - numRows*75 - 35)/numRows;
width  = (screen(3)-10)./numCols - numCols*2;

% Loop through all figures
index = 1;
for i = 1:numRows,
   
   % Calc figure y position
   y = (height+80)*(numRows-i) + 35;
   
   for j = 1:numCols(i),
      % Set figure units
      oldUnits = get(figs(index),'units');
      set(figs(index),'units','pixels');
      
      % Calc x figure position
      x = (width(i)+10)*(j-1)+5;
      
      % Move and size figure
      set(figs(index),'Position',[x y width(i) height]);
      
      % Reset units
      set(figs(index),'units',oldUnits);
      
      % Bookkeeping
      index = index + 1;
      
   end
end

