%COMMONFIELDS  Find field names which are common between two structures.
%  [COMMON_FIELDS,STRUCT_A_ONLY]=COMMONFIELDS(STRUCT_A,STRUCT_B) returns a 
%  list of names COMMON_FIELDS which exist in both STRUCT_A and STRUCT_B
%  and a list of STRUCT_A_ONLY names which exist only in STRUCT_A.  Field
%  name match is case insensitive.
%
%  See also: struct, copyfields.

% <USER_ID>
% </USER_ID>

function [common,unique_a] = commonfields(struct_a,struct_b)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'CodeTools';
TOOLBOX_MODULE  = '';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Error check inputs.
if ~isa(struct_a,'struct')
    error('Input to copy data from must be a strucure.');
end
if ~isa(struct_b,'struct')
    error('Input to copy data to must be a strucure.');
end

% Extract field names
fieldnames_a = fieldnames(struct_a);
fieldnames_b = fieldnames(struct_b);

% Case insensitive
fieldnames_a = lower(fieldnames_a);
fieldnames_b = lower(fieldnames_b);

% Find similarities
tf     = ismember(fieldnames_a,fieldnames_b);
common = fieldnames_a(tf);

% Fieldnames unique only to a
unique_a = fieldnames_a(~tf);