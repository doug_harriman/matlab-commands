%GETPOINT  Labels (x,y) point nearest to mouse click.
%          Getpoint will accept 'x' or 'y' as an argument
%          if only one coordinate label is desired.
%
%>>[x,y,handle] = getpoint
%
%          Getpoint will also accept a number for the number of points
%          to get, followed by the optional 'x', or 'y'.

% If the number is zero, Getpoint will keep going until 
% the user presses 'q'.
%

function [xp,yp,han] = getpoint(varargin)

if nargin == 0,
   % Only want one point, all outputs
   [xp,yp,han] = doit ;
   
elseif nargin == 1,
   if isstr(varargin{1}),
      % Only want one point, single output
      [xp,yp,han] = doit(varargin{1}) ;
   else,
      % Want multiple points, both outputs
      for i = 1:varargin{1},
         [xp(i), yp(i), han(i,:)] = doit ;
      end
   end
   
elseif nargin == 2,
   % Want multiple points, single output
   for i = 1:varargin{1},
      [xp(i), yp(i), han(i,:)] = doit(varargin{2}) ;
   end
end


%--------------------------------------------------
% Actual work gets done here

function [xp,yp,han] = doit(in)

% UI
disp('Click on plot')
[x,y] = ginput(1) ;
axhan = gca ;

% Get data series
han = get(axhan,'children') ;
han = findobj(han,'type','line') ;

% Loop through all line data 
for i = 1:length(han),
   % Extract data
   xdata = get(han(i),'xdata') ; 
   ydata = get(han(i),'ydata') ; 

   % Calculate distance to points, vectorized 2 norm.
   dist{i} = [xdata-x;ydata-y] ;
   dist{i} = dist{i}.^2 ;
   dist{i} = sum(dist{i}) ;
   dist{i} = dist{i}.^0.5 ;
   
   % Get minumum
   mindist(i) = min(dist{i}) ;
end

% Overall minumum
MINDIST = min(mindist) ;

% Find the index of which plot it's in
ind1 = find(mindist == MINDIST) ;

% Find minimum distance
ind2 = find(dist{ind1} == MINDIST) ;

% Get that data
xdata = get(han(ind1),'xdata') ;
ydata = get(han(ind1),'ydata') ;

% Point coords
xp = xdata(ind2) ;
yp = ydata(ind2) ;

% Plot point
hold on
han = [] ;
han(1) = plot(xp,yp,'+') ;
if nargin == 0,
  han(2) = text(xp,yp,['(' num2str(xp,'%3.3g') ',' num2str(yp,'%3.3g') ')']) ;
elseif strcmp(lower(in),'x'),
  han(2) = text(xp,yp,['(' num2str(xp,'%3.3g') ')']) ;
elseif strcmp(lower(in),'y'),
  han(2) = text(xp,yp,['(' num2str(yp,'%3.3g') ')']) ;
end

    

