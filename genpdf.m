%GENPDF  Opens all figures in current directory and generates PDF files.
%

% Doug Harriman

function genpdf

% Let user select files.
[fn_list,pn] = uigetfile('*.fig','Figure Files',...
    'MultiSelect','on');
if isnumeric(fn_list)
    % User cancelled out
    return;
end

% Make sure we have a cell
if ~iscell(fn_list)
    fn_list = {fn_list};
end

for i = 1:length(fn_list)
    % UI
    disp(['Generating PDF ' int2str(i) ' of ' int2str(length(fn_list))]);
    
    % Load it
    fn = [pn filesep fn_list{i}];
    open(fn);
    
    % Set it up for nice landscape printing
    set(gcf,...
        'PaperOrientation','Landscape',...
        'PaperUnits','Inches',...
        'PaperSize',[ 11.0000    8.5000],...
        'PaperPosition',[ 0.2500    0.2500   10.5000    8.0000]);
    
    % Generate pdf
    [p,fn] = fileparts(fn);
    print('-dpdf',[fn '.pdf']);
   
    % Close it
    close(gcf);
end