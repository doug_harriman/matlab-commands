%HGINFO  Returns Mercurial repository information for current directory.
%  INFO=HGINFO returns a structure INFO with fields:
%    revision      - HG repository base revision.
%    repo_working  - HG repository working repository root path.
%    repo_source   - HG server repository path.
%    file_modified - List of HG controlled files currently modified.
%    file_change   - List of modification types for all modified files.
%

%  Doug Harriman (doug.harriman@hp.com)

function [info] = hginfo

% Bail for PC
if ispc
    info.revision       = -1;
    info.repo_working   = 'No HG Info';
    info.repo_source    = 'No HG Info';
    info.file_modified  = 'No HG Info';
    return;
end

% Current revision
[res,temp] = system('hg log -l1 --template ''{rev}''');
if res ~= 0
    error('HG command failed');
end
info.revision = str2double(temp);

% Current working repository
[res,temp] = system('hg root');
info.repo_working = strtrim(temp);

% Source repository
[res,temp] = system('hg showconfig paths.default');
info.repo_source = strtrim(temp);

% Modified files
[res,temp] = system('hg status -mard');
temp = words(temp,char(10));

% Eliminate empty entries.
idx = cellfun(@(x)~isempty(x),temp);
temp = temp(idx)';

% Break into list of modifications and list of modification type.
info.file_change   = cellfun(@(x)x(1),temp,'UniformOutput',false);
info.file_modified = cellfun(@(x)strtrim(x(2:end)),temp,'UniformOutput',false);

% If no output, pretty print
if nargout > 0
    return;
end

disp(' ')
disp('Mercurial repository information');
disp(['Server repository path : ' info.repo_source]);
disp(['Working repository path: ' info.repo_working]);
disp(['Revision               : ' int2str(info.revision)]);

len = length(info.file_modified);
if len > 0
    disp(['Modifications (' int2str(len) '):'])
    disp(temp);
else
    disp('No modified files');
    disp(' ');
end
clear('info');