%YAMLDB  Create a simple database from a YAML file.
%

classdef yamldb < hgsetget
    properties
        % General user defined DB information.
        metadata = struct;
        
    end % properties - public

    properties (SetAccess=protected)
        % Data object type.
        datatype = '';
        
        % Data
        data = {};
        
    end % properties - read only
    
    methods
        function obj = yamldb(filename)
            %YAMLDB  Constructor.
            %
            
            % Error checks
            error(nargchk(0,1,nargin));
            if nargin < 1
                filename = [];
            end
            
            % Read the file
            S = yamlread(filename);
            
            % Look for a database entry
            if ~isfield(S,'database')
                error('No database found in YAML data');
            end
            
            % Process the database.
            db = S.database;
            if isfield(db,'metadata')
                obj.metadata = db.metadata;
            end
            
            if ~isfield(db,'data')
                error('Database must contain a minimum of one entry.');
            end
            
            % Determine the data type
            fn = fields(db.data{1});
            if length(fn) > 1
                error('Data entries must be of a single type.');
            end
            fn = fn{1};
            obj.datatype = fn;
            
            % Construct objects and store.
            for i = 1:length(db.data)
                obj.data{end+1,1} = feval(obj.datatype,...
                    db.data{i}.(obj.datatype));
            end 
            
            
        end % constructor
        
        function res = query(obj,field,val)
            %QUERY  Returns all matching data.
            
            tf = cellfun(@(x)isequal(x.(field),val),obj.data);
            if ~any(tf)
                res = [];
            elseif length(find(tf)) > 1
                res = obj.data(tf);
            else
                res = obj.data{tf};
            end
            
        end % query
        
        function val = length(obj)
            %LENGTH  Returns number of entries in database.
            %
            
            val = length(data);
            
        end % length
        
        function delete(obj,field,val)
            %DELETE  Deletes the entry matching the specified field value.
            %
            
        end % delete
        
        function add(obj,data)
            %ADD  Adds the new data object to the database.
            %
            
        end % add
        
        function str = yaml(obj,filename)
            %YAML  Serializes the database to a YAML string or file.
            %  Data objects must support a YAML method.
            %
            
        end % yaml
        
        function str = csv(obj,filename)
            %CSV  Serializes the database to a CSV string or file.
            %  Data objects must support a CSV method.
            %
            
        end % csv
            
    end % methods - public
    
end % classdef