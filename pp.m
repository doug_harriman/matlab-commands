%PP Pretty print the plot
%   Default is layout is LANDSCAPE.
%   Other option is PORTRAIT
%

% DLH
% 11/29/01 - Complete rewrite.

function [] = pp(layout)

if nargin < 1,
    layout = 'landscape';
end

% Set for full page landscape
set(gcf,'PaperUnits','inches',...
    'PaperPositionMode','manual',...
    'PaperSize',[11 8.5],...
    'PaperType','usletter');

switch lower(layout)
    case 'landscape'
        set(gcf,'PaperOrientation','landscape',...
            'PaperPosition',[0.25 0.25 10.5 8]);

    case 'portrait'
        set(gcf,'PaperOrientation','portrait',...
            'PaperPosition',[0.25 0.25 8 10.5]);
    otherwise
        error(['Unknown layout specifier: ' layout]);
end

% Handle differently depending on renderer
origRenderer = get(gcf,'Renderer');
switch lower(origRenderer),
    case 'opengl',
        fileName = 'c:\figure.tif';

        % UI
        disp('Figure uses the OpenGL Renderer');
        disp('OpenGL rendered figures will not print from Matlab');
        disp(['TIF file of figure written to: ' fileName]);

        % Output
        print(gcf,'-dtiff','-r300',fileName);

        % Load the image
        i = imread(fileName);

        % Rotate it
        for ind = 1:3,
            ii(:,:,ind) = i(:,:,ind)';
        end

        % Create a new figure
        fig = figure;
        image(ii);
        set(gca,'xdir','reverse',...
            'visible','off',...
            'position',[0 0 1 1]);

        % Call ourself recursively.
        feval('pp',layout);
        delete(fig);
        delete(fileName);
        return;

    case 'zbuffer'
        set(gcf,'Renderer','painters');

end

label_han = plotlabel;

% Print it
if ispc
    print -dwinc
else
    print -dpsc
end

% Clean off stuff we added
set(gcf,'Renderer',origRenderer);
delete(label_han);
