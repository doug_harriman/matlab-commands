%CMDWAITSPIN  Command line wait spinner.
%  CMDWAITSPIN(COMMAND) where command is one of: start, update, end.  Call
%  update time a display update is required.
%

% Doug Harriman (doug.harriman@gmail.com)

function cmdwaitspin(cmd)

persistent state

% Supported commands
switch lower(cmd)
    case 'start'
        fprintf('[0]');
        state = 1;
        
    case 'update'
        % Spinner chars
        chars = {'|','/','-','\\'};
        fprintf('\b\b');
        fprintf([chars{state} ']']);
        
        % Update data
        state = state + 1;
        
        % Wrap counter
        if state > length(chars)
            state = 1;
        end
        
    case 'stop'
        fprintf('\b\b');
        fprintf('X]');
        
    otherwise
        error(['Unsupported command: ' cmd]);
end
