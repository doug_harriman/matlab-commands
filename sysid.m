%SYSID  System identification of a 1D system.
%
%>>[system, exp_magnitude, exp_phase] = sysid(input,output,model)
%     input         - Input signal (channel object).
%     output        - Output signal (channel object).
%     model         - System model (LTI object).
%     system        - Identified model (LTI object).
%     exp_magnitude - Experimental Bode magnitude (channel object).
%     exp_phase     - Experimental Bode phase (channel object).
%

% DLH 2/10/98

function [sys,mag,ph] = sysid(input,output,model)

% Get the useful part of the input
w       = psd(input) ;  % Calculate the power spectrum of the input
%w_limit = max(w)/1.1   ;  % Lowest usable power
%w       = w(find(w>=w_limit)) ;  % Drop everything below that
f_max   = max(get(w,'freqs')) ;  % Maximum frequency
%f_max=6;

% Get the fft data
[y_w,w] = fft_t(get(output,'time'), get(output,'data')) ;
[u_w,w] = fft_t(get(input,'time'), get(input,'data')) ;

% Eliminate all data above f_max
ind = find(w <= f_max) ;
w   = w(ind)   ;
y_w = y_w(ind) ;
u_w = u_w(ind) ;

% Build a filter for the time data
filt = buildfilt(f_max/2,4,get(input,'sampletime')) ;

% Filter the time data
input  = filter(input,filt) ;
output = filter(output,filt) ;

% Experimental Bode plot
mag = abs(y_w./u_w);
ph  = unwrap(angle(y_w./u_w))*180/pi;
ph(ph>180) = ph(ph>180)-360;
   
% Call system identification algorithm
sys = arx(model,get(input,'time'),get(input,'data'),get(output,'data')) ;
set(sys,'InputName',get(input,'Name'));
set(sys,'OutputName',get(output,'Name'));

% Convert to continuous time
%sys = d2c(sys) ;
   
% Do the identified system bode plot
[mag_ident,ph_ident] = bode(sys,w*(2*pi)) ;
mag_ident = reshape(mag_ident,length(mag_ident),1);
ph_ident  = reshape(ph_ident,length(ph_ident),1);    
ph_ident(ph_ident>180) = ph_ident(ph_ident>180) - 360;

% Plots
if 1,%~nargout,
   
   % Plot comparision bodes
   figure
   set(gcf,'Position',[42   549   560   420]);
   subplot(2,1,1);semilogx(w,20*log10(mag),w,20*log10(mag_ident),'r');
   grid;%stretch
   ylabel('Magnitude (dB)')
   title('Experimental and Identified Bode Plots')
   legend('Experimental','Identified')
   
   subplot(2,1,2);semilogx(w,ph,w,ph_ident,'r');
   grid;%stretch
   ylabel('Phase (deg)')
   xlabel('Frequency (Hz)') 
   
   % Plot residuals bode
   %figure
   %subplot(2,1,1);semilogx(w,20*log10(mag)-20*log10(mag_ident));
   %grid;%stretch
   %ylabel('Magnitude (dB)')
   %title('Residual Bode Plots')
   
   %subplot(2,1,2);semilogx(w,ph-ph_ident);
   %grid;%stretch
   %ylabel('Phase (deg)')
   %xlabel('Frequency (Hz)') 
   
   % Do time comparisons
   [y_ident,t] = lsim(sys,get(input,'data'),get(input,'time')) ;
   
   % Plot time comparisons
   figure
   set(gcf,'Position',[658   549   560   420]);
   subplot(2,1,1);plot(t,get(output,'data'),t,y_ident,'r');
   grid;%stretch
   legend('Experimental','Identified')
   ylabel('System Response')
   title('Time Comparisons of Experimental and Identified Systems')
   
   subplot(2,1,2);plot(t,y_ident-get(output,'data'),'r');
   grid;%stretch
   ylabel('Residual')
   xlabel('Time (sec)')
   
end

% Wrap up the experimental magnitude and phase as channels
mag = channel('Experimental Bode Magnitude',mag,w*i) ;
ph  = channel('Experimental Bode Phase',ph,w*i) ;