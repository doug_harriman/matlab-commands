%ONSCREEN  Forces all figures to be fully on screen.
%

% Doug Harriman (doug.harriman@hp.com)
% 03-Jan-08

function [] = onscreen(kids)

% Default to all figures if no handle provided.
if nargin < 1
    kids = get(0,'Children');
end

set(0,'Units','Pixels');
screen = get(0,'ScreenSize');
units  = get(0,'Units');
topmargin = 100;
sidemargin = 10;

for i = 1:length(kids)
    % Check for figures extending off of any one side
    han = kids(i);
    oldunits = get(han,'Units');
    set(han,'Units',units);
    pos = get(han,'position');

    % Top
    if pos(2) + pos(4) > screen(4) - topmargin
        pos(2) = screen(4) - pos(4) - topmargin;
    end

    % Left
    if pos(1) < 0
        pos(1) = sidemargin;
    end
    
    % Right
    if pos(1) + pos(3) > screen(3)
        pos(1) = screen(3) - pos(3) - sidemargin;
    end
    
    % Bottom
    if pos(2) < 0 
        pos(2) = sidemargin;
    end
    
    % Set the new position
    set(han,'Position',pos);
    
    % Set units back
    set(han,'Units',oldunits);
end