%BK  BitKeeper file access on Linux machine.
%
% See also: bkgui.
%

% Doug Harriman (doug.harriman@hp.com)
% 31-Jul-07 - Added check for BitKeeperPath existance and GUI to select.
% 17-Nov-06 - Created

function [out] = bk(cmd,varargin)

% Preferences list
toolbox_name = 'BitKeeperTools';
prefs = {'BitKeeperPath','BitKeeperRepo','BitKeeperServo','BitKeeperMech'};
vars  = {'path',         'repo',         'servo',         'mech'};
for i = 1:length(prefs)
    if ~ispref(toolbox_name,prefs{i})
        addpref(toolbox_name,prefs{i},[]);
    end

    % If BitKeeperPath is not set, then prompt user to set one.
    bkp = getpref(toolbox_name,'BitKeeperPath');
    if isempty(bkp)
        % Dir selection dialog to pick dir that houses repos.
        dir_name = uigetdir('\\vcsdptguest\net_root\','BK Repo Dir');
       
        if dir_name == 0
            error('BitKeeperPath has not been set.');
        end
       
        setpref(toolbox_name,'BitKeeperPath',dir_name);
    end
    
    % Read value
    value = getpref(toolbox_name,prefs{i});
    if isempty(value)
        eval([vars{i} '=[];']);
    else
        eval([vars{i} '=''' value ''';']);
    end
end

fcns = {'SetPath',...
        'SetRepo',...
        'SetServo',...
        'SetMech',...
        'GetPath',...
        'GetRepo',...
        'GetServo',...
        'GetMech',...
        'GetServoPath',...
        'GetFullPath',...
        'ListRepo',...
        'ListServo',...
        'ListMech'};

% Default output    
if nargin < 1
    disp('Supported commands:');
    disp(fcns');
    
    disp('Current values:');
    disp(['Repo : ' bk('GetRepo')]);
    disp(['Servo: ' bk('GetServo')]);
    disp(['Mech : ' bk('GetMech')]);
    return;
end

% Make sure command is supported
cmd = lower(cmd);
if ~ismember(cmd,lower(fcns))
    error(['Unsupported command: ' cmd]);
end

% Error check inputs.
if strcmp(cmd(1:3),'get') && (nargin > 1)
    error('One input expected for "get" commands.');
end
if strcmp(cmd(1:3),'set') && (nargin ~= 2)
    error('Two inputs expected for "set" commands.');
end
if strcmp(cmd(1:4),'list') && (nargin > 1)
    error('One input expected for "list" commands.');
end

% filesep = '/';
switch cmd
    case 'setpath'
        path = varargin{1};
        setpref(toolbox_name,'BitKeeperPath',path);
    case 'setrepo'
        repo = varargin{1};
        
        % Verify valid repo
        repo_list = bk('ListRepo');
        if ~ismember(repo,repo_list)
            error(['Invalid repo specified: "' repo '", see >> bk listrepo']);
        end
        
        setpref(toolbox_name,'BitKeeperRepo',repo);
    case 'setservo'
        servo = varargin{1};
        setpref(toolbox_name,'BitKeeperServo',servo);
    case 'setmech'
        mech = varargin{1};

        % Make sure '_mech' is in specification
        if isempty(strfind(mech,'_mech'))
            mech = [mech '_mech'];
        end

        % Verify valid repo
        mech_list = bk('ListMech');
        if ~ismember(mech,mech_list)
            error(['Invalid mech specified: "' mech '", see >> bk listmech']);
        end

        setpref(toolbox_name,'BitKeeperMech',mech);
        
    case 'getpath'
        out = path;
    case 'getrepo'
        out = repo;
    case 'getservo'
        out = servo;
    case 'getmech'
        out = mech;
        
    case 'getservopath'
        if isempty(path)
            error('Path not set.');
        end
        if isempty(repo)
            error('Repo not set.');
        end
        if isempty(servo)
            error('Servo not set.');
        end
        
        out = [path filesep repo filesep 'servo' filesep 'src' filesep servo filesep 'src' filesep];
        
    case 'getfullpath'
        if isempty(mech)
            error('Mech not set.');
        end
        
        out = [bk('GetServoPath') mech filesep];
        
    case 'listrepo'
        % Get directory listing
        d = dir([path filesep]);
        d = scalarstruct(d);
        out = d.name;
        out = RemoveRelativeDirs(out)';
        
    case 'listservo'
        if isempty(repo)
            error('Repo must be set before servos can be listed.');
        end
        
        % Get directory listing
        d = dir([path filesep repo filesep 'servo' filesep 'src']);
        d = scalarstruct(d);
        out = d.name;
        out = RemoveRelativeDirs(out)';

    case 'listmech'
        if isempty(repo)
            error('Repo must be set before servos can be listed.');
        end
        if isempty(servo)
            error('Servo must be set before mechs can be listed.');
        end
        
        % Get directory listing
        d = dir([path filesep repo filesep 'servo' filesep 'src' filesep servo filesep 'src' filesep '*_mech']);
        d = scalarstruct(d);
        out = d.name;

    otherwise
        error(['Unsupported command: ' cmd]);
end

% Removes '.' and '..' dirs from a directory listing.
function list = RemoveRelativeDirs(list)
list = setxor(list,{'.','..'});
