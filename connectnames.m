%CONNECTNAMES  Simpler version of the Control Toolbox CONNECT.
%  SYSc = CONNECTNAMES({SYS1, SYS2...},'INPUTS','OUTPUTS')
%  Returns the state space model SYSc for the interconnection
%  of the given systems.  The 'InputName' and 'OutputName' fields 
%  of the subsystems are used to determine which lines to interconnect.
%  INPUTS and OUTPUTS are string lists of the subsystem inputs and 
%  outputs which should be used for the inputs and outputs of the 
%  connected system.
%
%  See also: connect

% Doug Harriman (doug.harriman@hp.com)
% 15-Apr-08 
% 28-Mar-00 - Created

function sysout = connectnames(sysList,inputs,outputs)

% Error check inputs
error(nargchk(3,3,nargin));

% Force input and output specifications to cells.
if ischar(inputs)
    inputs = {inputs};
end
if ischar(outputs)
    outputs = {outputs};
end

% Only works on SS systems
tf = cellfun(@(x)isa(x,'ss'),sysList);
idx = find(~tf);
for i = 1:length(idx)
    sysList{idx} = ss(sysList{idx});
end

% Build large, unconnected system
unconSys  = append(sysList{1},sysList{2:end});

% Consolidate inputs and outputs if have multiple copies of each.
inputName       = get(unconSys,'InputName');
uniqueInputName = unique(inputName);
if length(uniqueInputName) < length(inputName)
    B = [];
    D = [];
    for i = 1:length(uniqueInputName)
        idx = ismember(inputName,uniqueInputName{i});
        idx = find(idx);
        if length(idx) > 1
            B = [B sum(unconSys.b(:,idx)')'];
            D = [D sum(unconSys.d(:,idx)')'];
        else
            B = [B unconSys.b(:,idx)];
            D = [D unconSys.d(:,idx)];
        end
    end
    set(unconSys,'b',B,'d',D,'InputNames',uniqueInputName);
end

outputName       = get(unconSys,'OutputName');
uniqueOutputName = unique(outputName);
if length(uniqueOutputName) < length(outputName)
    C = [];
    D = [];
    for i = 1:length(uniqueOutputName)
        idx = ismember(outputName,uniqueOutputName{i});
        idx = find(idx);
        if length(idx) > 1
            C = [C sum(unconSys.c(idx,:))];
            D = [D sum(unconSys.d(idx,:))];
        else
            C = [C unconSys.c(idx,:)];
            D = [D unconSys.d(idx,:)];
        end
    end
    set(unconSys,'c',C,'d',D,'OutputNames',uniqueOutputName);
end


% Build interconnection matrix
[numOutputs, numInputs] = size(unconSys);
inputName  = get(unconSys,'InputName');
outputName = get(unconSys,'OutputName');
Q = zeros(numInputs,2);
for i = 1:numInputs,
   Q(i,1) = i;
   for j = 1:numOutputs,
      if strcmp(inputName{i},outputName{j}),
      	Q(i,2) = j;
      end
   end
end

% Build input matrix
numConInputs = length(inputs);
inputIndex = [];
for i = 1:numConInputs,
   for j = 1:numInputs,
      if strcmp(inputs{i},inputName{j}),
      	inputIndex(length(inputIndex)+1) = j;
      end
   end
end

% Build output matrix
numConOutputs = length(outputs);
outputIndex = [];
for i = 1:numConOutputs,
   for j = 1:numOutputs,
      if strcmp(outputs{i},outputName{j}),
      	outputIndex(length(outputIndex)+1) = j;
      end
   end
end

% Connect the system
sysout = connect(unconSys,Q,inputIndex,outputIndex);