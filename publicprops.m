%PUBLICPROPS  Return list of public properties for a class.
% PROPLIST=PUBLICPROPS(OBJ) returns a cell string list of public settable
% property names for the class of OBJ.
%

% Doug Harriman

function list = publicprops(obj)

% Error checks
error(nargchk(1,1,nargin));

% Get object metadata
meta = metaclass(obj);

% Property list
props = meta.Properties;

% Look for properties that are 'SetAccess=public'.
idx = cellfun(@(x)strcmp(x.SetAccess,'public'),props);

% Extract any that meet criteria
props = props(idx);

% Get the property names
list = cellfun(@(x)x.Name,props,'UniformOutput',false);
