%F  Function for Figure Switching/Creation.
%

function [] = f()

% Get desired figure
f = input('Figure? ') ;

% Switch to/Create it
figure(f) ;


