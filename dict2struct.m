%DICT2STRUCT  Convert dictionary to structure.
%  S=DICT2STRUCT(D) Converts the keys of D to the fieldnames of S if the
%  keys are convertable to Matlab variable names.
%

function S = dict2struct(D)

warning('DICT2STRUCT is depricated.  Use @DICT/STRUCT instead.');
S=D.struct;